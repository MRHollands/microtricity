SET DATEFORMAT DMY;
DECLARE @FIT_Qtr AS VARCHAR(5), @Read_Date AS DATE;
SET @FIT_Qtr = 'F9Q2';
SET @read_date = '01/09/2018';
WITH Reference  -- This should create the reference column but at the moment it is not taking into account that the same email address can have multiple identifier types
     AS (
     SELECT 
	 BP_ID,
	 email_address,
            STUFF(
(
    SELECT ', '+C1.FiT_ID_Initial
    FROM dbo.Core_Data_vw C1
    WHERE C1.BP_ID = C2.BP_ID FOR XML PATH('')
), 1, 1, '') [Reference]
     FROM dbo.Core_Data_vw C2
     WHERE C2.Email_Address IS NOT NULL
     GROUP BY C2.BP_ID,
              C2.Email_Address),
     QtrReads  -- This gets a list of Reads already recieved which are then excluded
     AS (
     SELECT READINGS.INSTALLATION_ID_link,
            READINGS.Meter_Read_Date,
            READINGS.Meter_Read_Reason
     FROM dbo.READINGS
     WHERE READINGS.Meter_Read_Reason = @FIT_Qtr
           AND READINGS.Meter_Read_Date > @Read_Date)
 
 SELECT * FROM (
     SELECT DISTINCT
            T.Email_Address,
            T.First_Name,
            T.Last_Name,
            LEFT(R.Reference, 14) AS Core_FIT_ID,
            T.Title,
            CASE
                WHEN T.Email_Address IS NULL
                THEN NULL
                WHEN Export_Status <> 'Standard Tariff'
                     AND Export_Meter_Photo = 1
                THEN @FIT_Qtr+' NoExp Photo'
                WHEN Export_Status <> 'Standard Tariff'
                     AND (Export_Meter_Photo = 0
                          OR Export_Meter_Photo IS NULL)
                THEN @FIT_Qtr+' NoExp No Photo'
                WHEN Export_Status = 'Standard Tariff'
                     AND Export_Meter_Photo = 1
                THEN @FIT_Qtr+' Exp Photo'
                WHEN Export_Status = 'Standard Tariff'
                     AND (Export_Meter_Photo = 0
                          OR Export_Meter_Photo IS NULL)
                THEN @FIT_Qtr+' Exp No Photo'
                ELSE NULL
            END AS Identifier,
            R.Reference
     FROM dbo.Tables_Combined T
          LEFT JOIN dbo.MSGS M ON M.BP_ID = T.BP_ID
          LEFT JOIN QtrReads Q ON T.INSTALLATION_ID = Q.INSTALLATION_ID_link
          LEFT JOIN Reference R ON T.bp_id = R.BP_ID AND T.Email_Address = R.Email_Address
     WHERE T.Account_Status = 'Registered Live'
           AND T.Email_Address IS NOT NULL
           AND M.BP_ID IS NULL
           AND Q.INSTALLATION_ID_link IS NULL) RESULTS
		   WHERE Core_FIT_ID is not null and Email_Address <> 'loren.heron@ecolutionenergyservices.com' ;
