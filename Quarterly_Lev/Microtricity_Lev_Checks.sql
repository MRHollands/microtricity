
/*
Server:		uh-gendb-01
Database:	Microtricity2
Author:  Matthew Hollands
Date Last Modified:  28/08/2018
*/

SET DATEFORMAT DMY;
DECLARE @Fit_Period AS VARCHAR(5), @Cutoff_Date AS DATE;
SET @Fit_Period = 'F9Q2';
SET @Cutoff_Date = '04/10/2018';

--Reads greater than Cutoff Date
SELECT 
1 AS 'Reads greater than Cutoff Date',
FiT_ID_Initial,
       r.*
FROM READINGS R
     JOIN INSTALLATION i ON r.INSTALLATION_ID_link = i.installation_id
WHERE Meter_Read_Reason = @FIT_Period
      AND meter_read_date > @Cutoff_Date;

--Reads on non-Registered Live accounts
SELECT 
2 as 'Reads on non-Registered Live accounts',
FiT_ID_Initial,
       account_status,
       move_in,
       Move_out,
       READ_ID,
       Meter_Read_Date,
       Meter_Read_Reason,
       Generation_Read,
       Export_Read
FROM READINGS R
     JOIN INSTALLATION i ON r.INSTALLATION_ID_link = i.installation_id
     JOIN CONTRACT C ON i.installation_id = c.INSTALLATION_ID_link
WHERE Meter_Read_Reason = @FIT_Period
      AND Account_Status <> 'Registered Live'
      AND Meter_read_date BETWEEN Move_In AND Move_Out;

--Initial read not ticked
SELECT 
3 as 'Initial read not ticked',
FiT_ID_Initial,
       READ_ID,
       Meter_Read_Date,
       Meter_Read_Reason,
       Generation_Read,
       Export_Read,
       IncludedInLevelisation,
       Claimed
FROM READINGS R
     JOIN INSTALLATION I ON r.INSTALLATION_ID_link = i.INSTALLATION_ID
WHERE Meter_Read_Reason = 'Initial'
      AND (IncludedInLevelisation = 0
           OR Claimed = 0
           OR IncludedInLevelisation IS NULL
           OR Claimed IS NULL);

--bad % split
SELECT 
4 as 'bad % split',
FiT_ID_Initial,
       INSTALLATION_ID,
       Initial_System_Pct_Split,
       Extension_1_Pct_Split,
       Extension_2_Pct_Split,
       SUM(COALESCE(Initial_System_Pct_Split, 0) + COALESCE(Extension_1_Pct_Split, 0) + COALESCE(Extension_2_Pct_Split, 0)) AS [Total%]
FROM INSTALLATION
GROUP BY FiT_ID_Initial,
         INSTALLATION_ID,
         Initial_System_Pct_Split,
         Extension_1_Pct_Split,
         Extension_2_Pct_Split
HAVING SUM(COALESCE(Initial_System_Pct_Split, 0) + COALESCE(Extension_1_Pct_Split, 0) + COALESCE(Extension_2_Pct_Split, 0)) <> 1
       AND FiT_ID_Initial IS NOT NULL; 

--Tariff code mismatch to rates table
SELECT DISTINCT
    	   5 as 'Tariff code mismatch to rates table',
	   fit_id_initial,
       Initial_System_Tariff_Rate,
       r1.Tariff_Code AS rate_1,
       Initial_System_Export_Rate,
       r2.Tariff_Code AS rate_2,
       Extension_1_Tariff_Rate,
       r3.Tariff_Code AS rate_3,
       Extension_1_export_Rate,
       r4.Tariff_Code AS rate_4,
       Extension_2_Tariff_Rate,
       r5.Tariff_Code AS rate_5,
       Extension_2_Export_Rate,
       r6.Tariff_Code AS rate_6
FROM INSTALLATION I
     JOIN contract c ON i.INSTALLATION_ID = c.INSTALLATION_ID_link
     LEFT JOIN Rates R1 ON I.Initial_System_Tariff_Rate = R1.Tariff_Code
     LEFT JOIN Rates R2 ON I.Initial_System_Export_Rate = R2.Tariff_Code
     LEFT JOIN Rates R3 ON I.Extension_1_Tariff_Rate = R3.Tariff_Code
     LEFT JOIN Rates R4 ON I.Extension_1_export_Rate = R4.Tariff_Code
     LEFT JOIN Rates R5 ON I.Extension_2_Tariff_Rate = R5.Tariff_Code
     LEFT JOIN Rates R6 ON I.Extension_2_Export_Rate = R6.Tariff_Code
WHERE account_status = 'Registered Live'
      AND fit_id_initial IS NOT NULL
      AND ((I.Initial_System_Tariff_Rate IS NOT NULL
            AND R1.Tariff_Code IS NULL)
           OR (I.Initial_System_Export_Rate IS NOT NULL
               AND R2.Tariff_Code IS NULL)
           OR (I.Extension_1_Tariff_Rate IS NOT NULL
               AND R3.Tariff_Code IS NULL)
           OR (I.Extension_1_export_Rate IS NOT NULL
               AND R4.Tariff_Code IS NULL)
           OR (I.Extension_2_Tariff_Rate IS NOT NULL
               AND R5.Tariff_Code IS NULL)
           OR (I.Extension_2_Export_Rate IS NOT NULL
               AND R6.Tariff_Code IS NULL));


--Multiple F8Q1 reads on FiT
SELECT DISTINCT
      6 as 'Multiple F*Q* reads on FiT',
	   FiT_ID_Initial,
       INSTALLATION_ID,
       [COUNT] AS Count_Reads
FROM INSTALLATION I
     JOIN
(
    SELECT INSTALLATION_ID_link,
           COUNT(read_id) [count]
    FROM READINGS
    GROUP BY INSTALLATION_ID_link,
             Meter_Read_Reason
    HAVING Meter_Read_Reason = @FIT_Period
) qtr ON I.INSTALLATION_ID = qtr.INSTALLATION_ID_link
WHERE [count] > 1;

