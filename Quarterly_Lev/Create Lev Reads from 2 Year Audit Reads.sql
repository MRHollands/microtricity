/*
Server:		uh-gendb-01
Database:	Microtricity2
Author:  Matthew Hollands
Date Last Modified:  28/08/2018
*/


set dateformat dmy
Declare 
@Qtr as Varchar(10),
@StartDate as Date,
@EndDate as Date

--  SET VARIABLES TO CURRENT LEVELISATION VALUES

Set @Qtr = 'F9Q2'
Set @StartDate = '01/07/2018'
Set @EndDate = '04/10/2018'


Insert into Readings
(
Installation_ID_link,
Meter_Read_Type,
Meter_Read_Date,
Meter_Read_Reason,
Generation_Read,
Export_Read,
Gen_Validation,
MSN,
Export_MSN
)

select 
Readings.INSTALLATION_ID_link,
Meter_Read_Type,
Meter_Read_Date,
@Qtr,
Generation_Read,
Export_Read,
Gen_Validation,
MSN,
Readings.Export_MSN 
from READINGS 
Left join (Select INSTALLATION_ID_link from READINGS Where Meter_Read_Reason = @qtr) AS R2 on READINGS.INSTALLATION_ID_link = R2.INSTALLATION_ID_link 
inner join INSTALLATION on readings.INSTALLATION_ID_link = Installation.INSTALLATION_ID
where 
Meter_Read_Date between @startdate and @enddate
AND 
Meter_Read_Reason = '2 Year Audit'
AND
R2.INSTALLATION_ID_link is NULL
AND
Export_Status <> 'Standard Tariff'
