/*
Server:		uh-gendb-01
Database:	Microtricity2
Author:  Matthew Hollands
Date Last Modified:  28/08/2018
*/

set dateformat dmy

select
FiT_ID_Initial,
GeneratorID,
Account_Status,
Meter_Read_Reason,
readings.IncludedInLevelisation AS Paid,
readings.Claimed,
readings.Created_by2,
readings.Created_date2

from contract 
inner join installation on contract.INSTALLATION_ID_link = INSTALLATION.INSTALLATION_ID
inner join readings on installation.INSTALLATION_ID = readings.INSTALLATION_ID_link
inner join bp on bp.BP_ID = contract.BP_ID_link


where 
Meter_Read_Reason = 'F8Q3'
AND
Meter_Read_Date > '05/01/2018'