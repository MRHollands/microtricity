
-- Step 1 Clear the Read Validation Table and the Valid FIT list.
/*
DELETE dbo.READ_VALIDATION
delete ASG_MASS_READ_VALID_LIST
*/


-- Step 2 Insert the new set of reads into the Read Validation Table

-- Step 2a check reads have uploaded
/*
SELECT * FROM dbo.READ_VALIDATION
*/

-- Step 3 Get the list of Valid read FITs from Ben R.  Upload list of FITS into the ASG_MASS_READ_VALID_LIST table

-- Step 3a Check you have the right number of valid reads to upload
/*
select * from
  dbo.Mass_Read_Stage_1_vw M INNER JOIN ASG_MASS_READ_VALID_LIST A ON M.FiT_ID = A.FIT_ID
 LEFT JOIN readings R ON M.INSTALLATION_ID = R.INSTALLATION_ID_link AND M.New_Read_Date = R.Meter_Read_Date AND M.New_Gen_Read = R.Generation_Read
 WHERE R.READ_ID IS null
 */

-- Step 4 Run this query to add the valid reads to the Readings table



INSERT INTO dbo.READINGS
(    INSTALLATION_ID_link,
    Meter_Read_Type,
    Meter_Read_Date,
    Meter_Read_Reason,
    Generation_Read,
    Export_Read,
    IncludedInLevelisation,
    Gen_Validation,
    Claimed,
    MSN,
    Recieved_Method)

SELECT

    INSTALLATION_ID,
    New_Read_Type,
    New_Read_Date,
    New_Read_Reason,
    New_Gen_Read,
    New_Exp_Read,
    0,
    'Mass Upload',
    0,
    Generation_MSN,
    m.Recieved_Method

 FROM dbo.Mass_Read_Stage_1_vw M INNER JOIN ASG_MASS_READ_VALID_LIST A ON M.FiT_ID = A.FIT_ID
 LEFT JOIN readings R ON M.INSTALLATION_ID = R.INSTALLATION_ID_link AND M.New_Read_Date = R.Meter_Read_Date AND M.New_Gen_Read = R.Generation_Read
 WHERE R.READ_ID IS null
