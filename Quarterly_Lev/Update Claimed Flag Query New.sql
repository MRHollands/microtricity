/*
Server:		uh-gendb-01
Database:	Microtricity2
Author:  Matthew Hollands
Date Last Modified:  28/08/2018
*/
/*  

1.  Delete Temp_Claimed  (To clear previous Lev data).
2.  Upload data from previous FxQx Levelisation Submission Data.xlsx  (NOT the Post submission version).
3.	Use this select script to confirm you are selecting the right data.

*/
select distinct
A.READ_ID,
A.INSTALLATION_ID_link, 
A.Meter_Read_Date,
A.Meter_Read_Reason,
A.Meter_Read_Type,
A.Claimed
from READINGS as A  join
Temp_Claimed as B

ON A.INSTALLATION_ID_link = B.INSTALLATION_ID
AND A.Meter_Read_Date <= B.[Meter_Read_Date(NEW)]
WHERE (A.claimed <> 1
OR A.claimed is null)
Order by INSTALLATION_ID_link


-- Once you've confirmed the data by running the select above, run the script below to update the Claimed Field with a '1'
/*

Update Readings 
Set Claimed = 1

from READINGS as A  join
Temp_Claimed as B

ON A.INSTALLATION_ID_link = B.INSTALLATION_ID
AND A.Meter_Read_Date <= B.[Meter_Read_Date(NEW)]
WHERE (A.claimed <> 1
OR A.claimed is null)
*/


-- select distinct meter_read_reason, count(meter_read_reason) as Count_Reasons  from READINGS WHERE (claimed <> 1 OR claimed is null) Group by Meter_read_reason