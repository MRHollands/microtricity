/*
Server:		uh-gendb-01
Database:	Microtricity2
Author:  Matthew Hollands
Date Last Modified:  28/08/2018
*/
/*  

1.  Delete Temp_includedinlevelisation  (To clear previous Lev data).
2.  Upload data from previous FxQx Levelisation Payment Data.xlsx  (Levelisation_Generator__With_Ca tab). 
3.	Use this select script to confirm you are selecting the right data.

*/

select distinct
I.FiT_ID_Initial,
A.READ_ID,
A.INSTALLATION_ID_link, 
A.Meter_Read_Date,
A.Meter_Read_Reason,
A.Meter_Read_Type,
A.IncludedInLevelisation
from READINGS as A  join
Temp_includedinlevelisation as B

ON A.INSTALLATION_ID_link = B.Installation_ID
AND A.Meter_Read_Date <= B.[Meter_Read_Date(NEW)]
inner join installation I on A.INSTALLATION_ID_link = I.INSTALLATION_ID

WHERE (A.IncludedInLevelisation <> 1
OR A.IncludedInLevelisation is null)
Order by INSTALLATION_ID_link


-- Once you've confirmed the data by running the select above, run the script below to update the Claimed Field with a '1'
/*
Update Readings 
Set IncludedInLevelisation = 1

from READINGS as A join
Temp_includedinlevelisation as B

ON A.INSTALLATION_ID_link = B.Installation_ID
AND A.Meter_Read_Date <= B.[Meter_Read_Date(NEW)]
WHERE A.IncludedInLevelisation <> 1
OR A.IncludedInLevelisation is null  

*/