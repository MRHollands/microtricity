
/*
Server:		uh-gendb-01
Database:	Microtricity2
Author:		Matthew Hollands
Date:		09/07/18
Query:		This query gives the list of readings which need to be duplicated as FxQx reads for Levelisation. These are only customers who are not on Standard Export and with 
			2 Year Audit or Interim reads which are between the dates provided by Microtricity.

To Do:		If there is an unpaid 2 Year Audit AND an Interim read this brings both back.  Manually removing at the moment, but the query should only bring back the latest read in this scenario

*/

SET DATEFORMAT DMY;
DECLARE @Start_Date AS DATE, @End_Date AS DATE, @Fit_Period AS VARCHAR(4);
SET @Start_Date = '01/04/2018';
SET @End_Date = '30/06/2018';
SET @Fit_Period = 'F9Q1';
SELECT *
FROM dbo.readings R
     INNER JOIN dbo.INSTALLATION I ON R.INSTALLATION_ID_link = I.INSTALLATION_ID
     LEFT JOIN
(
    SELECT INSTALLATION_ID_link
    FROM dbo.readings
    WHERE Meter_Read_Reason = @Fit_Period
) R1 ON R.installation_id_link = R1.installation_id_link
     INNER JOIN
(
    SELECT INSTALLATION_ID_link,
           MAX(Meter_Read_Date) AS Max_Date
    FROM dbo.readings
    GROUP BY INSTALLATION_ID_link,
             Meter_Read_Reason
    HAVING Meter_Read_Reason IN('2 Year Audit', 'Interim')
) R2 ON R.installation_id_link = R2.installation_id_link
        AND R.Meter_Read_Date = R2.Max_Date
WHERE I.Export_Status <> 'Standard Tariff'
      AND R1.INSTALLATION_ID_link IS NULL
      AND R.Meter_Read_Date BETWEEN @Start_Date AND @End_Date
      AND R.IncludedInLevelisation IN(0, NULL)
ORDER BY I.INSTALLATION_ID,
         R.Meter_Read_Date;


