 insert into Readings_Archive
 (
 [FiT_ID_Initial]
      ,[READ_ID]
      ,[INSTALLATION_ID_link]
      ,[Meter_Read_Type]
      ,[Meter_Read_Date]
      ,[Meter_Read_Reason]
      ,[Generation_Read]
      ,[Export_Read]
      ,[IncludedInLevelisation]
      ,[Gen_Validation]
      ,[Modified_By]
      ,[Date_Modified]
      ,[Time_Modified]
      ,[Claimed]
      ,[Generation_Clocked]
      ,[Export_Clocked]
      ,[Notes]
      ,[MSN]
      ,[Modified_by2]
      ,[Created_by2]
      ,[Created_date2]
      ,[Modified_date2]
      ,[ESP_Billed]
      ,[Recieved_Method]
      ,[Photo]
      ,[Export_MSN]
      ,[SSMA_TimeStamp]
	  )
 
Select 
I.FiT_ID_Initial
 ,[READ_ID]
      ,[INSTALLATION_ID_link]
      ,[Meter_Read_Type]
      ,[Meter_Read_Date]
      ,[Meter_Read_Reason]
      ,[Generation_Read]
      ,[Export_Read]
      ,[IncludedInLevelisation]
      ,[Gen_Validation]
      ,R.[Modified_By]
      ,R.[Date_Modified]
      ,R.[Time_Modified]
      ,[Claimed]
      ,[Generation_Clocked]
      ,[Export_Clocked]
      ,[Notes]
      ,[MSN]
      ,R.[Modified_by2]
      ,R.[Created_by2]
      ,R.[Created_date2]
      ,R.[Modified_date2]
      ,[ESP_Billed]
      ,[Recieved_Method]
      ,[Photo]
      ,R.[Export_MSN]
      ,R.[SSMA_TimeStamp]

from Microtricity2.dbo.readings R

              left JOIN  Microtricity2.dbo.INSTALLATION I

                     ON R.installation_id_link = I.INSTALLATION_ID

where (msn is null  or msn =  '') and  I.Installation_id is null


