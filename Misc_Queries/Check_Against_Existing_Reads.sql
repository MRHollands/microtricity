SELECT        
READ_VALIDATION.FiT_ID, 
Meter_Read_Date, 
READ_VALIDATION.New_Read_Date, 
Meter_Read_Reason, 
READ_VALIDATION.New_Read_Reason, 
Generation_Read, 
READ_VALIDATION.New_Gen_Read, 

CASE 
	WHEN Meter_Read_Date = New_Read_Date THEN 'Duplicate meter read date' 
	WHEN Meter_Read_Reason = New_Read_Reason THEN 'Duplicate meter read reason' 
	WHEN Generation_Read = New_Gen_Read THEN 'Duplicate meter read' 
	WHEN Fit_ID_Initial Is Null THEN 'FIT ID not recognised'
	WHEN Generation_MSN <> READ_VALIDATION.MSN THEN 'MSN & FIT ID Mismatch'
	WHEN Account_Status <> 'Registered Live' THEN 'Account Status Error'
	WHEN Move_Out <> '9999-12-31 00:00:00.000' THEN 'Move Out Date Error'
	WHEN Export_Status <> 'Standard Tariff' and READ_VALIDATION.New_Exp_Read IS NOt null OR READ_VALIDATION.New_Exp_Read <> 0 THEN 'Export Read added Error'
	WHEN Export_Status = 'Standard Tariff' and READ_VALIDATION.New_Exp_Read IS null OR READ_VALIDATION.New_Exp_Read = 0 THEN 'No Export Read added Error'
	WHEN READ_VALIDATION.New_Read_Date > GETDATE() THEN 'Future Meter Read Date'
	ELSE NULL 
END AS Check_Reads, 

CASE WHEN BP_ID_Link = 11174 AND Contract.Third_party = Read_Validation.Fund THEN 'ASG Fund Error' 
ELSE NULL 
END AS Check_ASG, 

CASE 
	WHEN Round((((New_Gen_Read - Generation_Read) / NULLIF (Datediff(Day, Meter_Read_Date, CONVERT(date, new_read_date)), 0)) / Total_Installed_Capacity_kW), 0) >= 20 THEN 'Hours Per Day too high' 
	WHEN Round((((New_Gen_Read - Generation_Read) / NULLIF (Datediff(Day, Meter_Read_Date, CONVERT(date, new_read_date)), 0)) / Total_Installed_Capacity_kW), 0) < 0 THEN 'Hours Per Day too low' 
	ELSE NULL 
END AS HrsPerDayCheck, 

ROUND((READ_VALIDATION.New_Gen_Read - Generation_Read) / NULLIF (DATEDIFF(Day, Meter_Read_Date, CONVERT(date, READ_VALIDATION.New_Read_Date)), 0) / Total_Installed_Capacity_kW, 0) AS HrsPerDay


FROM
READ_VALIDATION 
LEFT JOIN Installation on Read_Validation.FiT_ID = Installation.FiT_ID_Initial
Left Join Readings on installation.INSTALLATION_ID = readings.INSTALLATION_ID_link
Left Join (Select INSTALLATION_ID_link, Account_Status, Move_Out, bp_id_link, Third_Party, ROW_NUMBER() over(PARTITION by installation_id_link order by move_in desc) as RN  from Contract) Contract on installation.INSTALLATION_ID = contract.INSTALLATION_ID_link and RN = 1
LEFT JOIN
	(SELECT  INSTALLATION_ID_link, MAX(Meter_Read_Date) AS MaxDate FROM READINGS AS READINGS_1	GROUP BY INSTALLATION_ID_link) AS R2 
	ON INSTALLATION.INSTALLATION_ID = R2.INSTALLATION_ID_link AND READINGS.Meter_Read_Date = R2.MaxDate

WHERE
((Read_id is null and R2.INSTALLATION_ID_link is null) OR (R2.INSTALLATION_ID_link is not null))
AND
(
Meter_Read_Date = READ_VALIDATION.New_Read_Date OR
Meter_Read_Reason = READ_VALIDATION.New_Read_Reason OR
Generation_Read = READ_VALIDATION.New_Gen_Read OR
FiT_ID_Initial IS NULL OR
Generation_MSN <> READ_VALIDATION.MSN OR
Account_Status <> 'Registered Live' OR
Move_Out <> '9999-12-31 00:00:00.000' OR
(Export_Status <> 'Standard Tariff' and READ_VALIDATION.New_Exp_Read IS NOt null OR READ_VALIDATION.New_Exp_Read <> 0) OR
(Export_Status = 'Standard Tariff' and READ_VALIDATION.New_Exp_Read IS null OR READ_VALIDATION.New_Exp_Read = 0) OR
READ_VALIDATION.New_Read_Date > GETDATE() OR
(ROUND((READ_VALIDATION.New_Gen_Read - Generation_Read) / NULLIF (DATEDIFF(Day, Meter_Read_Date, CONVERT(date, READ_VALIDATION.New_Read_Date)), 0) / Total_Installed_Capacity_kW, 0) >= 20) OR
(ROUND((READ_VALIDATION.New_Gen_Read - Generation_Read) / NULLIF (DATEDIFF(Day, Meter_Read_Date, CONVERT(date, READ_VALIDATION.New_Read_Date)), 0) / Total_Installed_Capacity_kW, 0) < 0)
)



