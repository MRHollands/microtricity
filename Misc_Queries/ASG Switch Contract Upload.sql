/*
DO THIS FIRST:

Select * from TEMP_Contract


Check the number of results is the same as the upload you are currently doing.  If not, delete entire contents of TEMP_Contract and import again.



*/


insert into contract
(BP_ID_link,
Third_Party,
Third_Party_Ref,
Terms_and_Conditions_Sent_Out_Date,
Terms_and_Conditions_Agreed_Date,
Account_Status,
VAT_Invoice_Self_Billing,
INSTALLATION_ID_link,
Move_Out,
Proof_of_Ownership_Received,
Payment_Method,
Vat_Inv_Rqd,
Bank_Acc_Name,
Sort_Code,
Bank_Acc_Number)

select 
11174,
temp_contract.Third_Party,
TEMP_Contract.Third_Party_Ref,
TEMP_Contract.Terms_and_Conditions_Sent_Out_Date,
TEMP_Contract.Terms_and_Conditions_Agreed_Date,
TEMP_Contract.Account_Status,
TEMP_Contract.VAT_Invoice_Self_Billing,
temp_Contract.Installation_ID_Link,
temp_contract.Move_Out,
temp_contract.Proof_of_Ownership_Received,
temp_contract.Payment_Method,
temp_contract.VAT_Inv_Rqd,
bank_details.Bank_Acc_Name,
bank_details.Sort_Code,
bank_details.Bank_Acc_Number
	
from TEMP_Contract 
Left join Bank_Details on temp_contract.Third_Party = Bank_Details.Third_Party

