Declare @FIT as Varchar(4), @StartDate as Date, @EndDate as Date

Set @FIT = 'F8Q2'
Set @StartDate = '01Jul2017'
Set @EndDate = '04Oct2017'

select 
FiT_ID_Initial,
R1.* 

FROM 
INSTALLATION 
inner join (Select Distinct * from READINGS where Meter_Read_Reason <> @FIT 
													AND  IncludedInLevelisation = 0 
													AND Meter_Read_Date >= @StartDate and Meter_Read_Date <= @EndDate) R1 on INSTALLATION.INSTALLATION_ID = R1.INSTALLATION_ID_link

Left join (Select Distinct  * from READINGS where Meter_Read_Reason = @FIT) R2 on INSTALLATION.INSTALLATION_ID = R2.INSTALLATION_ID_link

Left join (Select INSTALLATION_ID_link, MAX(Meter_Read_Date) AS Max_Date from READINGS where Meter_Read_Reason <> @FIT 
																							AND  IncludedInLevelisation = 0 Group By INSTALLATION_ID_link) R3 on R1.INSTALLATION_ID_link = R3.INSTALLATION_ID_link AND R1.Meter_Read_Date = R3.Max_Date
inner join CONTRACT on CONTRACT.INSTALLATION_ID_link = INSTALLATION.INSTALLATION_ID

WHERE 
R1.Generation_Read is not null
AND R2.Meter_Read_Reason is null 
AND R3.INSTALLATION_ID_link is not null
AND Account_Status = 'Registered Live'
AND R1.Meter_Read_Date Between CONTRACT.Move_In and CONTRACT.Move_Out
AND R1.Meter_Read_Reason in ('2 Year Audit','Interim')


ORDER BY
Meter_Read_Reason



