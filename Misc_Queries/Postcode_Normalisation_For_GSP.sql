IF OBJECT_ID('tempdb.dbo.##Postcode_Temp', 'U') is not null
  DROP TABLE ##Postcode_Temp

Create Table ##Postcode_Temp (
Distribution_Network nvarchar(255), 
Area  nvarchar(255),
Area_ID  nvarchar(255),
Postcode  nvarchar(255))

/* Adds the data from dbtmgmtexport into the temp table which is identified by having a 7 digit Cont_Account */
INSERT INTO ##Postcode_Temp
select A.[Distribution Network ],A.Area,A.[Area ID],A.PostCode + ISNULL(A.[42],'')
from TEMP_MATT A inner join TEMP_MATT B on A.id = b.id
INSERT INTO ##Postcode_Temp
select A.[Distribution Network ],A.Area,A.[Area ID],A.PostCode + ISNULL(A.[43],'')
from TEMP_MATT A inner join TEMP_MATT B on A.id = b.id
INSERT INTO ##Postcode_Temp
select A.[Distribution Network ],A.Area,A.[Area ID],A.PostCode + ISNULL(A.[44],'')
from TEMP_MATT A inner join TEMP_MATT B on A.id = b.id
INSERT INTO ##Postcode_Temp
select A.[Distribution Network ],A.Area,A.[Area ID],A.PostCode + ISNULL(A.[45],'')
from TEMP_MATT A inner join TEMP_MATT B on A.id = b.id
INSERT INTO ##Postcode_Temp
select A.[Distribution Network ],A.Area,A.[Area ID],A.PostCode + ISNULL(A.[46],'')
from TEMP_MATT A inner join TEMP_MATT B on A.id = b.id
INSERT INTO ##Postcode_Temp
select A.[Distribution Network ],A.Area,A.[Area ID],A.PostCode + ISNULL(A.[47],'')
from TEMP_MATT A inner join TEMP_MATT B on A.id = b.id
INSERT INTO ##Postcode_Temp
select A.[Distribution Network ],A.Area,A.[Area ID],A.PostCode + ISNULL(A.[48],'')
from TEMP_MATT A inner join TEMP_MATT B on A.id = b.id
INSERT INTO ##Postcode_Temp
select A.[Distribution Network ],A.Area,A.[Area ID],A.PostCode + ISNULL(A.[49],'')
from TEMP_MATT A inner join TEMP_MATT B on A.id = b.id
INSERT INTO ##Postcode_Temp
select A.[Distribution Network ],A.Area,A.[Area ID],A.PostCode + ISNULL(A.[50],'')
from TEMP_MATT A inner join TEMP_MATT B on A.id = b.id
select * from ##Postcode_Temp

