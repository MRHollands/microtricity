(SELECT	Bulk_COT_ID,
		Generation_Read
FROM 
(


SELECT DISTINCT
FIT_ID + CAST(New_Reading_Date AS nvarchar) AS  Bulk_COT_ID, 
INSTALLATION_ID_link, 
Max(Meter_Read_Date) AS LRead

FROM dbo.READINGS 
inner join installation on dbo.readings.INSTALLATION_ID_link = installation.INSTALLATION_ID 
inner join Bulk_COT on installation.FiT_ID_Initial = Bulk_COT.FIT_ID

GROUP BY FIT_ID + CAST(New_Reading_Date AS nvarchar),  INSTALLATION_ID_link,Meter_Read_Date,New_Reading_Date

HAVING Meter_Read_Date < Bulk_COT.New_Reading_Date




) LR
INNER JOIN READINGS R on LR.INSTALLATION_ID_link = R.INSTALLATION_ID_link AND LR.LRead = R.Meter_Read_Date)