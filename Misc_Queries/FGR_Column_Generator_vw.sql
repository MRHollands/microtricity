-- Server uh-etrmdbd-01
-- 03/05/2018
-- This query is currently under development.


USE [FGR]
GO

-- This CTE is just to find the last Minute of the First half.  This is then added to the Second half Calc to create the Min in 90 column
With F_h_e as (
select Matchname, [Min in Half] AS First_Half_End, ROW_NUMBER() over (partition by matchname order by [min in half] Desc) as rn
from [League2DataRaw_MH_Test]
Where Half = 'First half')





Select
[One],
[Number],
Data.[Matchname],
[Half],
[Time],
[Min in Half],
CASE 
	WHEN Half = 'First Half' THEN [Min in Half]
	WHEN half = 'Second Half' THEN [Min in Half] + F_h_e.first_half_End
	ELSE NULL
END AS [Min in 90],
Min_in_90_old,
[EventName],	
[Player1Name],
[Player1ClubName],
[Player2Name],
[Player2Clubname],
[XPosOrigin],
[YPosOrigin],
[XPosDest],
[YPosDest],
[Rotation],
[X (On-Pitch) ORIGIN],
[Y (On-Pitch) ORIGIN],
[Stage 2 ORIGIN],
[Stage 2 ORIGIN1],
[X (Left to Right) ORIGIN],
[Y (Left to Right) ORIGIN],
[X (on pitch) DEST],
[Y (on pitch) DEST],
[Stage 2 DEST],
[Stage 2 DEST1],
[X (left to right) DEST],
[Y (Left to right) DEST],
[X (left to right) DEST] AS Xspecial,
Yspecial,
dbo.fnfirstinitials(Player1Name) AS Initials,
dbo.fnfirstinitials(EventName) AS [Event Initials],
[Goal?],
CASE 
	WHEN (EventName = 'goal' AND [Player1ClubName] = [Home Team] AND Data.Matchname = Lag(Data.Matchname) OVER (partition by Data.matchname order by Data.matchname,number)) THEN (Lag(FGR_Game_State_Calc,1,0) OVER (partition by Data.matchname order by Data.matchname,number))+1
	WHEN EventName = 'goal' THEN [FGR_Game_State_Calc] -1
	ELSE [FGR_Game_State_Calc]  
END AS [FGR Game State], -- NOT WORKING
[Key Pass OP/SP?],
[Shot from Corner?],
[Pass?],
CASE	WHEN [pass?] = 1 and [Distance] > 25 THEN 1	ELSE 0 END AS [Long Pass?],
CASE	WHEN [pass?] = 1 and [Distance] <=10 THEN 1	ELSE 0 END AS [Short Pass?],
CASE	WHEN [pass?] = 1 and [Distance] between 10 and 25 THEN 1	ELSE 0 END AS [Medium Pass?],
[Passes Recieved]
	  	
FROM
(SELECT
[One],
[Number],
[Matchname],
[Half],
[Time],
[Time]/60 AS [Min in Half],
[Min in 90] AS Min_in_90_old,
[EventName],
[Player1Name],
[Player1ClubName],
[Player2Name],
[Player2Clubname],
[XPosOrigin],
[YPosOrigin],
[XPosDest],
[YPosDest],
[Rotation],
[XPosOrigin] AS [X (On-Pitch) ORIGIN],
[YPosOrigin]*-1 AS [Y (On-Pitch) ORIGIN],
CASE WHEN [Rotation] = 180 THEN [XPosOrigin] *-1 ELSE [XPosOrigin] END AS [Stage 2 ORIGIN],
CASE WHEN [Rotation] = 180 THEN [YPosOrigin] *-1 ELSE [YPosOrigin] END AS [Stage 2 ORIGIN1],
CASE WHEN [Half] = 'First Half' THEN [XPosOrigin] *-1 ELSE [XPosOrigin] END AS [X (Left to Right) ORIGIN],
CASE WHEN [Half] = 'First Half' THEN [YPosOrigin] *-1 ELSE [YPosOrigin] END AS [Y (Left to Right) ORIGIN],
[XPosDest] AS [X (on pitch) DEST],
[YPosDest] AS [Y (on pitch) DEST],
CASE WHEN [Rotation] = 180 THEN [XPosDest] *-1 ELSE [XPosDest] END AS [Stage 2 DEST],
CASE WHEN [Rotation] = 180 THEN [YPosDest] *-1 ELSE [YPosDest] END AS [Stage 2 DEST1],
CASE WHEN [Half] = 'First Half' THEN [XPosDest] *-1 ELSE [XPosDest] END AS [X (left to right) DEST],
CASE WHEN [Half] = 'First Half' THEN [YPosDest] *-1 ELSE [YPosDest] END AS [Y (Left to right) DEST],
CASE 
	WHEN [Half] = 'First Half' AND [Rotation] = 180 THEN [Y (on pitch) DEST]
	WHEN [Half] = 'Second Half' AND [Rotation] = 180 THEN [Y (on pitch) DEST]*-1
	WHEN [Half] = 'First Half' AND [Rotation] = 0 THEN [Y (on pitch) DEST]*-1
ELSE [Y (on pitch) DEST]
END AS Yspecial,
CASE 
	WHEN (EventName in ('post','crossbar')) AND (Lead(EventName) OVER(partition by matchname order by Matchname,Number)) = 'goal' THEN 0
	WHEN ((Lead(EventName) OVER(partition by matchname order by Matchname,Number)) = 'post' or (Lead(EventName) OVER(partition by matchname order by Matchname,Number)) = 'crossbar') AND (Lead(EventName,2) OVER(partition by matchname order by Matchname,Number)) = 'goal' THEN 1
	WHEN (EventName in ('shot','header shot','penalty shot','direct free kick shot','post','crossbar')) AND ((Lead(EventName) OVER(partition by matchname order by Matchname,Number)) = 'goal' OR (Lead(EventName,2) OVER(partition by matchname order by Matchname,Number)) = 'goal') THEN 1	
	ELSE 0
END AS [Goal?],
CASE
	WHEN Matchname <> (Lag(Matchname,1,0) OVER(partition by matchname order by matchname,number))  THEN 0
	WHEN (EventName = 'goal' AND [Player1ClubName] = [Home Team] AND Matchname = Lag(Matchname) OVER (partition by matchname order by matchname,number)) THEN 1
	ELSE 0
END AS [FGR_Game_State_Calc],
[FGR Game State] AS [FGR Game State Old],
[Home Team],
CASE
	WHEN [Key Pass Type] in ('corner cross','corner pass','direct free kick cross','direct free kick pass','direct free kick shot','Indirect free kick pass','Throw in') THEN 0
	ELSE 1
END AS [Key Pass OP/SP?],
CASE 
	WHEN [Key Pass Type] = 'corner cross' THEN 1
	ELSE 0
END AS [Shot from Corner?],
CASE
	WHEN EventName in ('pass','goalkeeper kick','goal kick','goalkeeper throw') THEN 1
	ELSE 0
END AS [Pass?],
[Distance],
CASE
	WHEN lag(eventname,1,'') over(partition by matchname order by matchname,number) = 'Pass' AND lag(Player1ClubName,1,'') over(partition by matchname order by matchname,number) = Player1ClubName THEN 1
	ELSE 0
END AS [Passes Recieved]

/*
,[Passes Received]
,[Header Shot?]
,[Penalty?]
,[Rebound?]
,[Key Pass Type]
,[Key PASS Distance]
,[Assist Type]
,[Unintentional Assist?]
,[Key Pass/Assist Distance]
,[Successful Through Ball?]
,[Dribble Distance]
,[Lost Possession (All Times)]
,[Lost Possession (Passing etc)]
,[Regained Possession/ Interception]
,[Tackle?]
,[xG pos]
,[xG GS]
,[WillTGM]
,[Eastwood xG]
,[Combined xG avgs.]
,[Key Pass (Pass)]
,[Key Pass (All)]
,[xG of Key Pass (All)]
,[None]
,[Cross]
,[Successful Cross]
,[Cross-to-shot conversion]
,[Shot]
,[DZ Shot]
,[Cross led to shot]
,[Distance]
,[Pass Angle]
,[Forwards Pass?]
,[45 Deg Passes]
,[Successful Pass?1That let to shot2]
,[Successful Pass?1Thru Ball2]
,[Successful? (team=team)]
,[Successful Pass]
,[Xg Pos Reversed]
,[xG of Pass End Point]
,[xG Added of Pass]
,[Distance to goal (Strt Pt)]
,[Distance to goal (End Pt)]
,[Angle to Goal]
,[Relative angle to goal]
,[FGR Cumulative xG]
,[Oppo Cumulative xG]
,[Pass Into 1/3rd]
,[Successful Pass into 1/3]
,[Pass Into Box]
,[Cross into Box]
,[Set Piece Into Box]
,[Combined Into Box]
,[Successful Pass Into Box]
,[Successful Cross Into Box]
,[Successful Set Piece Into Box]
,[Successful Combined Into Box]
,[Final 1/3 Retained Possession]
,[Final 1/3rd Possession String]
,[Possession line]
,[Occasion]
,[xG during Possession String]
,[Keeper Distribution]
,[Saves from own G.Kick]
,[Chances from own G.Kick]
,[FGR Pass From Link Zone]
,[FGR Pass Dist From Link Zone]
,[FGR Pass Into Link Zone]
,[FGR Pass Dist Into Link Zone]
,[Opp Pass From Link Zone]
,[Opp Pass Dist From Link Zone]
,[Opp Pass Into Link Zone]
,[Opp Pass Dist Into Link Zone]
,[Link Zone Thruball]
,[Ball Out of Play Time]
,[Retained Regain (2 pass)]
,[Throw-in Kept Possession]
,[Width deployed]
,[Switch From Throw in]
,[Possession]
,[Distance Covered W Ball WRONG]
,[Position]
,[CM Pass to Wide]
,[Territory]
,[Time in Territory]
,[In My Territory?]
,[Direct Striker Linkup]
,[Indirect Striker Linkup]
,[Pass to Wide?]
,[Pass From Wide?]
,[Pass Start]
,[Pass End]
,[Pass Aggressiveness]
,[OP/SP Event?]
,[After Block]
,[Regained Possession in My Territory]
,[In My Territory?1]
,[Assist?]
,[Pass to Feet In Box]
,[Pass From Phase 1]
,[Pass From Phase 2]
,[Pass From Phase 3]
,[Successful Pen Area Entry (Any)]
,[Pass Receiver]
,[Pass Sender]
,[Pass From CAM]
,[Touch?]
,[Pass]
,[Touches (& Passes) in Box]
,[Avg. Shot Distance]
,[Touches (& Passes) in DZ]
,[GK Save]
,[Pass From BIG Lkzn]
,[Pass Into BIG lkzn]
,[FGR Pass From BIG lkzn]
,[FGR Pass Into BIG lkzn]
,[Pass From BIG lkzn Distance]
,[Pass Into BIG lkzn Distance]
,[Successful Pass From BIG lkzn]
,[Successful Pass Into BIG lkzn]
,[From LZ Pass Aggressiveness]
,[Sideways Pass]
,[Retained Possession]
,[Number of Passes in Possession]
,[Pass Number in Possession]
,[Time (Secs) Per Possession]
,[Vertical Distance per Possession]
,[Average Vertical Pass Distance per Possession]
,[Metres per Second]
,[X of Dribble]
,[Y of Dribble]
,[Part of Dribble?]
,[Distance Carried]
,[Player Seperator]
,[Part of Dribble Real]
,[Position of Event]
,[Shot From Beyond Box]
,[Start of A Dribble]
,[End of a Dribble]
,[Dribble Directness (m's forwards per m dribbled)]
,[Successful End of Dribble?]
,[Successful End of Dribble in Box]
,[2nd Assist]
,[Carry X]
,[Carry Y]
,[Substitution]
,[Deep Completions]
,[Very Deep Completions]
,[Deep Completions Left]
,[Deep Completions Centre]
,[Deep Completions Right]
,[Touches in LZ]
,[FGR Touches and Passes from LZ]
,[Opp Touches and Passes from LZ]
,[FGR Kept Possession]
,[FGR Kept Possession String]
,[xG of Set Piece shots]
,[Opp Kept Possession]
,[Opp Kept Possession String]
,[Pass From LZ Into Box]
,[Key Key Pass]
,[Touch In Box]
,[Position of Event Destination]
,[Time To Regain Ball]
,[Gave Possession to Opposition]
,[UnSuc. Long Straight Forwards Pass]
,[Minutes in Match]
,[Time of Substitution]
,[Mins Left After Sub]
,[Minutes Played]
,[Spare1]
,[Spare2]
,[Season Year]
,[league]
,[matchid]
,[Home Team]
,[Away Team]
,[Against Team]
,[oldxg]
,[xg1pos]
,[[xG Added Carry]
,[Pass after dribble xg]
,[30secsbeforegoal]
,[20secsbeforegoal]
,[10secsbeforegoal]

*/
FROM [dbo].[League2DataRaw_MH_Test]) DATA
inner join F_h_e on data.Matchname = F_h_e.Matchname and F_h_e.rn = 1



