select distinct
Reads.READ_ID,
Reads.INSTALLATION_ID_link,
Reads.Meter_Read_Type,
Reads.Meter_Read_Date,
Reads.Meter_Read_Reason,
Reads.Generation_Read,
Reads.Export_Read,
Reads.IncludedInLevelisation,
Reads.Claimed,
Reads.MSN,
Reads.Export_MSN,
Initial_System_Tariff_Rate,
Initial_System_Tariff_Rate_Price = R1.Price,
Initial_System_Export_Rate,
Initial_System_Export_Rate_Price = R2.Price,
Extension_1_Tariff_Rate,
Extension_1_Tariff_Rate_Price = R3.Price,
Extension_1_Export_Rate,
Extension_1_Export_Rate_Price = R4.Price,
Extension_2_Tariff_Rate,
Extension_2_Tariff_Rate_Price = R5.Price,
Extension_2_Export_Rate,
Extension_2_Export_Rate_Price = R5.Price	
from 
readings Reads 
inner join installation I on Reads.INSTALLATION_ID_link = I.installation_id

left join Rates R1 on R1.Tariff_Code = I.Initial_System_Tariff_Rate and Reads.Meter_Read_Date between R1.Valid_From and R1.Valid_To
left join Rates R2 on R2.Tariff_Code = I.Initial_System_Export_Rate and Reads.Meter_Read_Date between R2.Valid_From and R2.Valid_To

left join Rates R3 on R3.Tariff_Code = I.Extension_1_Tariff_Rate and Reads.Meter_Read_Date between R3.Valid_From and R3.Valid_To
left join Rates R4 on R4.Tariff_Code = I.Extension_1_Export_Rate and Reads.Meter_Read_Date between R4.Valid_From and R4.Valid_To

left join Rates R5 on R5.Tariff_Code = I.Extension_2_Tariff_Rate and Reads.Meter_Read_Date between R5.Valid_From and R5.Valid_To
left join Rates R6 on R6.Tariff_Code = I.Extension_2_Export_Rate and Reads.Meter_Read_Date between R6.Valid_From and R6.Valid_To


