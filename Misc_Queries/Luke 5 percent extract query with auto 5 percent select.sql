Select * 
from

(select
EC.Account_Status, 
EC.SAP_BP,
EC.SAP_CA,
EC.First_Name,
EC.Last_Name,
EC.Email_Address,
EC.[Address],
EC.Address_2,
EC.Address_3,
EC.City,
EC.County,
EC.PostCode,
EC.Payment_Method,
EI2.Total_Amount_Due As Total_Invoiced_Amount,
DB1.Sum_Amount AS Oustanding_Balance,
CASE
WHEN EI.MAX_Inv_Date IS null then 'Unbilled'
WHEN DateDiff(DAY,EI.MAX_Inv_Date,getdate()) > 80 THEN 'Not Billed in Last Quarter'
ELSE 'Billed'
END AS Invoice_Sent
from ESP_Contract EC 
Left join (Select Contract_Account, MAX(Invoice_Date) AS Max_Inv_Date from ESP_Invoicing Group by Contract_Account) EI on EC.SAP_CA = EI.Contract_Account
Left join (Select Contract_Account, SUM(Total_Amount_DUE) AS Total_Amount_Due  from ESP_Invoicing Group by Contract_Account) EI2 on EC.SAP_CA = EI2.Contract_Account
Left Join (Select Cont_Account, SUM(Amount) as Sum_Amount from DatSup.dbo.DBTMGMTEXPORT Group by Cont_Account) DB1     on EC.sap_ca = DB1.Cont_Account
) ESP_Data


Order by Invoice_Sent ASC, Oustanding_Balance DESC