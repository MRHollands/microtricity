SET DATEFORMAT DMY;

DECLARE @Start_Date AS DATE, @End_Date AS DATE;
SET @Start_Date = '01/04/2017';
SET @End_Date = '31/03/2018';

SELECT fit_id_initial,
       'E' AS Read_Type,
       CASE WHEN MX.INSTALLATION_ID_link is not null THEN 1 ELSE NULL END AS Meter_Change,
       CASE WHEN CAST(Move_Out as varchar) <>'Dec 31 9999 12:00AM' THEN 1 ELSE NULL END AS Installation_No_Longer_Live,
       CASE
           WHEN FiT_ID_Ext1 IS NOT NULL
           THEN 1
           ELSE NULL
       END AS Meter_Covers_More_Than_One_Installation,
       Meter_read_reason,
       fit_id_ext1,
       fit_id_ext2,
       ROW_NUMBER() OVER(PARTITION BY readings.installation_id_link ORDER BY readings.meter_read_date ASC) AS Row_num,
       Fit_id_initial + CAST(ROW_NUMBER() OVER(PARTITION BY readings.installation_id_link ORDER BY readings.meter_read_date ASC) AS VARCHAR) AS [Lookup],
       Meter_read_date,
       export_read
FROM installation
     INNER JOIN contract ON installation.installation_id = contract.installation_id_link
     INNER JOIN
(
    SELECT installation_id_link,
           meter_read_date,
           meter_read_reason,
           export_read
    FROM readings
    WHERE Meter_Read_Date BETWEEN @Start_Date AND @End_Date
) Readings ON readings.installation_id_link = installation.installation_id
     Left JOIN
(
    SELECT installation_id_link
    FROM readings
    WHERE Meter_Read_Date BETWEEN @Start_Date AND @End_Date and Meter_Read_Reason = 'Final (MX)'
) MX ON MX.installation_id_link = installation.installation_id



WHERE export_read IS NOT NULL;

SELECT fit_id_initial,
       'G' AS Read_Type,
       CASE WHEN MX.INSTALLATION_ID_link is not null THEN 1 ELSE NULL END AS Meter_Change,
       CASE WHEN CAST(Move_Out as varchar) <>'Dec 31 9999 12:00AM' THEN 1 ELSE NULL END AS Installation_No_Longer_Live,
       CASE
           WHEN FiT_ID_Ext1 IS NOT NULL
           THEN 1
           ELSE NULL
       END AS Meter_Covers_More_Than_One_Installation,
       Meter_read_reason,
       fit_id_ext1,
       fit_id_ext2,
       ROW_NUMBER() OVER(PARTITION BY readings.installation_id_link ORDER BY readings.meter_read_date ASC) AS Row_num,
       Fit_id_initial + CAST(ROW_NUMBER() OVER(PARTITION BY readings.installation_id_link ORDER BY readings.meter_read_date ASC) AS VARCHAR) AS [Lookup],
       Meter_read_date,
       generation_read
FROM installation
     INNER JOIN contract ON installation.installation_id = contract.installation_id_link
     INNER JOIN
(
    SELECT installation_id_link,
           meter_read_date,
           meter_read_reason,
           Generation_Read
    FROM readings
    WHERE Meter_Read_Date BETWEEN @Start_Date AND @End_Date
) Readings ON readings.installation_id_link = installation.installation_id
     Left JOIN
(
    SELECT installation_id_link
    FROM readings
    WHERE Meter_Read_Date BETWEEN @Start_Date AND @End_Date and Meter_Read_Reason = 'Final (MX)'
) MX ON MX.installation_id_link = installation.installation_id
WHERE Generation_Read IS NOT NULL;