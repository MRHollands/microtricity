/*  First move any FITs that are trying to be uploaded but which already exist into an exceptions table */
insert into Switch_Account_Upload_Exceptions
select Switch_Account_Upload.* from installation inner join Switch_Account_Upload on installation.FiT_ID_Initial = Switch_Account_Upload.fit_id_initial

delete Switch_Account_Upload from installation inner join Switch_Account_Upload on installation.FiT_ID_Initial = Switch_Account_Upload.fit_id_initial

/* Second - Create the Installation Records */

insert into INSTALLATION
(fit_id_initial,
Type_of_Installation,
Generation_Type,
Export_Status,
Generation_Meter_Make_and_Model,
Generation_Meter_Model,
Remotely_Read_Enabled,
Address_1,	
Address_2,	
City,
Postal_Code,
Initial_System_Export_Rate)

select 
fit_id_initial,
Type_of_Installation,
Generation_Type,
Export_Status,
Generation_Meter_Make_and_Model,
Generation_Meter_Model,
Remotely_Read_Enabled,
Address_1,	
Address_2,	
City,
Postal_Code,
Initial_System_Export_Rate
from Switch_Account_Upload

/* Third - Update Switch_Account_Upload table with Installation ID's */

update Switch_Account_Upload
set installation_id_link = installation.INSTALLATION_ID
from installation inner join Switch_Account_Upload on installation.FiT_ID_Initial = Switch_Account_Upload.fit_id_initial

/* Fourth - Upload the Contract records */
insert into CONTRACT
(BP_ID_link,
Third_Party,
Third_Party_Ref,
Terms_and_Conditions_Sent_Out_Date,
Terms_and_Conditions_Agreed_Date,
Account_Status,
--VAT_Invoice_Self_Billing,
installation_id_link,
Move_Out,
Proof_of_Ownership_Received,
Payment_Method,
VAT_Inv_Rqd)
select
11174,
Third_Party,
Third_Party_Ref,
Terms_and_Conditions_Sent_Date,
Terms_and_Conditions_Agreed_Date,
Account_Status,
--VAT_Invoice_Self_Billing,
installation_id_link,
Move_Out,
Proof_of_Ownership_Received,
Payment_Method,
VAT_Inv_Rqd
from Switch_Account_Upload
