SELECT 
*
FROM

(SELECT
Title,
First_Name,
Last_Name,
Bank_Details.Company,
GeneratorID,
BP_Address_1,
BP_Postal_Code,
Move_In,
Move_Out,
Account_Status,
Tables_Combined.Third_Party,
Tables_Combined.Third_Party_Ref,
FiT_Main,
Generation_Type,
Export_Status,
Generation_MSN,
Total_Installed_Capacity_kW,
Remotely_Read_Enabled,
Export_Meter_Photo,
INSTALLATION_Address_1,
INSTALLATION_Address_2,
INSTALLATION_City,
INSTALLATION_County,
INSTALLATION_Postal_Code,
Supply_MPAN,
Confirmation_Date,
Latest_Two_Year_Read,
Failed_Visits.Count_Visits as Number_Of_Failed_Visits,
CASE WHEN Latest_Two_Year_Read is null THEN Confirmation_Date+730
ELSE Latest_Two_Year_Read+730
END AS Two_Year_Anniversary,
CASE 
WHEN Latest_Two_Year_Read is null AND DATEDIFF(day,Confirmation_Date,getdate()) > 730 THEN DATEDIFF(day,Confirmation_Date,getdate())-730
WHEN Latest_Two_Year_Read is not null AND DATEDIFF(day,Latest_Two_Year_Read,getdate()) >730 THEN DATEDIFF(day,Latest_Two_Year_Read,getdate()) -730
ELSE NULL
END AS Days_Overdue


FROM Tables_Combined 
inner join 
(select 
installation_id_link,
Max(meter_read_date) AS Latest_Two_Year_Read
from readings
where Meter_Read_reason in ('2 Year Audit','2 Year Audit (Physical)')
Group by INSTALLATION_ID_link,Meter_Read_Reason) R1
on Tables_Combined.INSTALLATION_ID = R1.INSTALLATION_ID_link

left join
(Select
Installation_ID,
Sum(Count_Visits) as Count_Visits
from
(select 
Installation_ID,
visit_date,
1 as Count_visits
from Failed_Visits
where visit_date between getdate()-730 and getdate()) Data
group by Installation_ID) Failed_Visits
on Tables_Combined.INSTALLATION_ID = Failed_Visits.Installation_ID

left join Bank_Details on Tables_Combined.Third_Party = Bank_Details.Third_Party

WHERE 
tables_combined.Third_Party is null
and Account_Status IN ('Registered Live','Suspended-2 Yr Read')) DATA
Where Days_Overdue is not null
order by FiT_Main



