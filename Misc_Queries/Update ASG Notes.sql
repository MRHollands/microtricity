update Annual_Lev_Notes
set Notes = 'Payment made outside of levelisation quarter due to incorrect fund transfer readings provided.  This was then claimed in quarter 4.'

from ASG_Totals inner join Annual_Lev_Notes on ASG_Totals.Contract_ID = Annual_Lev_Notes.Contract_ID
where Qtr = 3 and split =1