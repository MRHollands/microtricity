/*  This creates the Non-Split rows */
SELECT
[Payment_Type],[GeneratorID],[FiT_ID_Initial],[FiT_ID_Ext1],[FiT_ID_Ext2],[BP_ID],[CONTRACT_ID],[INSTALLATION_ID],[Initial_System_Pct_Split],[Extension_1_Pct_Split],[Extension_2_Pct_Split],[Export_Status],[ExpPct1],[Initial_System_Export_Rate],[Current_Price(Export)],[ExpPct2],[InitSysExpRate2],[Current_Price(InitExp2)],[Extension_1_Export_Rate],[Current_Price(Export1)],[Extension_2_Export_Rate],[Current_Price(Export2)],[PaidToREAD_ID],[LatestPaidToRead_Meter_Read_Reason],REPLACE([Export_Read(OLD)],NULL,0) AS [Export_Read(OLD)],[Generation_Read(OLD)],[Meter_Read_Date(OLD)],LatestRead_ID,ReadForQuarter_Meter_Read_Reason,
[Export_Read(NEW)], 
[Generation_Read(NEW)],
[Meter_Read_Date(NEW)],
[Initial_System_Tariff_Rate],[Current Price(Initial)],[Extension_1_Tariff_Rate],[Current Price(Ext1)],[Extension_2_Tariff_Rate],[Current Price(Ext2)],(END_Fit_Year - Start_Fit_Year) As Split_No,Start_Fit_Year,END_Fit_Year

FROM [dbo].[Levelisation_Generator_Initial_Extract_vw]
WHERE END_Fit_Year - Start_Fit_Year = 0

UNION

/* This creates the first row of the Split  */

SELECT
[Payment_Type],[GeneratorID],[FiT_ID_Initial],[FiT_ID_Ext1],[FiT_ID_Ext2],[BP_ID],[CONTRACT_ID],[INSTALLATION_ID],[Initial_System_Pct_Split],[Extension_1_Pct_Split],[Extension_2_Pct_Split],[Export_Status],[ExpPct1],[Initial_System_Export_Rate],[Current_Price(Export)],[ExpPct2],[InitSysExpRate2],[Current_Price(InitExp2)],[Extension_1_Export_Rate],[Current_Price(Export1)],[Extension_2_Export_Rate],[Current_Price(Export2)],[PaidToREAD_ID],[LatestPaidToRead_Meter_Read_Reason],REPLACE([Export_Read(OLD)],NULL,0) AS [Export_Read(OLD)],[Generation_Read(OLD)],[Meter_Read_Date(OLD)],9999999999 AS LatestRead_ID,'CALC' AS ReadForQuarter_Meter_Read_Reason,

CASE 
	WHEN [Export_Read(NEW)] IS NULL THEN 0
	ELSE ([Export_Read(NEW)] - [Export_Read(OLD)]) / Datediff(Day,[Meter_Read_Date(NEW)],[Meter_Read_Date(OLD)]) * Datediff(day,DatefromParts(datepart(year,[Meter_Read_Date(NEW)]),03,31),[Meter_Read_Date(OLD)]) +[Export_Read(OLD)]
	END AS [Export_Read(NEW)], 
	
([Generation_Read(NEW)] - [Generation_Read(OLD)]) / Datediff(Day,[Meter_Read_Date(NEW)],[Meter_Read_Date(OLD)]) * Datediff(day,DatefromParts(datepart(year,[Meter_Read_Date(NEW)]),03,31),[Meter_Read_Date(OLD)]) +[Generation_Read(OLD)] AS [Generation_Read(NEW)],

DatefromParts(datepart(year,[Meter_Read_Date(OLD)]),03,31) AS [Meter_Read_Date(NEW)],

[Initial_System_Tariff_Rate],[Current Price(Initial)],[Extension_1_Tariff_Rate],[Current Price(Ext1)],[Extension_2_Tariff_Rate],[Current Price(Ext2)],(END_Fit_Year - Start_Fit_Year) As Split_No,Start_Fit_Year,END_Fit_Year

FROM [dbo].[Levelisation_Generator_Initial_Extract_vw]
WHERE END_Fit_Year - Start_Fit_Year = 1

UNION

/* This creates the Second Row of the Split */
/*  This should repeat to create however many extra split rows are needed, using Split_no. */

SELECT
[Payment_Type],[GeneratorID],[FiT_ID_Initial],[FiT_ID_Ext1],[FiT_ID_Ext2],[BP_ID],[CONTRACT_ID],[INSTALLATION_ID],[Initial_System_Pct_Split],[Extension_1_Pct_Split],[Extension_2_Pct_Split],[Export_Status],[ExpPct1],[Initial_System_Export_Rate],[Current_Price(Export)],[ExpPct2],[InitSysExpRate2],[Current_Price(InitExp2)],[Extension_1_Export_Rate],[Current_Price(Export1)],[Extension_2_Export_Rate],[Current_Price(Export2)],

9999999999 AS PaidToRead_ID,'CALC' AS LatestPaidToRead_Meter_Read_Reason,
CASE 
	WHEN [Export_Read(NEW)] IS NULL THEN 0
	ELSE ([Export_Read(NEW)] - [Export_Read(OLD)]) / Datediff(Day,[Meter_Read_Date(NEW)],[Meter_Read_Date(OLD)]) * Datediff(day,DatefromParts(datepart(year,[Meter_Read_Date(NEW)]),03,31),[Meter_Read_Date(OLD)]) +[Export_Read(OLD)]
	END AS [Export_Read(OLD)],
([Generation_Read(NEW)] - [Generation_Read(OLD)]) / Datediff(Day,[Meter_Read_Date(NEW)],[Meter_Read_Date(OLD)]) * Datediff(day,DatefromParts(datepart(year,[Meter_Read_Date(NEW)]),03,31),[Meter_Read_Date(OLD)]) +[Generation_Read(OLD)] AS [Generation_Read(OLD)],

DatefromParts(datepart(year,[Meter_Read_Date(Old)]),04,01) AS [Meter_Read_Date(Old)],

[LatestREAD_ID],[ReadForQuarter_Meter_Read_Reason],REPLACE([Export_Read(NEW)],NULL,0) AS [Export_Read(NEW)],[Generation_Read(NEW)],[Meter_Read_Date(NEW)],[Initial_System_Tariff_Rate],Rates.price AS [Current Price(Initial)],[Extension_1_Tariff_Rate],[Current Price(Ext1)],[Extension_2_Tariff_Rate],[Current Price(Ext2)],(END_Fit_Year - Start_Fit_Year) +1 As Split_No,Start_Fit_Year,END_Fit_Year

FROM [dbo].[Levelisation_Generator_Initial_Extract_vw] Lev
inner join RATES on Lev.Initial_System_Tariff_Rate = rates.Tariff_Code 
WHERE END_Fit_Year - Start_Fit_Year = 1
and DatefromParts(datepart(year,[Meter_Read_Date(Old)]),04,01) Between Rates.Valid_From and Rates.Valid_To


