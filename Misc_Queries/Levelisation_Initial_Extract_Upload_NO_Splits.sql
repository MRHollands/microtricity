
/* This adds all the Non-Split Accounts */
INSERT INTO LevelisationUploadData
([Payment_Type],[GeneratorID],[FiT_ID_Initial],[FiT_ID_Ext1],[FiT_ID_Ext2],[BP_ID],[CONTRACT_ID],[INSTALLATION_ID],[Initial_System_Pct_Split],[Extension_1_Pct_Split],[Extension_2_Pct_Split],[Export_Status],[ExpPct1],[Initial_System_Export_Rate],[Current_Price(Export)],[ExpPct2],[InitSysExpRate2],[Current_Price(InitExp2)],[Extension_1_Export_Rate],[Current_Price(Export1)],[Extension_2_Export_Rate],[Current_Price(Export2)],[PaidToREAD_ID],[LatestPaidToRead_Meter_Read_Reason],[Export_Read(OLD)],[Generation_Read(OLD)],[Meter_Read_Date(OLD)],[LatestREAD_ID],[ReadForQuarter_Meter_Read_Reason],[Export_Read(NEW)]     ,[Generation_Read(NEW)],[Meter_Read_Date(NEW)],[Initial_System_Tariff_Rate],[Current Price(Initial)],[Extension_1_Tariff_Rate],[Current Price(Ext1)],[Extension_2_Tariff_Rate],[Current Price(Ext2)],No_of_Splits,Start_Fit_Year,END_Fit_Year)

SELECT
[Payment_Type],[GeneratorID],[FiT_ID_Initial],[FiT_ID_Ext1],[FiT_ID_Ext2],[BP_ID],[CONTRACT_ID],[INSTALLATION_ID],[Initial_System_Pct_Split],[Extension_1_Pct_Split],[Extension_2_Pct_Split],[Export_Status],[ExpPct1],[Initial_System_Export_Rate],[Current_Price(Export)],[ExpPct2],[InitSysExpRate2],[Current_Price(InitExp2)],[Extension_1_Export_Rate],[Current_Price(Export1)],[Extension_2_Export_Rate],[Current_Price(Export2)],[PaidToREAD_ID],[LatestPaidToRead_Meter_Read_Reason],[Export_Read(OLD)],[Generation_Read(OLD)],[Meter_Read_Date(OLD)],[LatestREAD_ID],[ReadForQuarter_Meter_Read_Reason],[Export_Read(NEW)],[Generation_Read(NEW)],[Meter_Read_Date(NEW)],[Initial_System_Tariff_Rate],[Current Price(Initial)],[Extension_1_Tariff_Rate],[Current Price(Ext1)],[Extension_2_Tariff_Rate],[Current Price(Ext2)],END_Fit_Year - Start_Fit_Year As No_of_Splits,Start_Fit_Year,END_Fit_Year

FROM [dbo].[Levelisation_Generator_Initial_Extract_vw]
WHERE END_Fit_Year - Start_Fit_Year = 0




