/*  Adapt this SP of Glynn's to upload Microtricity's readings files 

*/

CREATE PROCEDURE [SapImp].[ADR2_usp] (@filename varchar(255))
AS
-- gmh - 07-11-2016
BEGIN
IF OBJECT_ID('tempdb..#ADR2') IS NOT NULL 	BEGIN DROP TABLE #ADR2	END
CREATE TABLE #ADR2  
(
client	INTEGER				NULL,
addrnumber	VARCHAR	(	10	)	NULL,
persnumber	VARCHAR	(	10	)	NULL,
date_from	varchar	(	99	)	NULL,
consnumber	DECIMAL	(	3	)	NULL,
country	VARCHAR	(	3	)	NULL,
flgdefault	VARCHAR	(	1	)	NULL,
flg_nouse	VARCHAR	(	1	)	NULL,
home_flag	VARCHAR	(	1	)	NULL,
tel_number	VARCHAR	(	30	)	NULL,
tel_extens	VARCHAR	(	10	)	NULL,
telnr_long	VARCHAR	(	30	)	NULL,
telnr_call	VARCHAR	(	30	)	NULL,
dft_receiv	VARCHAR	(	1	)	NULL,
r3_user	VARCHAR	(	1	)	NULL,
valid_from	VARCHAR	(	14	)	NULL,
valid_to	VARCHAR	(	15	)	NULL,

)



SET DATEFORMAT dmy
--DECLARE @filename VARCHAR(255) = 'e:\sapdata\bidata\OADR2_TABLEEXPORT_20161102_020844.CSV'
--DECLARE @filename VARCHAR(255) = 'e:\sapdata\bidata\OADR2_TEMP.csv'

DECLARE @sql  VARCHAR(max)


	SET @sql = 'BULK INSERT [#ADR2] from ''' + @filename + ''' 
				with (FIRSTROW = 2, FIELDTERMINATOR = ''|'')'

BEGIN TRY
	BEGIN TRANSACTION
	EXEC (@sql)


		TRUNCATE TABLE ADR2
		INSERT INTO ADR2 		
		SELECT
		client,
		addrnumber,
		persnumber,
		CASE 
						WHEN ISDATE(date_from) = 1 THEN CAST(date_from AS DATETIME) 
						WHEN date_from = '00000000' THEN cast('1980-01-01' AS DATETIME) 
						ELSE cast('1980-01-01' AS DATETIME) 
		END as  date_from,
		consnumber,
		country,
		flgdefault,
		flg_nouse,
		home_flag,
		tel_number,
		tel_extens,
		telnr_long,
		telnr_call,
		dft_receiv,
		r3_user,
		valid_from,
		valid_to

		FROM #ADR2

	COMMIT TRANSACTION

END TRY


BEGIN CATCH

	IF @@TRANCOUNT > 0
	BEGIN
		declare @error int, @message varchar(4000), @xstate int;
        select @error = ERROR_NUMBER(), @message = ERROR_MESSAGE(), @xstate = XACT_STATE();
        --if @xstate = -1
        --    rollback;
        --if @xstate = 1 and @@TRANCOUNT = 0
        --    rollback
        --if @xstate = 1 and @@TRANCOUNT > 0
        --    rollback transaction 

        raiserror ('SapImp.ADR2_usp: %d: %s', 16, 1, @error, @message) ;
		ROLLBACK TRANSACTION
	END
	--print ERROR_MESSAGE()
	DECLARE @MailRecipients VARCHAR(50)
	DECLARE @ErrMsg AS NVARCHAR(MAX) 
		
		SET @ErrMsg = ERROR_MESSAGE()
		SET @ErrMsg = @ErrMsg +  CHAR(13) +
					'There was an error importing data SP - SapImp.ADR2_usp:' + @filename

		--SET @MailRecipients = ('John.OSullivan@ecotricity.co.uk' + 'Glynn.Hammond@ecotricity.co.uk')
		SET @MailRecipients = ('Glynn.Hammond@ecotricity.co.uk')
	
		EXEC msdb.dbo.sp_send_dbmail @profile_name = 'lh-gendb-01',
			@recipients = @MailRecipients,
			@body =  @ErrMsg,
			@subject = 'SAP_BI_Import_ADR2'
	RETURN 0
	

END CATCH
END
