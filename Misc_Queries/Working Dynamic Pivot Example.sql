create table temp
(
    Installation_ID_Link Int,
    Meter_Read_Date date,
    Generation_Read Int
)

insert into temp 
(Installation_ID_Link,Meter_Read_Date,Generation_Read)
Select Installation_ID_Link,Meter_Read_Date,Generation_Read from readings where Meter_Read_Date between '2016-04-01' and '2017-03-31'
and Generation_Read is not null

DECLARE @cols AS NVARCHAR(MAX),
    @query  AS NVARCHAR(MAX);

SET @cols = STUFF((SELECT distinct ',' + QUOTENAME(c.Meter_Read_Date) 
            FROM temp c
            FOR XML PATH(''), TYPE
            ).value('.', 'NVARCHAR(MAX)') 
        ,1,1,'')

--Select @cols

set @query = 'SELECT Installation_ID_Link, ' + @cols + ' from 
            (
                select installation_id_link
                    , Generation_Read
                    , Meter_Read_Date
                from temp
           ) x
            pivot 
            (
                 max(Generation_Read)
                for meter_read_date in (' + @cols + ')
            ) p '


execute(@query)

drop table temp