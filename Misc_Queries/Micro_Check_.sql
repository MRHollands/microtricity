select distinct
FiT_ID_Initial,
Supply_MPAN,
R1.Meter_read_Reason,
R1.Meter_Read_Date

from Installation 
inner join readings R1 on installation.INSTALLATION_ID = R1.INSTALLATION_ID_link and R1.Meter_Read_Reason = 'Interim'
left join Readings R2 on installation.INSTALLATION_ID = R2.INSTALLATION_ID_link and R2.Meter_Read_Reason  = 'F8Q1'

where
R1.Meter_Read_Date >= getdate()-90
and R2.READ_ID is null
