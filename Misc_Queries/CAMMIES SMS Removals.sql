Select Distinct
Rate_Cat,
Main.Can AS Contract_Account,
E1.journey_status AS Elec_Journey_Status,
G1.journey_status AS Gas_Journey_Status,
Elec_MO_Date AS Elec_Move_Out_Date,
Gas_MO_Date AS Gas_Move_Out_Date,
cast(Main.Opt_Out_Date  as varchar(20)) AS Opt_Out_Date,
CASE
	WHEN Main.Opt_Out_Date is not null THEN 'Remove All Fuels'
	WHEN Elec_MO_Date < '31Dec9999' THEN 'Remove All Fuels'
	WHEN (Elec_MO_Date is Null AND Gas_MO_Date < '31Dec9999') THEN 'Remove Gas Only'
	WHEN (Elec_MO_Date = '31Dec9999' AND Gas_MO_Date < '31Dec9999') THEN 'Remove Gas Only'
	WHEN E1.journey_status IN ('Removed','Opted Out') THEN 'Remove All Fuels'
	WHEN G1.journey_status IN ('Removed','Opted Out') AND E1.journey_status NOT IN ('Removed','Opted Out')  THEN 'Remove Gas Only'
	ELSE NULL
END AS Remove_Fuel,
Elec_MPAN,
Gas_MPAN



FROM SmartRollout.installs.InstallTargetPipeline_vw Main 
left join (SELECT * from SmartRollout.installs.InstallTargetPipeline_vw WHERE Fuel = 'ELEC') E1 on
Main.Can = E1.Can 
left join (SELECT * FROM SmartRollout.installs.InstallTargetPipeline_vw WHERE Fuel = 'GAS') G1 on
Main.CAN = G1.CAN

Left join (Select Distinct
Main.Contract_Account,
CASE 
	WHEN CA1_E.Rate_Category is not null and CA1_G.Rate_Category is not null THEN 'Dual'
	WHEN CA1_E.Rate_Category is not null and CA1_G.Rate_Category is null THEN 'Elec'
	WHEN CA1_E.Rate_Category is null and CA1_G.Rate_Category is not null THEN 'Gas'
ELSE NULL
END AS Rate_Cat,
CA1_E.MO_Date AS Elec_MO_Date,
CA1_G.MO_Date AS Gas_MO_Date,
CA1_E.MPAN_MPRN AS Elec_MPAN,
CA1_G.MPAN_MPRN AS Gas_MPAN

from CAMPAN_Data Main

Left join (Select Contract_Account, Left(Rate_Category,1) AS Rate_Category, MO_Date, MPAN_MPRN From DatSup.dbo.CAMPAN_Data Where Left(Rate_Category,1)='E') AS  CA1_E on
Main.Contract_Account = CA1_E.Contract_Account
Left join (Select Contract_Account, Left(Rate_Category,1) AS Rate_Category, MO_Date, MPAN_MPRN From DatSup.dbo.CAMPAN_Data Where Left(Rate_Category,1)='G') AS  CA1_G on
Main.Contract_Account = CA1_G.Contract_Account) Campan
on Campan.Contract_Account = Main.CAN

WHERE 
(E1.journey_status IN ('Removed','Opted Out') 
OR 
G1.journey_status IN ('Removed','Opted Out'))
OR
(Elec_MO_Date < '31Dec9999'
OR
Gas_MO_Date < '31Dec9999')

