-- Accounts File
-- gmh 09-09-16 from code provided by Matt Hollands.

SELECT DISTINCT
	E1.MPAN_MPRN AS MPAN,
	G1.MPAN_MPRN AS MPRN,
	'Ecotricity' AS Supplier,

	REPLACE(COALESCE(PA1.[Floor],'') + COALESCE(PA1.House_No,' ') + COALESCE(PA1.Street,''),',',' ') AS Address1,
	REPLACE(COALESCE(PA1.Street2,'') + COALESCE(PA1.Street3,' ') + COALESCE(PA1.Street4,'') + COALESCE(PA1.Street5,''),',',' ') AS Address2,
	REPLACE( NULLIF(PA1.City,''),',',' ') AS Address3,
	REPLACE( NULLIF(PA1.Post_Code,''),',',' ') AS Post_Code_BA,
	'' AS AccessArrangements,
	LTRIM(COALESCE(Main.Mobile,'') +' '+ COALESCE(Main.Other_Mobile,'') +' '+ COALESCE(Main.Phone,'') +' '+ COALESCE(Main.Other_Phone,'')) AS AdditionalInfo,
	CASE
		WHEN G.bp is not null THEN 'Glide'
		WHEN substring(CA1.rate_category,2,1) = 'B' Then 'Business'
		WHEN substring(CA1.rate_category,2,1) = 'D' Then 'Domestic'
	END AS JobType,
	Main.DF_or_SF_Elec AS JobSubType,
	E1.Current_Meter_MSN AS MM_Electricity_MSNs,
	REPLACE( NULLIF(G1.Current_Meter_MSN,''),',',' ') AS MM_GAS_MSNs,
	REPLACE( NULLIF(Main.Customer_Name,''),',',' ') AS CONTACT_NAME,
	Main.Phone AS CONTACT_NUMBER,
	CASE
		WHEN E1.Current_Meter_Mode = 'PAYG' THEN 'Yes'
		ELSE 'No'
	END AS IS_PREPAYMENT_ELEC,
	CASE 
		WHEN G1.Current_Meter_Mode  = 'PAYG' THEN 'Yes'
		ELSE 'No'
	END AS IS_PREPAYMENT_GAS,
	BUT1.Type as Custom_1,
	CASE 
		WHEN CA1.Rate_Category like '%E7%' THEN 'Economy 7'
		ELSE 'Standard'
	END AS E7,


	Main.Scenario as [PRIORITY],
	CASE 
		WHEN CA1.MI_Date < '31Dec9999' THEN cast(CA1.MO_Date as varchar(20))
		ELSE NULL
	END AS Move_In_Date,
	'What is this' as Changed_in_the_last_week,
	CAST(MAIN.CAN as varchar(20)) AS Contract_Account

FROM 
SmartRollout.installs.InstallTargetPipeline_vw Main 
left join (SELECT * from SmartRollout.installs.InstallTargetPipeline_vw WHERE Fuel = 'ELEC') E1 
on Main.Can = E1.Can 
left join (SELECT * FROM SmartRollout.installs.InstallTargetPipeline_vw WHERE Fuel = 'GAS') G1 
on Main.CAN = G1.CAN
Left join DatSup.dbo.BillingAddress BA1 
on Main.CAN = BA1.Contract_Number
Left join DatSup.dbo.Premise_Address PA1 
on Main.CAN = PA1.Contract
Left join DatSup.dbo.CAMPAN_Data CA1 
on Main.CAN = CA1.Contract_Account
LEFT JOIN glide.bps G 
on CA1.BP = G.bp
LEFT Join 
(Select 
partner,
CASE WHEN Count(Type) > 1 THEN 'PSR Customer'
ELSE NULL
END AS Type
FROM [LH-GENDB-01].BI_RAW.dbo.BUT0ID 
Group by partner) BUT1 
on BUT1.partner = cA1.bp

WHERE 
Main.Customer_Name not like '%'+'Glid'+'%'
AND
(e1.Journey_Status not in ('Removed','Opted Out','Non-Smart Fitted','Complete: Meter Built in SAP','Installed','Installed: MTDs and Reads Received') 
OR
g1.Journey_Status not in ('Removed','Opted Out','Non-Smart Fitted','Complete: Meter Built in SAP','Installed','Installed: MTDs and Reads Received'))