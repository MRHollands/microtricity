-- Server uh-etrmdbd-01

USE [FGR]
GO

Select
[One],
[Number],
[Matchname],
[Half],
[Time],
[Min in Half],
CASE 
	WHEN [Half] <> (Lag(Half) OVER(partition by matchname order by Matchname,Number)) and [Half]='second half' THEN [Min in Half] + (Lag([Min_in_90_Calc]) OVER(partition by matchname order by Matchname,Number))
	ELSE [Min_in_90_Calc]
END AS [Min_in_90_New],
[Min in 90 Old],
Min_in_90_Calc

FROM
(SELECT
		[One],
      [Number],
      [Matchname],
      [Half],
      [Time],
      [Time]/60 AS [Min in Half],
	  CASE 
		WHEN ([Half] <> (Lag(Half) OVER(partition by matchname order by Matchname,Number)) and [Half]='first half') OR (Lag(Half) OVER(partition by matchname order by Matchname,Number)) is null THEN 0
		WHEN [Half] <> (Lag(Half) OVER(partition by matchname order by Matchname,Number)) and [Half]='second half' THEN [Min in Half] + (Lag([Min in Half]) OVER(partition by matchname order by Matchname,Number))
		ELSE [Min in Half]
		END AS [Min_in_90_Calc],
      [Min in 90] AS [Min in 90 Old]


  FROM [dbo].[League2DataRaw_MH_Test]) DATA
