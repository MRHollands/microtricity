DECLARE @Latest_Quarter_Read AS Nvarchar(10)

SET @Latest_Quarter_Read = (Select Max(Meter_Read_Reason) from Readings Where Meter_Read_Date >= Getdate()-92 and Meter_read_reason Like'F%Q%')

SELECT 
[CONTRACT].CONTRACT_ID, 
READINGS.READ_ID, 
READINGS.INSTALLATION_ID_link, 
READINGS.Meter_Read_Type, 
READINGS.Meter_Read_Date AS [Meter_Read_Date(NEW)], 
READINGS.Meter_Read_Reason, 
READINGS.Generation_Read AS [Generation_Read(NEW)], 
READINGS.Export_Read AS [Export_Read(NEW)], 
READINGS.IncludedInLevelisation, 
READINGS.Gen_Validation, 
READINGS.Modified_By, 
READINGS.Date_Modified, 
READINGS.Time_Modified, 
READINGS.SSMA_TimeStamp,
Contract.Move_In,
Contract.Move_Out

FROM READINGS 
INNER JOIN [CONTRACT] ON READINGS.INSTALLATION_ID_link = [CONTRACT].INSTALLATION_ID_link

WHERE 
READINGS.Meter_Read_Date Between [CONTRACT].Move_In And [CONTRACT].Move_Out
AND READINGS.Meter_Read_Reason IN (@Latest_Quarter_Read,'Final','Final (COT)','Change of Supply','Final (COS)','Final (MX)')
AND READINGS.IncludedInLevelisation = 'False'





