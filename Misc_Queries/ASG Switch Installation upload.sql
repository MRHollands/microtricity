/*

Have you cleared the TEMP_Installation table before uploading the new data?  Run this to check that nothing in the temp table already exists in the Installation Table:

select * from installation inner join temp_installation on installation.fit_id_initial = temp_Installation.fit_id_initial

Delete temp table if any exist in installation and throw back to Micro team:

delete temp_installation


*/

insert into INSTALLATION
(FiT_ID_Initial,
Type_of_Installation,
Generation_Type,
Export_Status,
Generation_Meter_Make_and_Model,
Generation_Meter_Model,
Remotely_Read_Enabled,
Address_1,
City,
Postal_Code,
Cos_Flag,
Initial_System_Export_Rate)

select
fit_id_initial,
Type_of_Installation,
Generation_Type,
Export_Status,
Generation_Meter_Make_and_Model,
Generation_Meter_Model,
Remotely_Read_Enabled,
Address_1,
City,
Postal_Code,
CoS_Flag,
Initial_System_Export_Rate
from temp_installation

