/*
Server:		uh-gendb-01
Database:	Microtricity2
Author:  Matthew Hollands
Date Last Modified:  28/08/2018
*/

INSERT INTO MASS_READ_UPLOAD_TEMPLATE 
(
FiT_ID, 
INSTALLATION_ID_link, 
Generation_Type, 
Last_Read_Date, 
New_Read_Date, 
New_Read_Type, 
New_Read_Reason, 
Last_Generation_Read, 
New_Generation_Read, 
Total_Gen_Read_Diff, 
Generation_Read_Valid, 
Last_Export_Read, 
New_Export_Read,
Total_Exp_Read_Diff, 
Export_Read_Valid, 
Account_Status, 
HrsPerDay, 
MSN, 
Recieved_Method )

SELECT 
[Validation_Check(MASS)].FiT_ID, 
[Validation_Check(MASS)].INSTALLATION_ID, 
[Validation_Check(MASS)].generation_Type, 
[Validation_Check(MASS)].Meter_Read_Date, 
[Validation_Check(MASS)].New_Read_Date2, 
[Validation_Check(MASS)].New_Read_Type, 
[Validation_Check(MASS)].[New_Read_Reason], 
[Validation_Check(MASS)].Generation_Read, 
[Validation_Check(MASS)].New_Gen_Read, 
[Validation_Check(MASS)].Gen_Read_Diff, 
[Validation_Check(MASS)].Gen_Validation_Check, 
[Validation_Check(MASS)].Export_Read, 
[Validation_Check(MASS)].New_Exp_Read, 
[Validation_Check(MASS)].Exp_Read_Diff, 
[Validation_Check(MASS)].Exp_Validation_Check, 
CONTRACT.Account_Status, 
[Validation_Check(MASS)].HrsPerDay, 
[Validation_Check(MASS)].MSN, 
[Validation_Check(MASS)].Recieved_Method

FROM [Validation_Check(MASS)] LEFT JOIN CONTRACT ON [Validation_Check(MASS)].INSTALLATION_ID = CONTRACT.INSTALLATION_ID_link
WHERE (((CONTRACT.Move_Out)=#12/31/9999#));
