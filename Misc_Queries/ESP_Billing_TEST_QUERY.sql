SELECT
Check_Start_Date,
Check_End_Date,
Contract_ID_Link, 
account_status, 
Meter_Read_Reason, 
Contract_Account, 
First_Name, 
Last_Name, 
Email_Address, 
Address_1, 
Address_2, 
Address_3, 
city, 
county, 
PostCode, 
Contract_Start_Date, 
(SELECT max(invoice_Number) FROM ESP_Invoicing) + (ROW_NUMBER() OVER (ORDER BY Contract_ID_Link)) AS Invoice_Number, 
Invoice_Date, 
Invoice_Due_Date, 
CASE 
  WHEN Email_Address IS NOT NULL THEN 'Email' 
  WHEN Email_Address IS NOT NULL AND Invoice_Route = 'Letter' THEN 'Letter'
  ELSE 'Letter' 
END AS Invoice_Method, 
Total_Past_Due_Charges,

CASE 
WHEN Last_Payment_Date IS NOT null Then Last_Payment_Date
ELSE '01Jan1900' 
END AS Last_Payment_Date, 

CASE
WHEN Last_Payment_Amount IS NOT NULL Then Last_Payment_Amount
ELSE NULL 
END AS Last_Payment_Amount, 
Billing_Start, 
Billing_End, 
CAST(Total_Anticipated_Output as Numeric(10,2)) AS Total_Anticipated_Output, 
Convert(nvarchar(100),MPAN) AS MPAN, 
CASE WHEN Discount = 1 THEN 0
ELSE CAST((Generation_kWh * (Gross_Price/100)) - ((Generation_kWh * (Fit_Gen_Price/100)) + (Export_kWh * (Fit_Exp_Price/100))) AS Numeric(10,2)) + CAST(((Generation_kWh * (Gross_Price/100)) - ((Generation_kWh * (Fit_Gen_Price/100)) + (Export_kWh * (Fit_Exp_Price/100)))) * .05 AS Numeric(10,2)) 
END AS Total_Amount_Due, 
CASE WHEN Discount = 1 THEN 0
ELSE CAST((Generation_kWh * (Gross_Price/100)) - ((Generation_kWh * (Fit_Gen_Price/100)) + (Export_kWh * (Fit_Exp_Price/100))) AS Numeric(10,2)) + CAST(((Generation_kWh * (Gross_Price/100)) - ((Generation_kWh * (Fit_Gen_Price/100)) + (Export_kWh * (Fit_Exp_Price/100)))) * .05 AS Numeric(10,2)) + Total_Past_Due_Charges 
END AS Total_Due,
Gross_Price, 
CAST(Start_Read as Int) as Start_Read, 
CAST(End_Read as Int) as End_Read, 
Generation_kWh, 
Fit_Gen_Price, 
CAST((Generation_kWh * (Fit_Gen_Price/100)) AS Numeric(10,2)) AS Fit_Gen_Cost, 
Fit_Exp_Price, 
CAST(Export_kWh AS Numeric(10,2)) AS Export_kWh, 
CAST((Export_kWh * (Fit_Exp_Price/100)) as Numeric(10,2)) AS Fit_Exp_Cost, 
CAST((Generation_kWh * (Fit_Gen_Price/100)) + (Export_kWh * (Fit_Exp_Price/100)) AS Numeric(10,2)) AS Fit_Discount, 
CAST((Generation_kWh * (Gross_Price/100)) - ((Generation_kWh * (Fit_Gen_Price/100)) + (Export_kWh * (Fit_Exp_Price/100))) AS Numeric(10,2)) AS Net_Charges, 
Discount, 
CAST((Generation_kWh * (Gross_Price/100)) - ((Generation_kWh * (Fit_Gen_Price/100)) + (Export_kWh * (Fit_Exp_Price/100))) - (((Generation_kWh * (Gross_Price/100)) - ((Generation_kWh * (Fit_Gen_Price/100)) + (Export_kWh * (Fit_Exp_Price/100)))) * Discount) AS Numeric(10,2)) AS Net_Cost, 
CAST(((Generation_kWh * (Gross_Price/100)) - ((Generation_kWh * (Fit_Gen_Price/100)) + (Export_kWh * (Fit_Exp_Price/100)))) * .05 AS Numeric(10,2)) AS VAT, 
CAST((Generation_kWh * (Gross_Price/100)) AS Numeric(10,2)) AS Gross_Cost, 
0 AS Late_Fees, 
Bank_Acc_Number, 
Payment_Method, 
Total_Installed_Capacity_kW AS System_Size, 
Start_MSN AS Meter_ID,
Move_In_ESP,
Move_Out_ESP,
Start_MSN,
END_MSN,
Start_Read_ID,
END_Read_ID

FROM            
(SELECT DISTINCT
Check_Start_Date,
Check_End_Date,
Contract_ID_Link, 
Contract.Account_status, 
Start_R.meter_Read_Reason, 
ESP_Contract.SAP_CA AS Contract_Account, 
ESP_Contract.First_Name, 
ESP_contract.Last_Name, 
esp_contract.Email_Address, 
ESP_Contract.Address AS Address_1, 
ESP_Contract.Address_2, 
ESP_Contract.Address_3, 
ESP_Contract.city, 
ESP_Contract.county, 
ESP_Contract.PostCode, 
Initial_System_Commissioning_Date AS Contract_Start_Date,
getdate() AS Invoice_Date, 
getdate() + 14 AS Invoice_Due_Date, 
CASE 
  WHEN DBTMGMTEXPORT.Amount IS NULL THEN 0 
  ELSE DBTMGMTEXPORT.Amount 
END AS Total_Past_Due_Charges, 
Billing_Start, 
Billing_End, 
PPA_Anticipated_Output AS Total_Anticipated_Output, 
Supply_MPAN AS MPAN, 
R1.Gross_Electricity_Charge AS Gross_Price, 
Start_Read, 
End_Read, 
CASE 
/* WHEN Start_Read is null and End_Read is not null then 'No Start Read'*/ 
  WHEN Start_Read IS NOT NULL AND End_Read IS NULL THEN (PPA_Anticipated_Output / 365 * datediff(day,Billing_Start,Billing_End)) + Start_Read 
  ELSE End_Read - Start_Read 
END AS Generation_kWh, 
R2.Price AS Fit_Gen_Price, 
R3.Price AS Fit_Exp_Price, 
((End_Read - Start_Read) / 2) AS Export_kWh, 
CASE 
  WHEN promotion_code IN (2, 3) THEN 1 
  ELSE 0 
END AS Discount, 
ESP_Contract.Bank_Account AS Bank_Acc_Number, 
Esp_Contract.Payment_Method, 
Total_Installed_Capacity_kW, 
Generation_MSN,
Move_In_ESP,
Move_Out_ESP,
Start_MSN,
END_MSN,
Start_Read_ID,
END_Read_ID,
Invoice_Route,
Last_Payment_Date,
Last_Payment_Amount

FROM BP INNER JOIN Contract ON bp.BP_ID = CONTRACT.BP_ID_link 
INNER JOIN installation ON CONTRACT.INSTALLATION_ID_link = INSTALLATION.INSTALLATION_ID 
LEFT JOIN ESP_Contract ON contract.CONTRACT_ID = ESP_Contract.Contract_ID_Link AND ESP_Contract.Account_Status = 'Live'
INNER JOIN (SELECT *
            FROM ESP_Annual
            WHERE ESP_Annual.Year = 1) ESP_Annual /* Year value needs to be calculated rather than manual*/ ON INSTALLATION.ESP_ID = ESP_Annual.ESP_ID 

LEFT JOIN
(select Cont_Account, SUM(Amount) AS Amount from [UH-GENDB-01].[DatSup].[dbo].[DBTMGMTEXPORT] Group by Cont_Account) AS DBTMGMTEXPORT  
ON DBTMGMTEXPORT.Cont_Account = ESP_Contract.sap_CA 



/* This is the join to get the start Reads */ 
LEFT JOIN  (SELECT
Convert(date,Main_Read.Meter_Read_Date) AS Check_Start_Date,
Convert(date,Billing_End) AS Check_End_Date,
Main_Read.Meter_Read_Reason, 
Main_Read.INSTALLATION_ID_link,
Main_Read.MSN AS Start_MSN,
Main_Read.Read_ID AS Start_Read_ID,
CASE 
WHEN ESP_invoicing.Contract_ID_Link is not null THEN (select DATEADD(d,1,MAX(Billing_End)) from esp_invoicing E_1 where E_1.Contract_ID_Link = ESP_Invoicing.Contract_ID_Link)
WHEN (Select min(Meter_Read_Date) from Readings R_1 where R_1.INSTALLATION_ID_link = Main_Read.INSTALLATION_ID_link) = Main_Read.Meter_Read_Date THEN Main_Read.Meter_Read_Date
WHEN (Select min(Meter_Read_Date) from Readings R_1 where R_1.INSTALLATION_ID_link = Main_Read.INSTALLATION_ID_link and R_1.Meter_Read_Reason = 'Initial (MX)') = Main_Read.Meter_Read_Date THEN Main_Read.Meter_Read_Date
WHEN (Select min(Meter_Read_Date) from Readings R_1 where R_1.INSTALLATION_ID_link = Main_Read.INSTALLATION_ID_link and R_1.Meter_Read_Reason = 'Initial (COT)') = Main_Read.Meter_Read_Date THEN Main_Read.Meter_Read_Date
ELSE NULL
END AS Billing_Start, 
CASE 
WHEN ESP_invoicing.Contract_ID_Link is not null THEN E_1.end_read
-- Need to find End Read where it's the highest billing end for this contract
WHEN (select min(Meter_Read_Date) from Readings R_1 where R_1.INSTALLATION_ID_link = Main_Read.INSTALLATION_ID_link) = Main_Read.Meter_Read_Date THEN Main_Read.Generation_Read
WHEN (select min(Meter_Read_Date) from Readings R_1 where R_1.INSTALLATION_ID_link = Main_Read.INSTALLATION_ID_link AND R_1.Meter_Read_Reason = 'Initial (MX)') = Main_Read.Meter_Read_Date THEN Main_Read.Generation_Read
WHEN (select min(Meter_Read_Date) from Readings R_1 where R_1.INSTALLATION_ID_link = Main_Read.INSTALLATION_ID_link AND R_1.Meter_Read_Reason = 'Initial (COT)') = Main_Read.Meter_Read_Date THEN Main_Read.Generation_Read
ELSE NULL
END AS Start_Read
FROM CONTRACT 
INNER JOIN INSTALLATION ON CONTRACT.INSTALLATION_ID_link = INSTALLATION.INSTALLATION_ID 
LEFT JOIN ESP_Invoicing ON CONTRACT.CONTRACT_ID = ESP_Invoicing.Contract_ID_Link 
RIGHT OUTER JOIN READINGS Main_Read ON INSTALLATION.INSTALLATION_ID = Main_Read.INSTALLATION_ID_link
left join (select Contract_id_link, MAX(Billing_End) AS Max_Bill_End, End_Read from esp_invoicing Group by Contract_ID_Link, End_Read) E_1 on E_1.Contract_ID_Link = ESP_Invoicing.Contract_ID_Link
) Start_R ON Start_R.INSTALLATION_ID_link = INSTALLATION.INSTALLATION_ID 



/* This is the join to get the End Read */
LEFT JOIN  (SELECT        
READINGS.Meter_Read_Reason, 
READINGS.INSTALLATION_ID_link,
Meter_Read_Date AS Billing_End,
Generation_Read as End_Read,
MSN AS End_MSN,
Read_ID AS END_Read_ID

FROM            
CONTRACT 
INNER JOIN INSTALLATION ON CONTRACT.INSTALLATION_ID_link = INSTALLATION.INSTALLATION_ID 
LEFT JOIN ESP_Contract ON CONTRACT.CONTRACT_ID = ESP_Contract.Contract_ID_Link
RIGHT OUTER JOIN READINGS ON INSTALLATION.INSTALLATION_ID = READINGS.INSTALLATION_ID_link
INNER JOIN (select INSTALLATION_ID_link, MSN AS R2_MSN, Max(meter_read_date) MaxDate from readings Group by INSTALLATION_ID_link, MSN) R2 on installation.INSTALLATION_ID = R2.INSTALLATION_ID_link and readings.MSN = R2.R2_MSN

WHERE 
Meter_Read_Date between Move_In_ESP and Move_Out_ESP
AND Meter_Read_Date = MaxDate
AND Meter_Read_Reason not in ('2 Year Audit','Initial','Initial (COT)','Initial (MX)','Interim') 
AND ESP_Billed is null) End_Read 
ON End_Read.INSTALLATION_ID_link = INSTALLATION.INSTALLATION_ID 

LEFT JOIN rates R1 ON R1.Tariff_Code = ESP_Contract.Sun_Edison_Contract_Number AND (Billing_End Between R1.Valid_From AND R1.Valid_To) 
LEFT JOIN rates R2 ON R2.Tariff_Code = ESP_Contract.Initial_System_Tariff_Rate AND (Billing_End Between R2.Valid_From AND R2.Valid_To) 
LEFT JOIN rates R3 ON R3.Tariff_Code = ESP_Contract.Initial_System_Export_Rate AND (Billing_End Between R3.Valid_From AND R3.Valid_To)
LEFT JOIN ESP_Last_Payment ON ESP_Last_Payment.SAP_CA = ESP_Contract.SAP_CA) ESP_Bill

WHERE 
Start_MSN = End_MSN 
AND Start_Read_ID <> END_Read_ID 
AND Billing_Start < Billing_End 
AND Billing_Start between Move_In_Esp and Move_out_ESP 
AND Billing_End between Move_In_Esp and Move_out_ESP