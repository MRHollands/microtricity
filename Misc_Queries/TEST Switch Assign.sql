

/* Add any Assigneds that don't exist in the Table already */
SELECT Switch_Assign_Agents.FIT_ID, Switch_Assign_Agents.Assigned_to
FROM Switch_Assign_Agents LEFT JOIN ASG_Switch ON Switch_Assign_Agents.FIT_ID = ASG_Switch.FIT_ID
WHERE ASG_Switch.FIT_ID IS NULL;


/*Update the Assignee if the FIT is already in the Switch table */
select *
FROM ASG_Switch
INNER JOIN Switch_Assign_Agents ON Switch_Assign_Agents.FIT_ID = ASG_Switch.FIT_ID 


/* Adds the Move In, if it Exists */
Select *
From Switch_Assign_Agents
Inner Join INSTALLATION on Switch_Assign_Agents.FIT_ID = INSTALLATION.FiT_ID_Initial 
Inner Join Contract on CONTRACT.INSTALLATION_ID_link = INSTALLATION.INSTALLATION_ID
WHERE 
Switch_Assign_Agents.Move_In is not null 
AND
CONTRACT.Account_Status='ASG Switch Pending'
