/* 
DBTMGMTEXPORT Summary Query
Author: Matthew Hollands
Date last modified: 29/01/16 
*/


/* Creates a temporary table to hold the various data, if the temp table already exists it deletes it before creating a new, empty one */
SET dateformat  'dmy';
IF OBJECT_ID('tempdb.dbo.##Campan_Temp', 'U') is not null
  DROP TABLE ##Campan_Temp

Create Table ##Campan_Temp (
	CA Int, 
	Type nvarchar(50))

/* Adds the data from dbtmgmtexport into the temp table which is identified by having a 7 digit Cont_Account */
INSERT INTO ##Campan_Temp

SELECT DISTINCT Coll_bill_act,
CASE 
	WHEN BP in (10068091, 10122504, 10121390, 10121100, 10121101, 10121102, 10121103, 20123460, 20123464) THEN 'Glide'
	WHEN Contract_Account Between 0 AND 39999999 OR Contract_Account Between 70000000 AND 79999999 THEN 'Business'
ELSE 'Domestic'
END AS Type
FROM campan_Data INNER JOIN DBTMGMTEXPORT 
	ON CAMPAN_Data.Coll_bill_act = DBTMGMTEXPORT.Cont_Account*1
WHERE LEN(DBTMGMTEXPORT.Cont_Account*1) = 7


/* Adds the data from dbtmgmtexport into the temp table which is identified by having a 8 digit Cont_Account */
INSERT INTO ##Campan_Temp

SELECT DISTINCT Contract_Account,
CASE 
	WHEN BP in (10068091, 10122504, 10121390, 10121100, 10121101, 10121102, 10121103, 20123460, 20123464) THEN 'Glide'
	WHEN Contract_Account Between 0 and 39999999 or Contract_Account Between 70000000 and 79999999 THEN 'Business'
ELSE 'Domestic'
END AS Type
FROM campan_Data INNER JOIN DBTMGMTEXPORT
ON CAMPAN_Data.Contract_Account = DBTMGMTEXPORT.Cont_Account*1
WHERE LEN(DBTMGMTEXPORT.Cont_Account*1) = 8


/* Adds the data from dbtmgmtexport into the temp table which doesn't match anything in the Campan Data */
INSERT INTO ##Campan_Temp

SELECT DISTINCT Cont_Account,
CASE WHEN CA Between 0 AND 39999999 OR CA Between 70000000 AND 79999999 THEN 'Business'
ELSE 'Domestic'
END AS Type
FROM DBTMGMTEXPORT LEFT JOIN ##Campan_Temp ON (DBTMGMTEXPORT.Cont_Account*1) = ##Campan_Temp.CA
WHERE ##Campan_Temp.CA is NULL



/* Summarises the data and appends it to the DBTMGMTSUM Table */
INSERT INTO DBTMGMTSUM
SELECT 
CONVERT(varchar(11),getdate(),103)   as Date,
##Campan_Temp.Type,
Count(Distinct (concat(Cont_Account,dbtmgmtexport.contract))) As Accounts,
Sum(Amount) As Value,
Sum(LT30),
Sum(LT60),
Sum(LT90),
Sum(LT180),
Sum(LT365),
Sum(GT365)
FROM DBTMGMTEXPORT inner join ##Campan_Temp on DBTMGMTEXPORT.Cont_Account = ##Campan_Temp.CA
Group by Type
EXCEPT 
SELECT * FROM DBTMGMTSUM


/* Email Todays results */
IF (datename(dw,getdate()) = 'Wednesday')
BEGIN
DECLARE @Separator varchar(1)
DECLARE @bodyHtml NVARCHAR(MAX)
DECLARE @mailSubject NVARCHAR(MAX)
DECLARE @STMT VARCHAR(100)
DECLARE @RtnCode INT
SET @Separator=';'
DECLARE @tableHTML  NVARCHAR(MAX) ;

SET @Mailsubject = 'Aged Debt Summary Weekly Report'


SET @tableHTML = 
       N'<style type="text/css">
       #box-table
       {
       font-family: "Lucida Sans Unicode", "Lucida Grande", Sans-Serif;
       font-size: 12px;
 	   text-align: left;
       border-collapse: collapse;
       border-top: 7px solid #9baff1;
       border-bottom: 7px solid #9baff1;
       }
       #box-table th
       {
       font-size: 13px;
       font-weight: normal;
       background: #b9c9fe;
       border-right: 2px solid #9baff1;
       border-left: 2px solid #9baff1;
       border-bottom: 2px solid #9baff1;
       color: #039;
       }
       #box-table td
       {
       border-right: 1px solid #aabcfe;
       border-left: 1px solid #aabcfe;
       border-bottom: 1px solid #aabcfe;
       color: #669;
       }
       tr:nth-child(odd) { background-color:#eee; }
       tr:nth-child(even) { background-color:#fff; } 
       </style>'+ 
       N'<H3><font color="Red">Aged Debt Summary Weekly Report</H3>' +
       N'<table id="box-table" >' +
       N'<tr><font color="Green"><th>Report Date</th>
       <th>Type</th>
       <th>Accounts</th>
       <th>Value</th>
       <th>Less Than 30</th>
       <th>Less Than 60</th>
       <th>Less Than 90</th>
       <th>Less Than 180</th>
       <th>Less Than 365</th>
       <th>More Than 365</th>
       </tr>' +
       CAST ( ( 

SELECT 
	td = Report_Date,'',
	td = [Type],'',
	td = [Accounts],'',
	td = [Value] ,'',
	td = [LT30] ,'',
	td = [LT60] ,'',
	td = [LT90] ,'',
	td = [LT180] ,'',
	td = [LT365] ,'',
	td = [GT365]
FROM
(SELECT 
	CASE 
		WHEN Convert(Date,[Date], 103) = convert(date,getdate(),103) Then 'This Week'
		WHEN Convert(Date,[Date], 103) = convert(date,getdate()-7,103) Then 'Last Week'
	Else Null
	END AS Report_Date,
	[Type],
	[Accounts],
	Convert(varchar,cast([Value] as money),1) as [Value],
	Convert(varchar,cast([LT30] as money),1) as [LT30],
	Convert(varchar,cast([LT60] as money),1) as [LT60],
	Convert(varchar,cast([LT90] as money),1) as [LT90],
	Convert(varchar,cast([LT180] as money),1) as [LT180],
	Convert(varchar,cast([LT365] as money),1) as [LT365],
	Convert(varchar,cast([GT365] as money),1) as [GT365]
  FROM [dbo].[DBTMGMTSUM]) Query
  WHERE Report_Date is not null
  FOR XML PATH('tr'), TYPE 
) AS NVARCHAR(MAX) ) +
N'</table>' 

	EXEC  @RtnCode =  msdb.dbo.sp_send_dbmail 
      @profile_name = 'DataSupport',
    --@query_result_separator=@Separator,
	  @recipients=@recipientlist,
    --@recipients='glynn.hammond@ecotricity.co.uk',        
		@body = @tableHTML,
		@body_format = 'HTML',
        @subject = @bodyHtml
	--@attach_query_result_as_file =1;

    IF @RtnCode <> 0
      RAISERROR('Error.', 16, 1)
END



