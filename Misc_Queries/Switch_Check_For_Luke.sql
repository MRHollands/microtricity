select
A.FIT_ID,
I.Created_By2,
A.Raised_By,
A.Completed_By,
C.Account_Status,
I.address_1,
I.Postal_code,
I.Generation_type,
I.Generation_MSN,
I.Total_Installed_Capacity_kW,
I.Total_Declared_Net_Capacity_kW,
I.Estimated_Annual_Generation,
I.Generation_Meter_Make_and_Model,
I.Generation_Meter_Model,
I.Supply_MPAN,
I.Export_MPAN,
I.MCS_Certification_Number_1


from ASG_Switch A 
inner join installation I on A.fit_id = I.fit_id_initial
inner join contract C on C.installation_id_link = I.installation_id