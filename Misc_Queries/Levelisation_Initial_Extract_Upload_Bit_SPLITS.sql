/* This adds Accounts with 1 Split, so it creates 2 rows.  */
INSERT INTO LevelisationUploadData
([Payment_Type],[GeneratorID],[FiT_ID_Initial],[FiT_ID_Ext1],[FiT_ID_Ext2],[BP_ID],[CONTRACT_ID],[INSTALLATION_ID],[Initial_System_Pct_Split],[Extension_1_Pct_Split],[Extension_2_Pct_Split],[Export_Status],[ExpPct1],[Initial_System_Export_Rate],[Current_Price(Export)],[ExpPct2],[InitSysExpRate2],[Current_Price(InitExp2)],[Extension_1_Export_Rate],[Current_Price(Export1)],[Extension_2_Export_Rate],[Current_Price(Export2)],[PaidToREAD_ID],[LatestPaidToRead_Meter_Read_Reason],[Export_Read(OLD)],[Generation_Read(OLD)],[Meter_Read_Date(OLD)],[LatestREAD_ID],[ReadForQuarter_Meter_Read_Reason],[Export_Read(NEW)]     ,[Generation_Read(NEW)],[Meter_Read_Date(NEW)],[Initial_System_Tariff_Rate],[Current Price(Initial)],[Extension_1_Tariff_Rate],[Current Price(Ext1)],[Extension_2_Tariff_Rate],[Current Price(Ext2)],No_of_Splits,Start_Fit_Year,END_Fit_Year)

SELECT 
[Payment_Type],
[GeneratorID],
[FiT_ID_Initial],
[FiT_ID_Ext1],
[FiT_ID_Ext2],
[BP_ID],
[CONTRACT_ID],
[INSTALLATION_ID],
[Initial_System_Pct_Split],
[Extension_1_Pct_Split],
[Extension_2_Pct_Split],
[Export_Status],[ExpPct1],
[Initial_System_Export_Rate],
[Current_Price(Export)],
[ExpPct2],
[InitSysExpRate2],
[Current_Price(InitExp2)],
[Extension_1_Export_Rate],
[Current_Price(Export1)],
[Extension_2_Export_Rate],
[Current_Price(Export2)],
[PaidToREAD_ID],
[LatestPaidToRead_Meter_Read_Reason],
[Export_Read(OLD)],
[Generation_Read(OLD)],
[Meter_Read_Date(OLD)],
LatestRead_ID,
ReadForQuarter_Meter_Read_Reason,

CASE 
	WHEN [Export_Read(NEW)] IS NULL THEN ''
	ELSE ([Export_Read(NEW)] - [Export_Read(OLD)]) / ([Meter_Read_Date(NEW)] - [Meter_Read_Date(OLD)]) * (Meter_Read_Date_New_Calc - [Meter_Read_Date(OLD)]) +[Export_Read(OLD)]
	END AS Export_Read_New_Calc,

Meter_Read_Date_New_Calc,
[Meter_Read_Date(NEW)],
[Initial_System_Tariff_Rate],
[Current Price(Initial)],
[Extension_1_Tariff_Rate],
[Current Price(Ext1)],
[Extension_2_Tariff_Rate],
[Current Price(Ext2)],
(END_Fit_Year - Start_Fit_Year) As No_of_Splits,
Start_Fit_Year,
END_Fit_Year

FROM

(SELECT
([Payment_Type],
[GeneratorID],
[FiT_ID_Initial],
[FiT_ID_Ext1],
[FiT_ID_Ext2],
[BP_ID],
[CONTRACT_ID],
[INSTALLATION_ID],
[Initial_System_Pct_Split],
[Extension_1_Pct_Split],
[Extension_2_Pct_Split],
[Export_Status],[ExpPct1],
[Initial_System_Export_Rate],
[Current_Price(Export)],
[ExpPct2],
[InitSysExpRate2],
[Current_Price(InitExp2)],
[Extension_1_Export_Rate],
[Current_Price(Export1)],[Extension_2_Export_Rate],[Current_Price(Export2)],[PaidToREAD_ID],[LatestPaidToRead_Meter_Read_Reason],[Export_Read(OLD)],[Generation_Read(OLD)],[Meter_Read_Date(OLD)],
'CALC' AS LatestRead_ID,'CALC' AS ReadForQuarter_Meter_Read_Reason,
[Export_Read(NEW)], 
[Generation_Read(NEW)],
CASE 
WHEN Start_Fit_Year = 1 THEN '31Mar2010'
WHEN Start_Fit_Year = 2 THEN '31Mar2011'
WHEN Start_Fit_Year = 3 THEN '31Mar2012'
WHEN Start_Fit_Year = 4 THEN '31Mar2013'
WHEN Start_Fit_Year = 5 THEN '31Mar2014'
WHEN Start_Fit_Year = 6 THEN '31Mar2015'
WHEN Start_Fit_Year = 7 THEN '31Mar2016'
WHEN Start_Fit_Year = 8 THEN '31Mar2017'
ELSE NULL
END AS Meter_Read_Date_New_Calc,
[Meter_Read_Date(NEW)],
[Initial_System_Tariff_Rate],[Current Price(Initial)],[Extension_1_Tariff_Rate],[Current Price(Ext1)],[Extension_2_Tariff_Rate],[Current Price(Ext2)],(END_Fit_Year - Start_Fit_Year) As No_of_Splits,Start_Fit_Year,END_Fit_Year

FROM [dbo].[Levelisation_Generator_Initial_Extract_vw]
WHERE END_Fit_Year - Start_Fit_Year = 1) Split1_1 




/* This inserts the Second Row */

INSERT INTO LevelisationUploadData
([Payment_Type],[GeneratorID],[FiT_ID_Initial],[FiT_ID_Ext1],[FiT_ID_Ext2],[BP_ID],[CONTRACT_ID],[INSTALLATION_ID],[Initial_System_Pct_Split],[Extension_1_Pct_Split],[Extension_2_Pct_Split],[Export_Status],[ExpPct1],[Initial_System_Export_Rate],[Current_Price(Export)],[ExpPct2],[InitSysExpRate2],[Current_Price(InitExp2)],[Extension_1_Export_Rate],[Current_Price(Export1)],[Extension_2_Export_Rate],[Current_Price(Export2)],[PaidToREAD_ID],[LatestPaidToRead_Meter_Read_Reason],[Export_Read(OLD)],[Generation_Read(OLD)],[Meter_Read_Date(OLD)],[LatestREAD_ID],[ReadForQuarter_Meter_Read_Reason],[Export_Read(NEW)]     ,[Generation_Read(NEW)],[Meter_Read_Date(NEW)],[Initial_System_Tariff_Rate],[Current Price(Initial)],[Extension_1_Tariff_Rate],[Current Price(Ext1)],[Extension_2_Tariff_Rate],[Current Price(Ext2)],No_of_Splits,Start_Fit_Year,END_Fit_Year)

SELECT
[Payment_Type],[GeneratorID],[FiT_ID_Initial],[FiT_ID_Ext1],[FiT_ID_Ext2],[BP_ID],[CONTRACT_ID],[INSTALLATION_ID],[Initial_System_Pct_Split],[Extension_1_Pct_Split],[Extension_2_Pct_Split],[Export_Status],[ExpPct1],[Initial_System_Export_Rate],[Current_Price(Export)],[ExpPct2],[InitSysExpRate2],[Current_Price(InitExp2)],[Extension_1_Export_Rate],[Current_Price(Export1)],[Extension_2_Export_Rate],[Current_Price(Export2)],'CALC','CALC',[Export_Read(OLD)],[Generation_Read(OLD)],
CASE 
WHEN Start_Fit_Year = 1 THEN '01Apr2010'
WHEN Start_Fit_Year = 2 THEN '01Apr2011'
WHEN Start_Fit_Year = 3 THEN '01Apr2012'
WHEN Start_Fit_Year = 4 THEN '01Apr2013'
WHEN Start_Fit_Year = 5 THEN '01Apr2014'
WHEN Start_Fit_Year = 6 THEN '01Apr2015'
WHEN Start_Fit_Year = 7 THEN '01Apr2016'
WHEN Start_Fit_Year = 8 THEN '01Apr2017'
ELSE NULL
END AS Meter_Read_Date_Old,
[LatestREAD_ID],[ReadForQuarter_Meter_Read_Reason],[Export_Read(NEW)],[Generation_Read(NEW)],[Meter_Read_Date(NEW)],[Initial_System_Tariff_Rate],[Current Price(Initial)],[Extension_1_Tariff_Rate],[Current Price(Ext1)],[Extension_2_Tariff_Rate],[Current Price(Ext2)],(END_Fit_Year - Start_Fit_Year) -1 As No_of_Splits,Start_Fit_Year,END_Fit_Year

FROM [dbo].[Levelisation_Generator_Initial_Extract_vw]
WHERE END_Fit_Year - Start_Fit_Year = 1


