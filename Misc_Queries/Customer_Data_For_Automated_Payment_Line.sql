USE Datsup;

SELECT DISTINCT 
CASE
       WHEN substring(Mobile,1,2)='07' THEN '+447'+substring(Mobile,3,len(Mobile)-2)
       WHEN substring(mobile,1,5)='00447' THEN '+447'+substring(Mobile,6,len(Mobile)-5)
       WHEN substring(mobile,1,3)='447' THEN '+447'+substring(Mobile,4,len(Mobile)-3)
       END AS Contact,
CASE
       WHEN substring(Other_Mobile,1,2)='07' THEN '+447'+substring(Other_Mobile,3,len(Other_Mobile)-2)
       WHEN substring(Other_mobile,1,5)='00447' THEN '+447'+substring(Other_Mobile,6,len(Other_Mobile)-5)
       WHEN substring(Other_mobile,1,3)='447' THEN '+447'+substring(Other_Mobile,4,len(Other_Mobile)-3)
       END AS Other_Mobile,
Phone,
Other_Phone,
C.Contract_Account AS Contract_Account,
CASE WHEN LEFT(Rate_Category,1)='E' THEN 'Electricity' ELSE NULL END AS Division_01,
CASE WHEN LEFT(Rate_Category,1)='G' THEN 'Gas' ELSE NULL END AS Division_02,
C.MO_Date AS Move_Out_Date,
P.House_No AS Premise_House_Number,
p.Street3 AS Premise_House_Name,
P.Room_No1 AS Premise_Flat_Number,
P.Street AS Premise_Street,
P.City AS Premise_City,
P.Post_Code AS Premise_PostCode,
B.House_Number AS BP_House_Number,
B.Street3 AS BP_House_Name,
B.Room_No AS BP_Flat_Number,
B.Street AS BP_Street,
B.City AS BP_City,
B.PostCode AS BP_PostCode,
C.BP AS BP,
C.Title AS Title,
C.First_Name AS First_Name,
C.Last_Name Last_Name,
cd.Email AS Email_Address,
CASE WHEN LEFT(Rate_Category,1)='E' THEN Outstanding_Amount ELSE NULL END AS Amount_Elec,
CASE WHEN LEFT(Rate_Category,1)='G' THEN Outstanding_Amount ELSE NULL END AS Amount_Gas

FROM   [UH-GENDB-01].DatSup.dbo.CAMPAN_Data c 
       INNER JOIN [UH-GENDB-01].DatSup.dbo.BillingAddress b on c.Contract_Account = b.Contract_Number
       INNER JOIN [UH-GENDB-01].DatSup.dbo.Premise_Address p on c.Contract_Account = p.Contract_Account
       INNER JOIN [UH-GENDB-01].DatSup.dbo.Contact_Detail cd on c.BP = cd.Business_Partner_ID
       LEFT JOIN (Select Cont_Account, SUM(Amount) AS Outstanding_Amount From [UH-GENDB-01].DatSup.dbo.dbtmgmtexport Group by Cont_Account) d on c.Contract_Account = d.Cont_Account
	   LEFT JOIN [LH-GENDB-01].bi_raw.dbo.FKKMAZE F on c.Contract_Account = f.vkont

WHERE
(C.MO_Date <= getdate() and Outstanding_Amount > 0)
OR
C.MO_Date > getdate()
