SELECT [Payment_Type]
      ,[GeneratorID]
      ,[FiT_ID_Initial]
      ,[FiT_ID_Ext1]
      ,[FiT_ID_Ext2]
      ,[BP_ID]
      ,[CONTRACT_ID]
      ,[INSTALLATION_ID]
      ,[Initial_System_Pct_Split]
      ,[Extension_1_Pct_Split]
      ,[Extension_2_Pct_Split]
      ,[Export_Status]
      ,[ExpPct1]
      ,[Initial_System_Export_Rate]
      ,[Current_Price(Export)]
      ,[ExpPct2]
      ,[InitSysExpRate2]
      ,[Current_Price(InitExp2)]
      ,[Extension_1_Export_Rate]
      ,[Current_Price(Export1)]
      ,[Extension_2_Export_Rate]
      ,[Current_Price(Export2)]
      ,[PaidToREAD_ID]
      ,[LatestPaidToRead_Meter_Read_Reason]
      ,[Export_Read(OLD)]
      ,[Generation_Read(OLD)]
      ,[Meter_Read_Date(OLD)]
      ,[LatestREAD_ID]
      ,[ReadForQuarter_Meter_Read_Reason]
      ,[Export_Read(NEW)]
      ,[Generation_Read(NEW)]
      ,[Meter_Read_Date(NEW)]
      ,[Initial_System_Tariff_Rate]
      ,[Current Price(Initial)]
      ,[Extension_1_Tariff_Rate]
      ,[Current Price(Ext1)]
      ,[Extension_2_Tariff_Rate]
      ,[Current Price(Ext2)]
 	  ,END_Fit_Year - Start_Fit_Year As No_of_Splits
	  ,Start_Fit_Year
	  ,END_Fit_Year



	  /*  Add these checks into the final extract, not into this Upload SP.  Just putting them here for now. 

CASE
	WHEN Initial_System_Pct_Split + Extension_1_Pct_Split + Extension_2_Pct_Split <> 1 THEN 'Percentage Split Error' 
	WHEN Extension_1_Pct_Split > 0 AND FiT_ID_Ext1 is NULL THEN 'Ext 1 Percentage Split Error'
	WHEN Extension_2_Pct_Split > 0 AND FiT_ID_Ext2 is NULL THEN 'Ext 2 Percentage Split Error'
	ELSE NULL
END AS System_Split_Check,

CASE
	WHEN Export_Status IN ('Deemed Export','Standard Tariff') AND Initial_System_Export_Rate is NULL THEN 'Initial System Export Error' 
	WHEN Export_Status IN ('Deemed Export','Standard Tariff') AND FiT_ID_Ext1 Is not null AND Extension_1_Export_Rate is NULL THEN 'Extension 1 System Export Error' 
	WHEN Export_Status IN ('Deemed Export','Standard Tariff') AND FiT_ID_Ext2 Is not null AND Extension_2_Export_Rate is NULL THEN 'Extension 2 System Export Error' 
	WHEN Export_Status IN ('Negotiated Tariff','No Export','No Export (Off Grid)', NULL) AND Initial_System_Export_Rate is NOT NULL THEN 'Initial System Export Error' 
	WHEN Export_Status IN ('Negotiated Tariff','No Export','No Export (Off Grid)', NULL) AND Extension_1_Export_Rate is NOT NULL THEN 'Extension 1 System Export Error' 
	WHEN Export_Status IN ('Negotiated Tariff','No Export','No Export (Off Grid)', NULL) AND Extension_2_Export_Rate is NOT NULL THEN 'Extension 2 System Export Error' 
	ELSE NULL
END AS Export_Rate_Check

*/
FROM [dbo].[Levelisation_Generator_Initial_Extract_vw]


