select 
GeneratorID,
Company,
Title,
First_Name,
Last_Name,
BP.Address_1,
BP.Address_2,
BP.City,
BP.County,
BP.Postal_Code,
Email_Address,
Home_Phone,
Mobile_Phone,
SAP_BP,
SAP_CA,
Payment_Method,
Bank_Acc_Name,
Sort_Code,
Bank_Acc_Number,
VAT_Reg_Number,
FiT_ID_Initial
from BP 
inner join CONTRACT on BP.BP_ID = CONTRACT.BP_ID_link
inner join INSTALLATION on CONTRACT.INSTALLATION_ID_link = INSTALLATION.INSTALLATION_ID