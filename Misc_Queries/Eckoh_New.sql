IF OBJECT_ID('tempdb.dbo.##PAYG_Temp', 'U') is not null
  DROP TABLE ##PAYG_Temp

Create Table ##PAYG_Temp (
Meter_Serial_Number nvarchar(50), 
MPAN_MPRN nvarchar(50))

INSERT INTO ##PAYG_Temp
SELECT 
	J0004,
	J0003
FROM AFMSELECLIVEP4..CUSTOMER.MPAN C_MPAN LEFT JOIN AFMSELECLIVEP4..CUSTOMER.METER C_METER 
	ON  C_MPAN.UNIQ_ID = C_METER.MPAN_LNK
WHERE J0049 <=getdate() AND (J0117 >=getdate() OR J0117 is null)
AND J0848 <=getdate() AND (ETD_MSID >=getdate() OR ETD_MSID is null)


INSERT INTO ##PAYG_Temp
SELECT 
	K0544,K0533
FROM 
	AFMSELECLIVEP4..DATAFLOW_RO.CONTINUOUS_MPRN C_MPRN 
	LEFT JOIN AFMSELECLIVEP4..DATAFLOW_RO.COMBINED_METER C_MTR 
	ON C_MPRN.MPRN_PK = C_MTR.MPRN_FK
WHERE K0116 <=getdate() and (K0882 >=getdate() OR K0882 is null)
AND K0518 <=getdate() and (K0543 >=getdate() OR K0543 is null)   


SELECT 'Contract_Account','Prepayment_ID','Fuel','Meter_Serial_Number','Supply_Building_Code','Supply_Room','Supply_Floor','Supply_Street_2','Supply_Street_3','Supply_House_Number','Supply_Street','Supply_Street_4','Supply_Street_5','District','Supply_City','Supply_County','Supply_Post_Code','Supply_Country','Billing_Building_Code','Billing_Room','Billing_Floor','Billing_Street_2','Billing_Street_3','Billing_House_Number','Billing_Street','Billing_Street_4','Billing_Street_5','Billing_District','Billing_City','Billing_County','Billing_Post_Code','Billing_Country','Effective_From_Date'
UNION ALL
SELECT DISTINCT CONVERT(varchar(13),campan_Data.contract_account) as Contract_Account,
RTRIM(dbo.fnGetLuhn(Concat('982601500',Division,campan_Data.Contract))) AS Prepayment_ID,
'0'+Convert(varchar(1),CAMPAN_Data.Division) as Fuel,
CASE WHEN (Substring (Meter_Serial_Number,3,1) = 'P' And LEN (Meter_Serial_Number) = 10 AND  isnumeric(LEFT (Meter_Serial_Number,2)) = 1 AND Division = 1) THEN Meter_Serial_Number
WHEN (CAMPAN_Data.MI_Date <= getdate() and Campan_Data.MO_Date >= getdate()  AND (Left (Meter_Serial_Number,3) = 'G4P' OR Left (Meter_Serial_Number,4) = 'G14P') AND Len (meter_serial_Number) in (10,11,14)) THEN Meter_Serial_Number
ELSE NULL
END AS Meter_Serial_Number, 
'' as Supply_Building_Code,
CASE WHEN Premise_Address.Room_No1 = Premise_Address.Room_No2 THEN Premise_Address.Room_No1
	WHEN Premise_Address.Room_No1 IS NULL THEN ''
ELSE Concat(Premise_Address.Room_No1,' ',Premise_Address.Room_No2)
END AS Supply_Room,
REPLACE( NULLIF(Premise_Address.[Floor],''),',',' ') AS Supply_Floor,
REPLACE( NULLIF(Premise_Address.Supplement1,''),',',' ') AS Supply_Street_2,
REPLACE( NULLIF(Premise_Address.Street3,''),',',' ') Supply_Street_3,
REPLACE( NULLIF(Premise_Address.House_No,''),',',' ') Supply_House_Number,
REPLACE( NULLIF(Premise_Address.Street,''),',',' ') Supply_Street,
REPLACE( NULLIF(Premise_Address.Street4,''),',',' ') Supply_Street_4,
REPLACE( NULLIF(Premise_Address.Street5,''),',',' ') Supply_Street_5,
'' AS District,
REPLACE( NULLIF(Premise_Address.City,''),',',' ') AS Supply_City,
REPLACE( NULLIF(Premise_Address.Description,''),',',' ') AS Supply_County,
REPLACE( NULLIF(Premise_Address.Post_Code,''),',',' ') AS Supply_Post_Code,
'GB' AS Supply_Country,
'' AS Billing_Building_Code,
REPLACE( NULLIF(BillingAddress.Room_No,''),',',' ') AS Billing_Room,
REPLACE( NULLIF(BillingAddress.[Floor],''),',',' ') AS Billing_Floor,
REPLACE( NULLIF(BillingAddress.Supplement,''),',',' ') AS Billing_Street_2,
REPLACE( NULLIF(BillingAddress.Street3,''),',',' ') AS Billing_Street_3,
REPLACE( NULLIF(BillingAddress.House_Number,''),',',' ') AS Billing_House_Number,
REPLACE( NULLIF(BillingAddress.Street,''),',',' ') AS Billing_Street,
REPLACE( NULLIF(BillingAddress.Street4,''),',',' ') AS Billing_Street_4,
REPLACE( NULLIF(BillingAddress.Street5,''),',',' ') AS Billing_Street_5,
'' AS Billing_District,
REPLACE( NULLIF(BillingAddress.City,''),',',' ') AS Billing_City,
REPLACE( NULLIF(BillingAddress.Description,''),',',' ') AS Billing_County,
REPLACE( NULLIF(BillingAddress.PostCode,''),',',' ') AS Billing_Post_Code,
CASE WHEN BillingAddress.PostCode IS NULL THEN ''
ELSE 'GB'
END AS Billing_Country,
ISNULL(CASE WHEN CONVERT(DATE, Eckoh_EFD.date) = '1900-01-01' THEN '' 
ELSE CONVERT(CHAR(10), Eckoh_EFD.date,126) END, '') AS Effective_From_Date

FROM CAMPAN_Data left join Premise_Address on 
CAMPAN_Data.Contract = Premise_Address.Contract
left join BillingAddress on 
CAMPAN_Data.Contract_Account = BillingAddress.Contract_Number
left join ##PAYG_Temp on
CAMPAN_Data.MPAN_MPRN = ##PAYG_Temp.MPAN_MPRN
left join Eckoh_EFD on
CAMPAN_Data.MPAN_MPRN = Eckoh_EFD.MPAN_MPRN

WHERE
(CAMPAN_Data.MI_Date <= getdate() and Campan_Data.MO_Date >= getdate() AND Eckoh_EFD.Date <= getdate())
OR
(Campan_data.Rate_Category like '%'+'D_P'+'%'
AND
((CAMPAN_Data.MI_Date <= getdate() and Campan_Data.MO_Date >= getdate() 
AND Substring (Meter_Serial_Number,3,1) = 'P'
And LEN (Meter_Serial_Number) = 10
AND ISNUMERIC(Left (Meter_Serial_Number,2)) <= 99
AND Division = 1)
OR
(CAMPAN_Data.MI_Date <= getdate() and Campan_Data.MO_Date >= getdate() 
AND (Left (Meter_Serial_Number,1) = 'G' 
AND (Isnumeric(Substring (Meter_Serial_Number,2,1)) <= 9 AND SUBSTRING (Meter_Serial_Number,3,1) = 'P'
OR	Isnumeric(Substring (Meter_Serial_Number,2,2)) <= 99 AND SUBSTRING (Meter_Serial_Number,4,1) = 'P')
AND Len (meter_serial_Number) in (10,11,14)))))
