select top 5 
a.server_name, 
a.database_name, 
backup_finish_date, 
a.backup_size,
CASE a.[type] 
 WHEN 'D' THEN 'Full'
 WHEN 'I' THEN 'Differential'
 WHEN 'L' THEN 'Transaction Log'
 ELSE a.[type]
END as BackupType
 ,b.physical_device_name
from 
msdb.dbo.backupset a join msdb.dbo.backupmediafamily b on a.media_set_id = b.media_set_id
where 
a.database_name Like 'microtricity%'
order by 
a.backup_finish_date desc