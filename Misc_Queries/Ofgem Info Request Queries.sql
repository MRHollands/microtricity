
/*
Author:		Matthew Hollands
Date:		03/09/2018

Server:		uh-gendb-01
Database:	Microtricity2 and Datsup
*/

CREATE TABLE #Ofgem_Data
(Data_Point VARCHAR(500),
 Data_Count INT
);

-- Count of FIT_ID, including extensions, that have an Account Status which is not Closed or COT Complete.
INSERT INTO #Ofgem_Data
       SELECT 'No of FIT installations registered',
              COUNT(fit_id_initial) + COUNT(FiT_ID_Ext1) + COUNT(FiT_ID_Ext2)
       FROM Core_Data_vw
       WHERE Account_Status NOT IN('Closed', 'COT Complete');

-- Count of FIT_ID, including extensions, that have an Account Status which is not Closed or COT Complete.
INSERT INTO #Ofgem_Data
       SELECT 'TIC Less Than 30KW',
              COUNT(fit_id_initial) + COUNT(FiT_ID_Ext1) + COUNT(FiT_ID_Ext2)
       FROM Core_Data_vw
       WHERE Account_Status NOT IN('Closed', 'COT Complete')
       AND Total_Installed_Capacity_kW < 30;


-- FIT installations where your company is the FIT licensee and import supplier

-- Number of FIT installations where this is the case
INSERT INTO #Ofgem_Data
       SELECT 'Ecotricity Customer - FIT installations where your company is the FIT licensee and import supplier',
              COUNT(M.fit_id_initial) + COUNT(M.FiT_ID_Ext1) + COUNT(M.FiT_ID_Ext2) AS No_FITs_Ecotricity_Customers
       FROM Core_Data_vw M
            INNER JOIN datsup.dbo.CAMPAN_Data E ON M.Sap_ca = E.Contract_Account
       WHERE E.MO_Date = '9999-12-31 00:00:00.000';

--  Number of these installations have had SMART meters deployed
INSERT INTO #Ofgem_Data
       SELECT 'Ecotricity Customer - Number of these installations have had SMART meters deployed',
              COUNT(M.fit_id_initial) + COUNT(M.FiT_ID_Ext1) + COUNT(M.FiT_ID_Ext2)
       FROM Core_Data_vw M
            INNER JOIN datsup.dbo.Ecoes_Microtricity E ON M.Supply_MPAN = E.MPAN
       WHERE E.MeterType IN('S1', 'S2A', 'S2B', 'S2C', 'S2AD', 'S2BD', 'S2CD', 'S2ADE', 'S2BDE', 'S2CDE');

--  Number of installations where SMART meters have been deployed are being paid export payments on a metered basis
INSERT INTO #Ofgem_Data
       SELECT 'Ecotricity Customer - Number of installations where SMART meters have been deployed are being paid export payments on a metered basis',
              COUNT(M.fit_id_initial) + COUNT(M.FiT_ID_Ext1) + COUNT(M.FiT_ID_Ext2)
       FROM Core_Data_vw M
            INNER JOIN datsup.dbo.Ecoes_Microtricity E ON M.Supply_MPAN = E.MPAN
            INNER JOIN datsup.dbo.CAMPAN_Data C ON M.Sap_ca = C.Contract_Account
       WHERE E.MeterType IN('S1', 'S2A', 'S2B', 'S2C', 'S2AD', 'S2BD', 'S2CD', 'S2ADE', 'S2BDE', 'S2CDE')
            AND M.Export_Status IN('Negotiated Tariff', 'Standard Tariff')
       AND C.MO_Date = '9999-12-31 00:00:00.000';

--  Number of installations where SMART meters have been deployed are being paid export payments on a deemed basis
INSERT INTO #Ofgem_Data
       SELECT 'Ecotricity Customer - Number of installations where SMART meters have been deployed are being paid export payments on a deemed basis',
              COUNT(M.fit_id_initial) + COUNT(M.FiT_ID_Ext1) + COUNT(M.FiT_ID_Ext2)
       FROM Core_Data_vw M
            INNER JOIN datsup.dbo.Ecoes_Microtricity E ON M.Supply_MPAN = E.MPAN
            INNER JOIN datsup.dbo.CAMPAN_Data C ON M.Sap_ca = C.Contract_Account
       WHERE E.MeterType IN('S1', 'S2A', 'S2B', 'S2C', 'S2AD', 'S2BD', 'S2CD', 'S2ADE', 'S2BDE', 'S2CDE')
            AND M.Export_Status IN('Deemed Export (<30kW)')
       AND C.MO_Date = '9999-12-31 00:00:00.000';


	   
--FIT installations where your company is the FIT licensee but not import supplier

-- Number of FIT installations where this is the case
INSERT INTO #Ofgem_Data
       SELECT 'NOT Ecotricity Customer - FIT installations where your company is the FIT licensee and import supplier',
              COUNT(M.fit_id_initial) + COUNT(M.FiT_ID_Ext1) + COUNT(M.FiT_ID_Ext2) AS No_FITs_Ecotricity_Customers
       FROM Core_Data_vw M
            LEFT JOIN datsup.dbo.CAMPAN_Data E ON M.Sap_ca = E.Contract_Account
       WHERE E.Contract_Account IS NULL;

--  Number of these installations that have had SMART meters deployed?
INSERT INTO #Ofgem_Data
       SELECT 'NOT Ecotricity Customer - Number of these installations have had SMART meters deployed',
              COUNT(M.fit_id_initial) + COUNT(M.FiT_ID_Ext1) + COUNT(M.FiT_ID_Ext2)
       FROM Core_Data_vw M
            INNER JOIN datsup.dbo.Ecoes_Microtricity E ON M.Supply_MPAN = E.MPAN
            LEFT JOIN datsup.dbo.CAMPAN_Data C ON M.Sap_ca = C.Contract_Account
       WHERE E.MeterType IN('S1', 'S2A', 'S2B', 'S2C', 'S2AD', 'S2BD', 'S2CD', 'S2ADE', 'S2BDE', 'S2CDE')
            AND C.Contract_Account IS NULL;
	   
--  Number of these installations where you do not know whether a SMART meter has been deployed?
INSERT INTO #Ofgem_Data
       SELECT 'NOT Ecotricity Customer - Number of these installations where you do not know whether a SMART meter has been deployed',
              COUNT(M.fit_id_initial) + COUNT(M.FiT_ID_Ext1) + COUNT(M.FiT_ID_Ext2)
       FROM Core_Data_vw M
            INNER JOIN datsup.dbo.Ecoes_Microtricity E ON M.Supply_MPAN = E.MPAN
            LEFT JOIN datsup.dbo.CAMPAN_Data C ON M.Sap_ca = C.Contract_Account
       WHERE E.MeterType NOT IN('S1', 'S2A', 'S2B', 'S2C', 'S2AD', 'S2BD', 'S2CD', 'S2ADE', 'S2BDE', 'S2CDE')
            AND C.Contract_Account IS NULL;

	   --  Number of installations where you are aware that a SMART meters have been deployed that are being paid export payments on a metered basis?
INSERT INTO #Ofgem_Data
       SELECT 'NOT Ecotricity Customer - Number of installations where you are aware that a SMART meters have been deployed that are being paid export payments on a metered basis',
              COUNT(M.fit_id_initial) + COUNT(M.FiT_ID_Ext1) + COUNT(M.FiT_ID_Ext2)
       FROM Core_Data_vw M
            INNER JOIN datsup.dbo.Ecoes_Microtricity E ON M.Supply_MPAN = E.MPAN
            LEFT JOIN datsup.dbo.CAMPAN_Data C ON M.Sap_ca = C.Contract_Account
       WHERE E.MeterType IN('S1', 'S2A', 'S2B', 'S2C', 'S2AD', 'S2BD', 'S2CD', 'S2ADE', 'S2BDE', 'S2CDE')
            AND M.Export_Status IN('Negotiated Tariff', 'Standard Tariff')
       AND C.Contract_Account IS NULL;

	   --  Count of FIT_ID, including extrensions, that does not have a live supply account but the MPANs on ecoes has a meter type S1 or S2/any smart meter idetifier will give us this number AND export status is "Deeemed"
INSERT INTO #Ofgem_Data
       SELECT 'NOT Ecotricity Customer - Number of installations where you are aware that a SMART meters have been deployed that are being paid export payments on a deemed basis',
              COUNT(M.fit_id_initial) + COUNT(M.FiT_ID_Ext1) + COUNT(M.FiT_ID_Ext2)
       FROM Core_Data_vw M
            INNER JOIN datsup.dbo.Ecoes_Microtricity E ON M.Supply_MPAN = E.MPAN
            LEFT JOIN datsup.dbo.CAMPAN_Data C ON M.Sap_ca = C.Contract_Account
       WHERE E.MeterType IN('S1', 'S2A', 'S2B', 'S2C', 'S2AD', 'S2BD', 'S2CD', 'S2ADE', 'S2BDE', 'S2CDE')
            AND M.Export_Status IN('Deemed Export (<30kW)')
       AND C.Contract_Account IS NULL;
SELECT *
FROM #Ofgem_Data;
DROP TABLE #Ofgem_Data;