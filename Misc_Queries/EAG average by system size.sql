

select fit_id_initial,
Generation_Type,
Total_Installed_Capacity_kW,
CASE
WHEN Total_Installed_Capacity_kW <= 2.09 THEN '0-2kw'
WHEN Total_Installed_Capacity_kW Between 2.1 AND 4.09 THEN '2.1-4kw'
WHEN Total_Installed_Capacity_kW Between 4.1 AND 6.09 THEN '4.1-6kw'
WHEN Total_Installed_Capacity_kW Between 6.1 AND 8.09 THEN '6.1-8kw'
WHEN Total_Installed_Capacity_kW Between 8.1 AND 10.09 THEN '8.1-10kw'
WHEN Total_Installed_Capacity_kW Between 10.1 AND 15.09 THEN '10.1-15kw'
WHEN Total_Installed_Capacity_kW Between 15.1 AND 20.09 THEN '15.1-20kw'
WHEN Total_Installed_Capacity_kW Between 20.1 AND 25.09 THEN '20.1-25kw'
WHEN Total_Installed_Capacity_kW Between 25.1 AND 30.09 THEN '25.1-30kw'
WHEN Total_Installed_Capacity_kW Between 30.1 AND 35.09 THEN '30.1-35kw'
WHEN Total_Installed_Capacity_kW Between 35.1 AND 40.09 THEN '35.1-40kw'
WHEN Total_Installed_Capacity_kW Between 40.1 AND 45.09 THEN '40.1-45kw'
WHEN Total_Installed_Capacity_kW Between 45.1 AND 50.09 THEN '45.1-50kw'
WHEN Total_Installed_Capacity_kW Between 50.1 AND 55.09 THEN '50.1-55kw'
WHEN Total_Installed_Capacity_kW Between 55.1 AND 60.09 THEN '55.1-60kw'
WHEN Total_Installed_Capacity_kW Between 75.1 AND 80.09 THEN '75.1-80kw'
WHEN Total_Installed_Capacity_kW Between 80.1 AND 85.09 THEN '80.1-85kw'
WHEN Total_Installed_Capacity_kW Between 90.1 AND 95.09 THEN '90.1-95kw'
WHEN Total_Installed_Capacity_kW Between 95.1 AND 100.09 THEN '95.1-100kw'
WHEN Total_Installed_Capacity_kW Between 100.1 AND 150.09 THEN '100.1-150kw'
WHEN Total_Installed_Capacity_kW Between 200.1 AND 250.09 THEN '200.1-250kw'
WHEN Total_Installed_Capacity_kW Between 450.1 AND 500.09 THEN '450.1-500kw'
WHEN Total_Installed_Capacity_kW Between 500.1 AND 600.09 THEN '500.1-600kw'
WHEN Total_Installed_Capacity_kW Between 600.1 AND 700.09 THEN '600.1-700kw'
WHEN Total_Installed_Capacity_kW Between 800.1 AND 900.09 THEN '800.1-900kw'
WHEN Total_Installed_Capacity_kW >= 1000 THEN '2.1-4kw'
ELSE NULL
END AS Band


from INSTALLATION
Where FiT_ID_Initial = 'FIT00000960-1'