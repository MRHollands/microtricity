IF OBJECT_ID('tempdb..#MAXlog') IS NOT NULL     BEGIN DROP TABLE #Maxlog   END
IF OBJECT_ID('tempdb..#MAXdev') IS NOT NULL     BEGIN DROP TABLE #Maxdev   END

SELECT * INTO #maxlog
FROM (SELECT       BD.anlage,
              BD.bis ,
              RRE.LOGIKZW,
              Max(RRE.BIS) AS MaxVal
FROM   BI_RAW.dbo.easts BD
       INNER JOIN BI_RAW.dbo.EASTE RRE 
ON BD.logikzw = RRE.logikzw
GROUP BY BD.anlage, BD.bis, RRE.LOGIKZW) a

CREATE NONCLUSTERED INDEX maxlog_anlage_ix ON [dbo].[#maxlog] ([anlage])

SELECT * INTO #maxdev 
FROM (SELECT anlage,max(BIS) as MaxDate FROM #Maxlog GROUP BY anlage) MaxDev 

CREATE NONCLUSTERED INDEX maxdev_anlage_ix ON [dbo].[#maxDev] ([anlage],[MaxDate])

SELECT BD.anlage AS Installation,
       sum(RRE.PERVERBR) AS EAC
FROM   BI_RAW.dbo.easts BD
                     INNER JOIN BI_RAW.dbo.EASTE RRE 
                                         ON     BD.logikzw = RRE.logikzw
                     INNER JOIN #MaxLog m 
                                         ON     BD.anlage = M.anlage 
AND BD.bis  = M.bis 
                                                AND RRE.LOGIKZW = M.LOGIKZW 
AND RRE.BIS  = M.MaxVal
                     INNER JOIN #MaxDev md 
                                         ON     BD.anlage = Md.anlage 
AND BD.bis = md.MaxDate
GROUP BY BD.anlage
