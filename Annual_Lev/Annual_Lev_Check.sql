select 
Annual_Paid_Summary.Contract_ID,
annual_paid_summary.qtr,
Fit_Period,
Total_Total_Payment,
T_Original_Payment_Total
 from Annual_Paid_Summary inner join Annual_Lev_Paid on annual_paid_summary.contract_ID = annual_lev_paid.contract_id and Annual_Paid_Summary.Qtr = Right(annual_lev_paid.fit_period,2)
 WHERE Total_Total_Payment - T_Original_Payment_Total <> 0
 Order by CONTRACT_ID
