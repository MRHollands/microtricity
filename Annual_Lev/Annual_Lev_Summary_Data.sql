Select
CASE
WHEN FIT_PERIOD is null THEN 'Total'
ELSE FIT_PERIOD
END AS FIT_PERIOD,
Units_Exported,
Deemed_Units_Exported,
Units_Generated,
Generation_Payment,
ExpPay_if_Std,
ExpPay_Not_Std,
Export_Payment,
TopUp_Payment,
Total_Payment,
Deemed_Export_Payment_For_Report
From
(
Select
FIT_Period,
SUM(Units_Exported) as Units_Exported,
SUM(Deemed_Units_Exported) as Deemed_Units_Exported,
SUM(Units_Generated) as Units_Generated,
SUM(Generation_Payment) as Generation_Payment,
SUM(ExpPay_if_Std) as ExpPay_if_Std,
SUM(ExpPay_Not_Std) as ExpPay_Not_Std,
SUM(Export_Payment) as Export_Payment,
SUM(TopUp_Payment) as TopUp_Payment,
SUM(Total_Payment) as Total_Payment,
SUM(Deemed_Export_Payment_For_Report) as Deemed_Export_Payment_For_Report

From
(
Select
Fit_Period,
Units_Exported AS Units_Exported,
Deemed_Units_Exported,
Units_Generated,
Generation_Payment,
ExpPay_if_Std,
ExpPay_Not_Std,
Export_Payment,
TopUp_Payment,
Total_Payment,
Deemed_Export_Payment_For_Report
from Annual_Lev_Claimed 

Union ALL

Select 
Fit_Period,
Units_Exported AS Units_Exported,
Deemed_Units_Exported,
Units_Generated,
Generation_Payment,
ExpPay_if_Std,
ExpPay_Not_Std,
Export_Payment,
TopUp_Payment,
Total_Payment,
Deemed_Export_Payment_For_Report
from Annual_Lev_Claimed_Clean
)CL
Group By Fit_Period 
With Rollup)DATA 


Select
SUM(Number_of_Fits) AS Number_of_FITs
From
(
select Distinct
count(Fit_id_initial) AS Number_of_FITs
From Annual_Lev_Claimed

Union ALL

select Distinct
count(Fit_id_initial) AS Number_of_FITs
From Annual_Lev_Claimed_Clean) Data