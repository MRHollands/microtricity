With Statement_Summary AS
(Select
CASE
WHEN [Quarter] is null THEN 'Total'
WHEN [Quarter] = 1 THEN 'F7Q1'
WHEN [Quarter] = 2 THEN 'F7Q2'
WHEN [Quarter] = 3 THEN 'F7Q3'
WHEN [Quarter] = 4 THEN 'F7Q4'
ELSE NULL
END AS FIT_PERIOD,
Units_Exported,
Deemed_Units_Exported,
Units_Generated,
Generation_Payment,
ExpPay_if_Std,
ExpPay_Not_Std,
Export_Payment,
TopUp_Payment,
Total_Payment,
Deemed_Export_Payment_For_Report
From
(
Select
[Quarter],
SUM(Units_Exported) as Units_Exported,
SUM(Deemed_Units_Exported) as Deemed_Units_Exported,
SUM(Units_Generated) as Units_Generated,
SUM(Generation_Payment) as Generation_Payment,
SUM(ExpPay_if_Std) as ExpPay_if_Std,
SUM(ExpPay_Not_Std) as ExpPay_Not_Std,
SUM(Export_Payment) as Export_Payment,
SUM(TopUp_Payment) as TopUp_Payment,
SUM(Total_Payment) as Total_Payment,
SUM(Deemed_Export_Payment_For_Report) as Deemed_Export_Payment_For_Report

From
(
Select
[Quarter],
Units_Exported,
Deemed_Units_Exported,
Units_Generated,
Generation_Payment,
ExpPay_if_Std,
ExpPay_Not_Std,
Export_Payment,
TopUp_Payment,
Total_Payment,
Deemed_Export_Payment_For_Report
from Annual_Lev_Statement

Union ALL

Select 
[Quarter],
Units_Exported,
Deemed_Units_Exported,
Units_Generated,
Generation_Payment,
ExpPay_if_Std,
ExpPay_Not_Std,
Export_Payment,
TopUp_Payment,
Total_Payment,
Deemed_Export_Payment_For_Report
from Annual_Lev_Statement_Clean
)CL
Group By [Quarter]
With Rollup)DATA),


Claimed_Summary AS

(Select
CASE
WHEN FIT_PERIOD is null THEN 'Total'
ELSE FIT_PERIOD
END AS FIT_PERIOD,
Units_Exported,
Deemed_Units_Exported,
Units_Generated,
Generation_Payment,
ExpPay_if_Std,
ExpPay_Not_Std,
Export_Payment,
TopUp_Payment,
Total_Payment,
Deemed_Export_Payment_For_Report
From
(
Select
FIT_Period,
SUM(Units_Exported) as Units_Exported,
SUM(Deemed_Units_Exported) as Deemed_Units_Exported,
SUM(Units_Generated) as Units_Generated,
SUM(Generation_Payment) as Generation_Payment,
SUM(ExpPay_if_Std) as ExpPay_if_Std,
SUM(ExpPay_Not_Std) as ExpPay_Not_Std,
SUM(Export_Payment) as Export_Payment,
SUM(TopUp_Payment) as TopUp_Payment,
SUM(Total_Payment) as Total_Payment,
SUM(Deemed_Export_Payment_For_Report) as Deemed_Export_Payment_For_Report

From
(
Select
Fit_Period,
Units_Exported,
Deemed_Units_Exported,
Units_Generated,
Generation_Payment,
ExpPay_if_Std,
ExpPay_Not_Std,
Export_Payment,
TopUp_Payment,
Total_Payment,
Deemed_Export_Payment_For_Report
from Annual_Lev_Claimed 

Union ALL

Select 
Fit_Period,
Units_Exported,
Deemed_Units_Exported,
Units_Generated,
Generation_Payment,
ExpPay_if_Std,
ExpPay_Not_Std,
Export_Payment,
TopUp_Payment,
Total_Payment,
Deemed_Export_Payment_For_Report
from Annual_Lev_Claimed_Clean
)CL
Group By Fit_Period 
With Rollup)DATA)

Select
C.FIT_PERIOD, 
Convert(Numeric(12,2),c.Units_Exported - S.Units_Exported) AS Units_Exported,
Convert(Numeric(12,2),c.Deemed_Units_Exported - S.Deemed_Units_Exported) AS Deemed_Units_Exported,
Convert(Numeric(12,2),c.Units_Generated - S.Units_Generated) AS Units_Generated,
Convert(Numeric(12,2),c.Generation_Payment - S.Generation_Payment) AS Generation_Payment,
Convert(Numeric(12,2),c.ExpPay_if_Std - S.ExpPay_if_Std) AS ExpPay_if_Std,
Convert(Numeric(12,2),c.ExpPay_Not_Std - S.ExpPay_Not_Std) AS ExpPay_Not_Std,
Convert(Numeric(12,2),c.Export_Payment - S.Export_Payment) AS Export_Payment,
Convert(Numeric(12,2),c.TopUp_Payment - S.TopUp_Payment) AS TopUp_Payment,
Convert(Numeric(12,2),c.Total_Payment - S.Total_Payment) AS Total_Payment,
Convert(Numeric(12,2),c.Deemed_Export_Payment_For_Report - S.Deemed_Export_Payment_For_Report) AS Deemed_Export_Payment_For_Report 



from Claimed_Summary C inner join Statement_Summary S on c.FIT_PERIOD = S.FIT_PERIOD
Order by FIT_PERIOD
