/* 
Server:		UH-gendb-01
Database:	Microtricity2
Author:		Matthew Hollands
Date:		13/06/2018
*/


Select distinct 
D.CONTRACT_ID,
D.Fit_ID_Initial,
A.Change

from
(

select distinct
BP_ID,
CONTRACT_ID,
INSTALLATION_ID,
FiT_ID_Initial,
Qtr
from Annual_Lev_Claimed

Union

select distinct
BP_ID,
CONTRACT_ID,
INSTALLATION_ID,
FiT_ID_Initial,
Qtr = [Quarter]
from Annual_Lev_Statement

Union

select distinct
BP_ID,
CONTRACT_ID,
INSTALLATION_ID,
FiT_ID_Initial,
Qtr
from Annual_Lev_Paid) D left join Annual_Lev_Exceptions A on D.FiT_ID_Initial = A.Fit_ID_Initial

Where 
Total_Payment_Check is null
AND
Export_Status_Check is null
and 
Reads_Check is null