/*  
ANNUAL LEVELISATION QUERY

This is just a quick query to check Annual Lev data.  It shows the data, which has been queried by Microtricity already, for a FIT in the main tables and the clean tables.  
It also checks to see if the FIT is in the Exceptions list.

Use this to make a visual check of the data.
*/

DECLARE @FIT varchar(20);

SET @FIT = 'FIT00053488-1'

SELECT
Fit_Period,
CONTRACT_ID,
FiT_ID_Initial,
Account_Status,
Generation_Read_OLD,
Generation_Read_NEW,
Initial_System_Pct_Split
FROM Annual_Lev_Claimed_clean 
WHERE fit_id_initial = @FIT
ORDER BY fit_period,contract_ID

SELECT 
[Quarter],
CONTRACT_ID,
fit_id_initial,
Account_Status,
Generation_Read_OLD,
Generation_Read_NEW,
Initial_System_Pct_Split
FROM Annual_Lev_Statement_clean 
WHERE fit_id_initial = @FIT
ORDER BY [Quarter],CONTRACT_ID,Split

SELECT 
Fit_Period,
CONTRACT_ID,
FiT_ID_Initial,
Account_Status,
Generation_Read_OLD,
Generation_Read_NEW,
Generation_Read_OLD_2,
Generation_Read_NEW_2,
Initial_System_Pct_Split
FROM Annual_Lev_Paid_clean 
WHERE fit_id_initial = @FIT
ORDER BY Fit_Period,CONTRACT_ID

SELECT 
Fit_Period,
CONTRACT_ID,
FiT_ID_Initial,
Generation_Read_OLD,
Generation_Read_NEW,
Initial_System_Pct_Split
FROM Annual_Lev_Claimed 
WHERE fit_id_initial = @FIT
ORDER BY fit_period,contract_ID

SELECT 
[Quarter],
CONTRACT_ID,
fit_id_initial,
Generation_Read_OLD,
Generation_Read_NEW,
Initial_System_Pct_Split
FROM Annual_Lev_Statement 
WHERE fit_id_initial = @FIT
ORDER BY [Quarter],CONTRACT_ID,Split

SELECT 
Fit_Period,
CONTRACT_ID,
FiT_ID_Initial,
Generation_Read_OLD,
Generation_Read_NEW,
Generation_Read_OLD_2,
Generation_Read_NEW_2,
Initial_System_Pct_Split
FROM Annual_Lev_Paid 
WHERE fit_id_initial = @FIT
ORDER BY Fit_Period,CONTRACT_ID

SELECT 
* 
FROM Annual_Lev_Exceptions 
WHERE Fit_ID_Initial = @FIT