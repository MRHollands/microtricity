DECLARE @ConID AS int
set @conid = 7456

SELECT

*

FROM

(
SELECT
C.contract_id,
C.[Qtr] AS C_Qtr,
P.Qtr,
CL.TOTAL_Payment AS CL_TotPay,
S.Total_Payment AS S_TotPay,
P.Total_Payment AS P_TotPay,
CASE 
	WHEN S.Total_Payment-P.Total_payment NOT BETWEEN -0.1 AND 0.1 THEN 'EXCEPTION'
	WHEN CL.Total_Payment-P.Total_payment NOT BETWEEN -0.1 AND 0.1 THEN 'EXCEPTION'
	WHEN CL.Total_Payment-S.Total_payment NOT BETWEEN -0.1 AND 0.1 THEN 'EXCEPTION'
	ELSE NULL
		
END AS Total_Payment_Check, 

CASE
	WHEN CountES > 0 THEN 'Export Status Exception'
	ELSE NULL
END AS Export_Status_Check



From  (Select Distinct Contract_ID, 1 as Qtr FROM CONTRACT
Union Select Distinct Contract_ID, 2 as Qtr FROM CONTRACT
Union Select Distinct Contract_ID, 3 as Qtr FROM CONTRACT
Union Select Distinct Contract_ID, 4 as Qtr FROM CONTRACT) C 

Left Join --JOIN TO CLAIMED

(Select 
Contract_ID,
Qtr, 
Sum(Total_Payment) AS Total_Payment 
FROM Annual_Lev_Claimed
GROUP BY Contract_ID,Qtr) CL on C.CONTRACT_ID = CL.CONTRACT_ID AND C.[Qtr] = Convert(float,Right(CL.qtr,1))

left join --JOIN TO STATEMENT DATA

(Select
Contract_ID,
[Quarter],
SUM(Total_Payment) AS Total_Payment
FROM Annual_LevWithCalcs
Group By Contract_ID, [Quarter]) S On C.CONTRACT_ID = S.CONTRACT_ID AND c.Qtr = S.[Quarter]
 
Left Join --JOIN TO PAID

(Select 
Contract_ID,
Qtr, 
Sum(T_Adjusted_Total) AS Total_Payment 
FROM Annual_Lev_Paid
GROUP BY Contract_ID,Qtr) P on C.CONTRACT_ID = P.CONTRACT_ID AND C.[Qtr] = Convert(float,Right(P.qtr,1))

LEFT Join

(SELECT DISTINCT 
SD.CONTRACT_ID,
CountES
FROM   Annual_LevWithCalcs SD
INNER JOIN 
(SELECT DISTINCT 
CONTRACT_ID
,COUNT(DISTINCT [Export_Status]) CountES
FROM Annual_LevWithCalcs
GROUP BY CONTRACT_ID) ES_Check 

ON SD.CONTRACT_ID = ES_CHECK.CONTRACT_ID
WHERE  CountES >1) ES_Check on C.CONTRACT_ID = ES_Check.CONTRACT_ID) Data

WHERE 
(Total_Payment_Check is not null-- AND
OR 
Export_Status_Check is not null)
--AND 
--AND Contract_ID = @conid

Order by CONTRACT_ID,C_Qtr

--Select Contract_ID,Qtr,sum(Total_Payment) FROM [dbo].[Annual_Lev_Claimed] WHERE CONTRACT_ID = @conid GROUP BY CONTRACT_ID,Qtr
--Select Contract_ID,Quarter,sum(Total_Payment) FROM [dbo].[Annual_LevWithCalcs] WHERE CONTRACT_ID = @conid GROUP BY CONTRACT_ID,Quarter
--Select Contract_ID,Qtr,sum(T_Adjusted_Total) FROM [dbo].[Annual_Lev_Paid] WHERE CONTRACT_ID = @conid GROUP BY CONTRACT_ID,Qtr