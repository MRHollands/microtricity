Select
Contract_ID,
[Quarter],
Split,
export_status,
--[InitSysExpRate2],
Units_generated,

CASE 
WHEN Export_Status = 'No Export' THEN 0
WHEN Export_Status = 'Standard tariff' Then Round((Export_Read_NEW - Export_Read_OLD),1)
WHEN Generation_Type = 'Hydro' Then Round(Units_Generated * 0.75,1)
WHEN Export_Status Not IN ('No Export','Standard tariff') AND Generation_type <> 'Hydro' THEN Round(Units_Generated * 0.5,1)
ELSE NULL
END AS Units_Exported,

CASE 
WHEN Export_Status = 'Deemed export (<30kW)' AND Generation_Type = 'Hydro' THEN Round(Units_Generated * 0.75,1)
WHEN Export_Status = 'Deemed export (<30kW)' AND Generation_Type <> 'Hydro' THEN Round(Units_Generated * 0.5,1)
ELSE NULL
END AS Deemed_Units_Exported,

Round(Generation_Read_NEW - Generation_Read_OLD,1) AS Units_Generated,

CASE
WHEN Initial_System_Pct_Split IS NULL THEN 0
WHEN Initial_System_Pct_Split IS NOT NULL AND Extension_1_Pct_Split IS NULL AND Extension_2_Pct_Split IS NULL THEN ROUND((Units_Generated * (Initial_System_Pct_Split * (Current_Price_Initial/100))),2)
WHEN Initial_System_Pct_Split IS NOT NULL AND Extension_1_Pct_Split IS NOT NULL AND Extension_2_Pct_Split IS NULL THEN ROUND((Units_Generated * (Initial_System_Pct_Split * (Current_Price_Initial/100)) + (Extension_1_Pct_Split * (Current_Price_Initial/100))),2)
WHEN Initial_System_Pct_Split IS NOT NULL AND Extension_1_Pct_Split IS NOT NULL AND Extension_2_Pct_Split IS NOT NULL THEN ROUND(Units_Generated * (Initial_System_Pct_Split * (Current_Price_Initial/100)) + (Extension_1_Pct_Split * (Current_Price_Initial/100)) + (Extension_2_Pct_Split * (Current_Price_Initial/100)),2)
ELSE NULL
END AS Generation_Payment,

CASE
WHEN Export_Status = 'Standard tariff' AND Initial_System_Pct_Split is null THEN 0
WHEN Export_Status = 'Standard tariff' AND InitSysExpRate2 IS NOT NULL THEN ROUND((Units_Exported * (ExpPct1 * Current_Price_Export + ExpPct2 * Current_Price_InitExp2)/100),2)
WHEN Export_Status = 'Standard tariff' AND InitSysExpRate2 IS NULL AND Extension_1_Pct_Split IS NOT NULL AND Extension_2_Pct_Split IS NULL THEN ROUND((Units_exported * ((Units_Exported * Initial_System_Pct_Split * Current_Price_Export) + Extension_1_Pct_Split * Current_Price_Export)/100),2)
WHEN Export_Status = 'Standard tariff' AND InitSysExpRate2 IS NULL AND Extension_1_Pct_Split IS NOT NULL AND Extension_2_Pct_Split IS NOT NULL THEN ROUND((Units_exported * ((Units_Exported * Initial_System_Pct_Split * Current_Price_Export) + Extension_1_Pct_Split * Current_Price_Export + Extension_2_Pct_Split * Current_Price_Export)/100),2)
ELSE NULL
END AS ExpPay_if_Std,  -- This is currently not working.  Rework it in the style of ExpPay_if_Not_Std

CASE
	WHEN Export_Status = 'Deemed Export (<30kW)' AND [InitSysExpRate2] Is Not Null THEN	ROUND((([Units_Exported]*[ExpPct1])*([Current_Price_Export]/100))
																						+(([Units_Exported]*[ExpPct2])*([Current_Price_InitExp2]/100)),2)
	WHEN Export_Status = 'Deemed Export (<30kW)' THEN	ROUND(([Units_Exported] * [Initial_System_Pct_Split]) * ([Current_Price_Export]/100) + 
														COALESCE(([Units_Exported] * [Extension_1_Pct_Split]) * ([Current_Price_Export1]/100),0) + 
														COALESCE(([Units_Exported] * [Extension_2_Pct_Split]) * ([Current_Price_Export2]/100),0),2)
	ELSE 0
END AS ExpPay_if_Not_Std,



Round((ExpPay_if_std + ExpPay_not_Std),2) AS Export_Payment,

TopUp_Payment AS TopUp_Payment, -- This should be calulated but no customer in 2016=17 fit year had a top up payment.  Will only need to be calculated if a customer ever needs it

ROUND ((Generation_Payment + Export_Payment + TopUp_Payment),2) AS Total_Payment,

CASE 
WHEN Export_Status = 'Deemed export (<30kW)' THEN ROUND(Export_Payment,2)
ELSE NULL
END AS Deemed_Export_Payment_For_Report


From Annual_Lev_Statement

