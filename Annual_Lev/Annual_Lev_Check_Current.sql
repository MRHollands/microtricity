SELECT        
CONVERT(Int, CONTRACT_ID) AS Contract_ID, 
FiT_ID_Initial, LTRIM(ISNULL(Company, '') + ' ' + ISNULL(Title, '') + ' ' + ISNULL(First_Name, '') + ' ' + ISNULL(Last_Name, '')) AS Full_Name, 
C_Qtr, 
Qtr, 
CL_TotPay, 
S_TotPay, 
P_TotPay, 
Total_Payment_Check, 
Export_Status_Check, 
Reads_Check, 
Change, 
Claimed_AFR AS Audit_FIT_Year

FROM            
(SELECT        
C.CONTRACT_ID, 
dbo.INSTALLATION.FiT_ID_Initial, 
dbo.BP.Company, 
dbo.BP.Title, 
dbo.BP.First_Name, 
dbo.BP.Last_Name, 
C.Qtr AS C_Qtr, 
P.Qtr, 
CL.Total_Payment AS CL_TotPay, 
S.Total_Payment AS S_TotPay, 
P.Total_Payment AS P_TotPay, 

CASE 
	WHEN COALESCE (S.Total_Payment, 0) - COALESCE (P.Total_payment, 0) NOT BETWEEN - 0.1 AND 0.1 THEN 'Total Diff' 
	WHEN COALESCE (CL.Total_Payment, 0) - COALESCE (P.Total_payment, 0) NOT BETWEEN - 0.1 AND 0.1 THEN 'Total Diff' 
	WHEN COALESCE (CL.Total_Payment, 0) - COALESCE (S.Total_payment, 0) NOT BETWEEN - 0.1 AND 0.1 THEN 'Total Diff' 
	ELSE NULL 
END AS Total_Payment_Check, 
CASE 
	WHEN CountES > 0 THEN 'Export Status Exception' 
	ELSE NULL 
END AS Export_Status_Check, 
CASE 
	WHEN generation_read_check = 'Exception' THEN 'Generation Read Error' 
	WHEN Export_Read_Check = 'Exception' THEN 'Export Read Error' 
	ELSE NULL 
END AS Reads_Check, 
S.Total_Payment - S.Original_Total_Payment AS Change, 
CL.Claimed_AFR, 
S.Statement_AFR, 
P.Paid_AFR

FROM            
(SELECT DISTINCT CONTRACT_ID, BP_ID_link, INSTALLATION_ID_link, 1 AS Qtr
	FROM            dbo.CONTRACT
UNION
SELECT DISTINCT CONTRACT_ID, BP_ID_link, INSTALLATION_ID_link, 4 AS Qtr
	FROM            dbo.CONTRACT AS CONTRACT_1
UNION
SELECT DISTINCT CONTRACT_ID, BP_ID_link, INSTALLATION_ID_link, 3 AS Qtr
	FROM            dbo.CONTRACT AS CONTRACT_2
UNION
SELECT DISTINCT CONTRACT_ID, BP_ID_link, INSTALLATION_ID_link, 2 AS Qtr
	FROM            dbo.CONTRACT AS CONTRACT_3) AS C LEFT OUTER JOIN

(SELECT        CONTRACT_ID, Qtr, MAX(Audit_Fit_Year) AS Claimed_AFR, SUM(Total_Payment) AS Total_Payment FROM dbo.Annual_Lev_Claimed
GROUP BY CONTRACT_ID, Qtr) AS CL ON C.CONTRACT_ID = CL.CONTRACT_ID AND C.Qtr = CONVERT(float, RIGHT(CL.Qtr, 1)) LEFT OUTER JOIN
(SELECT        CONTRACT_ID, MAX(Audit_Fit_Year) AS Statement_AFR, Quarter, SUM(Total_Payment) AS Total_Payment, SUM(Original_Total_Payment) AS Original_Total_Payment
FROM            dbo.Annual_Lev_Statement
GROUP BY CONTRACT_ID, Quarter) AS S ON C.CONTRACT_ID = S.CONTRACT_ID AND C.Qtr = S.Quarter LEFT OUTER JOIN
(SELECT        CONTRACT_ID, Qtr, MAX(Audit_Fit_Year) AS Paid_AFR, SUM(T_Adjusted_Total) AS Total_Payment
FROM            dbo.Annual_Lev_Paid
GROUP BY CONTRACT_ID, Qtr) AS P ON C.CONTRACT_ID = P.CONTRACT_ID AND C.Qtr = CONVERT(float, RIGHT(P.Qtr, 1)) LEFT OUTER JOIN
(SELECT DISTINCT SD.CONTRACT_ID, ES_Check.CountES
FROM            dbo.Annual_Lev_Statement AS SD INNER JOIN
(SELECT DISTINCT CONTRACT_ID, COUNT(DISTINCT Export_Status) AS CountES
FROM            dbo.Annual_Lev_Statement AS Annual_Lev_Statement_1
GROUP BY CONTRACT_ID) AS ES_Check ON SD.CONTRACT_ID = ES_Check.CONTRACT_ID
WHERE        (ES_Check.CountES > 1)) AS ES_Check ON C.CONTRACT_ID = ES_Check.CONTRACT_ID LEFT OUTER JOIN
dbo.INSTALLATION ON C.INSTALLATION_ID_link = dbo.INSTALLATION.INSTALLATION_ID LEFT OUTER JOIN
dbo.BP ON C.BP_ID_link = dbo.BP.BP_ID LEFT OUTER JOIN
dbo.Annual_Lev_Read_Checks_vw AS R ON C.CONTRACT_ID = R.CONTRACT_ID AND C.Qtr = R.Quarter) AS Data
WHERE        (Total_Payment_Check IS NOT NULL) OR
(Export_Status_Check IS NOT NULL) OR
(Reads_Check IS NOT NULL)