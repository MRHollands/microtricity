WITH Split_1 AS(
SELECT 
CONTRACT_ID,
Qtr,
[Export_Status],[ExpPct1],[Initial_System_Export_Rate],[Current_Price_Export],[ExpPct2],[InitSysExpRate2],[Current_Price_InitExp2],[Extension_1_Export_Rate],[Current_Price_Export1],[Extension_2_Export_Rate],[Current_Price_Export2],[PaidToREAD_ID],[LatestPaidToRead#Meter_Read_Reason],[Export_Read_OLD],[Generation_Read_OLD],[Meter_Read_Date_OLD],[LatestREAD_ID],
[ReadForQuarter#Meter_Read_Reason],[Export_Read_NEW],[Generation_Read_NEW],[Meter_Read_Date_NEW],[Initial_System_Tariff_Rate],[Current_Price_Initial],[Extension_1_Tariff_Rate],[Current_Price_Ext1],[Extension_2_Tariff_Rate],[Current_Price_Ext2],[Units_Exported],[Deemed_Units_Exported],[Units_Generated],[Generation_Payment],[ExpPay_if_Std],[ExpPay_Not_Std],[Export_Payment],[TopUp_Payment],[Total_Payment],[Deemed_Export_Payment__For_Report]
FROM Annual_Lev_Statement
UNION 
Select
CONTRACT_ID,
Qtr,
[Export_Status_2],[ExpPct1_2],[Initial_System_Export_Rate_2],[Current_Price_Export_2],[ExpPct2_2],[InitSysExpRate2_2],[Current_Price_InitExp2_2],[Extension_1_Export_Rate_2],[Current_Price_Export1_2],[Extension_2_Export_Rate_2],[Current_Price_Export2_2],[PaidToREAD_ID_2],[LatestPaidToRead#Meter_Read_Reason_2],[Export_Read_OLD_2],[Generation_Read_OLD_2],[Meter_Read_Date_OLD_2],[LatestREAD_ID_2],
[ReadForQuarter#Meter_Read_Reason_2],[Export_Read_NEW_2],[Generation_Read_NEW_2],[Meter_Read_Date_NEW_2],[Initial_System_Tariff_Rate_2],[Current_Price_Initial_2],[Extension_1_Tariff_Rate_2],[Current_Price_Ext1_2],[Extension_2_Tariff_Rate_2],[Current_Price_Ext2_2],[Units_Exported_2],[Deemed_Units_Exported_2],[Units_Generated_2],[Generation_Payment_2],[ExpPay_if_Std_2],[ExpPay_Not_Std_2],[Export_Payment_2],[TopUp_Payment_2],[Total_Payment_2],[Deemed_Export_Payment__For_Report_2]
FROM Annual_Lev_Statement
UNION 
Select 
CONTRACT_ID,
Qtr,
[Export_Status_3],[ExpPct1_3],[Initial_System_Export_Rate_3],[Current_Price_Export_3],[ExpPct2_3],[InitSysExpRate2_3],[Current_Price_InitExp2_3],[Extension_1_Export_Rate_3],[Current_Price_Export1_3],[Extension_2_Export_Rate_3],[Current_Price_Export2_3],[PaidToREAD_ID_3],[LatestPaidToRead#Meter_Read_Reason_3],[Export_Read_OLD_3],[Generation_Read_OLD_3],[Meter_Read_Date_OLD_3],[LatestREAD_ID_3],
[ReadForQuarter#Meter_Read_Reason_3],[Export_Read_NEW_3],[Generation_Read_NEW_3],[Meter_Read_Date_NEW_3],[Initial_System_Tariff_Rate_3],[Current_Price_Initial_3],[Extension_1_Tariff_Rate_3],[Current_Price_Ext1_3],[Extension_2_Tariff_Rate_3],[Current_Price_Ext2_3],[Units_Exported_3],[Deemed_Units_Exported_3],[Units_Generated_3],[Generation_Payment_3],[ExpPay_if_Std_3],[ExpPay_Not_Std_3],[Export_Payment_3],[TopUp_Payment_3],[Total_Payment_3],[Deemed_Export_Payment__For_Report_3]
FROM Annual_Lev_Statement
UNION 
SELECT
CONTRACT_ID,
Qtr,
[Export_Status_4],[ExpPct1_4],[Initial_System_Export_Rate_4],[Current_Price_Export_4],[ExpPct2_4],[InitSysExpRate2_4],[Current_Price_InitExp2_4],[Extension_1_Export_Rate_4],[Current_Price_Export1_4],[Extension_2_Export_Rate_4],[Current_Price_Export2_4],[PaidToREAD_ID_4],[LatestPaidToRead#Meter_Read_Reason_4],[Export_Read_OLD_4],[Generation_Read_OLD_4],[Meter_Read_Date_OLD_4],[LatestREAD_ID_4],
[ReadForQuarter#Meter_Read_Reason_4],[Export_Read_NEW_4],[Generation_Read_NEW_4],[Meter_Read_Date_NEW_4],[Initial_System_Tariff_Rate_4],[Current_Price_Initial_4],[Extension_1_Tariff_Rate_4],[Current_Price_Ext1_4],[Extension_2_Tariff_Rate_4],[Current_Price_Ext2_4],[Units_Exported_4],[Deemed_Units_Exported_4],[Units_Generated_4],[Generation_Payment_4],[ExpPay_if_Std_4],[ExpPay_Not_Std_4],[Export_Payment_4],[TopUp_Payment_4],[Total_Payment_4],[Deemed_Export_Payment__For_Report_4]
FROM Annual_Lev_Statement
UNION 
SELECT
CONTRACT_ID,
Qtr,
[Export_Status_5],[ExpPct1_5],[Initial_System_Export_Rate_5],[Current_Price_Export_5],[ExpPct2_5],[InitSysExpRate2_5],[Current_Price_InitExp2_5],[Extension_1_Export_Rate_5],[Current_Price_Export1_5],[Extension_2_Export_Rate_5],[Current_Price_Export2_5],[PaidToREAD_ID_5],[LatestPaidToRead#Meter_Read_Reason_5],[Export_Read_OLD_5],[Generation_Read_OLD_5],[Meter_Read_Date_OLD_5],[LatestREAD_ID_5],
[ReadForQuarter#Meter_Read_Reason_5],[Export_Read_NEW_5],[Generation_Read_NEW_5],[Meter_Read_Date_NEW_5],[Initial_System_Tariff_Rate_5],[Current_Price_Initial_5],[Extension_1_Tariff_Rate_5],[Current_Price_Ext1_5],[Extension_2_Tariff_Rate_5],[Current_Price_Ext2_5],[Units_Exported_5],[Deemed_Units_Exported_5],[Units_Generated_5],[Generation_Payment_5],[ExpPay_if_Std_5],[ExpPay_Not_Std_5],[Export_Payment_5],[TopUp_Payment_5],[Total_Payment_5],[Deemed_Export_Payment__For_Report_5]
From Annual_Lev_Statement
)

select 
A.CONTRACT_ID,
A.Qtr
	  ,[Initial_System_Pct_Split]
,[Extension_1_Pct_Split]
,[Extension_2_Pct_Split]
	  ,SPlit_1.*
From Annual_Lev_Statement A  Inner join Split_1 on A.CONTRACT_ID = Split_1.CONTRACT_ID and A.Qtr = Split_1.qtr
Where A.CONTRACT_ID = 7453
order by A.contract_id,A.qtr
