With CTE AS 
(select
Rank() Over (order by Contract_ID,[Quarter],Split) AS Rank_Split,
CONTRACT_ID,
[Quarter],
Split,
Meter_Read_Date_OLD,
Meter_Read_Date_NEW,
LatestPaidToRead_Meter_Read_Reason,
ReadForQuarter_Meter_Read_Reason,
Generation_Read_OLD,
Generation_Read_NEW,
Export_Read_OLD,
Export_Read_NEW
from Annual_Lev_Statement
)


Select * from

(
Select 
C_A.CONTRACT_ID,
C_A.[Quarter],
C_A.LatestPaidToRead_Meter_Read_Reason C_A_LatestPaidToRead_Meter_Read_Reason,
C_A.ReadForQuarter_Meter_Read_Reason C_A_ReadForQuarter_Meter_Read_Reason,
C_B.LatestPaidToRead_Meter_Read_Reason C_B_LatestPaidToRead_Meter_Read_Reason,
C_B.ReadForQuarter_Meter_Read_Reason C_B_ReadForQuarter_Meter_Read_Reason,

C_A.Meter_Read_Date_OLD C_A_Meter_Read_Date_OLD,
C_A.Meter_Read_Date_NEW C_A_Meter_Read_Date_NEW,
C_B.Meter_Read_Date_OLD C_B_Meter_Read_Date_OLD,
C_B.Meter_Read_Date_NEW C_B_Meter_Read_Date_NEW,

C_A.Generation_Read_OLD C_A_Generation_Read_OLD,
C_A.Generation_Read_NEW C_A_Generation_Read_NEW,
C_B.Generation_Read_OLD C_B_Generation_Read_OLD,
C_B.Generation_Read_NEW C_B_Generation_Read_NEW,

C_A.Export_Read_OLD C_A_Export_Read_OLD,
C_A.Export_Read_NEW C_A_Export_Read_NEW,
C_B.Export_Read_OLD C_B_Export_Read_OLD,
C_B.Export_Read_NEW C_B_Export_Read_NEW,

CASE 
WHEN C_A.Generation_Read_NEW - C_A.Generation_Read_OLD < 0 THEN 'Exception'
WHEN C_B.LatestPaidToRead_Meter_Read_Reason <> 'Initial (MX)' AND  C_A.Generation_Read_NEW <> C_B.Generation_Read_OLD THEN 'Exception'
WHEN C_B.Generation_Read_NEW - C_B.Generation_Read_OLD < 0 THEN 'Exception'
ELSE NULL
END AS Generation_Read_Check,

CASE 
WHEN C_A.Export_Read_NEW - C_A.Export_Read_OLD < 0 THEN 'Exception'
WHEN C_B.LatestPaidToRead_Meter_Read_Reason <> 'Initial (MX)' AND C_A.Export_Read_NEW <> C_B.Export_Read_OLD THEN 'Exception'
WHEN C_B.Export_Read_NEW - C_B.Export_Read_OLD < 0 THEN 'Exception'
ELSE NULL
END AS Export_Read_Check

From CTE C_A inner Join CTE C_B on C_A.CONTRACT_ID = C_B.CONTRACT_ID AND C_A.Rank_Split = C_B.Rank_Split -1) Data






