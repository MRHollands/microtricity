SELECT
CONTRACT_ID,
Qtr,
SUM(Total_Units_Exported) AS Total_Units_Exported,
SUM(Total_Deemed_Units_Exported) AS Total_Deemed_Units_Exported,
SUM(Total_Units_Generated) AS Total_Units_Generated,
SUM(Total_Generation_Payment) AS Total_Generation_Payment,
SUM(Total_ExpPay_if_Std) AS Total_ExpPay_if_Std

FROM
(
SELECT        
CONTRACT_ID, 
RIGHT(Fit_Period, 2) AS Qtr, 
ISNULL(Units_Exported,0) + ISNULL(Units_Exported_2,0) + ISNULL(Units_Exported_3,0) + ISNULL(Units_Exported_4,0) + ISNULL(Units_Exported_5,0) AS Total_Units_Exported,
ISNULL(Deemed_Units_Exported,0) + ISNULL(Deemed_Units_Exported_2,0) + ISNULL(Deemed_Units_Exported_3,0) + ISNULL(Deemed_Units_Exported_4,0) + ISNULL(Deemed_Units_Exported_5,0) AS Total_Deemed_Units_Exported,
ISNULL(Units_Generated,0) + ISNULL(Units_Generated_2,0) + ISNULL(Units_Generated_3,0) + ISNULL(Units_Generated_4,0) + ISNULL(Units_Generated_5,0) AS Total_Units_Generated,
ISNULL(Generation_Payment,0) + ISNULL(Generation_Payment_2,0) + ISNULL(Generation_Payment_3,0) + ISNULL(Generation_Payment_4,0) + ISNULL(Generation_Payment_5,0) AS Total_Generation_Payment,

ISNULL(ExpPay_if_Std,0) + ISNULL(ExpPay_if_Std_2,0) + ISNULL(ExpPay_if_Std_3,0) + ISNULL(ExpPay_if_Std_4,0) + ISNULL(ExpPay_if_Std_5,0) AS Total_ExpPay_if_Std,

(ExpPay_Not_Std) AS T_ExpPay_Not_Std, 
(Export_Payment) AS T_Export_Payment, 
(TopUp_Payment) AS T_Topup_Payment, 
(Total_Payment) AS T_Total_Payment, 
(Deemed_Export_Payment__For_Report) AS T_Deemed_Export_Payment_For_Report

FROM 
dbo.Annual_Lev_Paid
Where CONTRACT_ID = 7551
) DATA

Where CONTRACT_ID = 7551
GROUP BY 
CONTRACT_ID, 
Qtr
