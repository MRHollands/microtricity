SELECT
S.CONTRACT_ID,
S.FiT_ID_Initial,
Right(S.fit_period,2) AS Qtr,
L.T_Export_Payment AS Claimed_Export,
S.T_Export_Payment AS Statement_Export,
P.T_Export_Payment AS Paid_Export,
L.T_Generation_Payment AS Claimed_Generation,
S.T_Generation_Payment AS Statement_Generation,
P.T_Generation_Payment AS Paid_Generation,
L.T_Deemed_Export_Payment_For_Report AS Claimed__Deemed_Export,
S.T_Deemed_Export_Payment__For_Report AS Statement_Deemed_Export,
P.T_Deemed_Export_Payment__For_Report AS Paid_Deemed_Export,
P.T_original_payment_total,
CASE 
	WHEN L.T_Export_Payment <> S.T_Export_Payment THEN 'EXCEPTION'
	WHEN S.T_Export_Payment <> P.T_Export_Payment THEN 'EXCEPTION'
	ELSE NULL
END AS Export_Check,
CASE 
	WHEN L.T_Generation_Payment <> S.T_Generation_Payment THEN 'EXCEPTION'
	WHEN S.T_Generation_Payment <> P.T_Generation_Payment THEN 'EXCEPTION'
	ELSE NULL
END AS Generation_Check,
CASE 
	WHEN L.T_Deemed_Export_Payment_For_Report <> S.T_Deemed_Export_Payment__For_Report THEN 'EXCEPTION'
	WHEN S.T_Deemed_Export_Payment__For_Report <> P.T_Deemed_Export_Payment__For_Report THEN 'EXCEPTION'
	ELSE NULL
END AS Deemed_Export_Check



FROM
Annual_Lev_Statement S
INNER JOIN Annual_Lev_LevWithCalcs_SUMMARY L ON S.CONTRACT_ID = L.CONTRACT_ID AND Right(S.Fit_Period,2) = L.Qtr
INNER JOIN Annual_Lev_Paid P ON S.CONTRACT_ID = P.CONTRACT_ID AND S.Fit_Period = P.Fit_Period
ORDER BY S.Fit_Period
