/*


1) EAC is currently a static table using query saved - I:\Data Support\General\SQLS_Queries\EACs.sql.  This will need updating in to the table listed below prior to running.

2) Change @ReportStart and @ReportEnd dates before running.

3) Once files have been saved, open them and remove any lines before the header row

*/


IF OBJECT_ID('tempdb..#MaxBilled') IS NOT NULL     BEGIN DROP TABLE #MaxBilled   END
IF OBJECT_ID('tempdb..#AFMS_READS') IS NOT NULL     BEGIN DROP TABLE #AFMS_READS   END
IF OBJECT_ID('tempdb..#HH_Cons') IS NOT NULL     BEGIN DROP TABLE #HH_Cons   END
IF OBJECT_ID('tempdb..#TRAS') IS NOT NULL     BEGIN DROP TABLE #TRAS   END

DECLARE @ReportStart	Datetime
DECLARE @ReportEnd		Datetime
SET		@ReportStart = '01May2018'
SET		@ReportEnd = '31May2018'

Select	*
INTO	#AFMS_READS
FROM 
(Select	J0003 AS POD,
		max(MRR.J0016) AS MaxReadDate
FROM	AFMSLocal.dbo.mpan M
			INNER JOIN AFMSLocal.dbo.meter ME ON M.UNIQ_ID = ME.MPAN_LNK
			INNER JOIN AFMSLocal.dbo.meter_register MR ON ME.METER_PK = MR.METER_FK
			INNER JOIN AFMSLocal.dbo.meter_reg_reading MRR ON MR.METER_REG_PK = MRR.METER_REG_FK AND MRR.FLOW_RECEIVED = 'D0010' AND J0171 IN ('R','S','Q','I','F','C')
GROUP BY J0003

UNION SELECT	K0533 AS POD,
		Max(K0013) AS MaxReadDate
FROM	AFMSLocal.gas.continuous_MPRN M
			INNER JOIN AFMSLocal.gas.combined_meter ME ON M.MPRN_PK = ME.MPRN_FK
			INNER JOIN AFMSLocal.gas.combined_meter_read MRR ON ME.Meter_PK = MRR.Meter_FK AND FLOW_RECEIVED = 'URS_U10' AND MRR.K0980 IN ('E','M') AND MRR.Read_status = 'ACCEPTED'
GROUP BY K0533) DATA;

Select	vertrag AS [Contract]
		,max(endabrpe) AS MaxBilled
INTO	#MaxBilled
FROM 	[LH-GENDB-01].BI_RAW.dbo.erch	B
where	STORNODAT ='1980-01-01 00:00:00.000' and BELEGART in ('cb','fb')
group by vertrag;

--DECLARE @ReportStartHH	Datetime
--DECLARE @ReportEndHH		Datetime
--SET		@ReportStartHH = '01Oct2017'
--SET		@ReportEndHH = '31Oct2017'

SELECT	[MPAN]
		,sum([reading]) AS HH_Cons
INTO	#HH_Cons
FROM [vms-pnd-001].[D0275].[dbo].[Readings]
WHERE measure = 'AI' and settlementDate between @ReportStart and @ReportEnd
GROUP BY MPAN;


Select	CASE
			WHEN C.Division = 1 AND IsNull([j0117],'31Dec9999') >=@ReportStart THEN '001'
			WHEN C.Division = 2 AND IsNull([K0882],'31Dec9999') >=@ReportStart THEN '001'
			ELSE '002' END AS [Record Type], 
		C.MPAN_MPRN, 
		'N' AS [Multiple MPAN], 
		CASE
			WHEN C.Division = 1 THEN FORMAT(M.J0049,'yyyyMMdd')
			WHEN C.Division = 2 THEN FORMAT(GM.K0116,'yyyyMMdd')
			ELSE null END  AS [Supplier Start date (SSD)], 
		CASE
			WHEN Division = 1 THEN M.J1036 
			WHEN Division = 2 THEN GM.K0061 
			END AS [Supply Address line 1], 
		CASE
			WHEN Division = 1 THEN M.J1037 
			WHEN Division = 2 THEN GM.K0886
			END AS [Supply Address line 2], 
		CASE
			WHEN Division = 1 THEN M.J1038
			WHEN Division = 2 THEN GM.K0062
			END AS [Supply Address line 3], 
		CASE
			WHEN Division = 1 THEN M.J1039 
			WHEN Division = 2 THEN GM.K0733
			END AS [Supply Address line 4], 
		CASE
			WHEN Division = 1 THEN M.J1040
			WHEN Division = 2 THEN GM.K0217
			END AS [Supply Address line 5], 
		CASE
			WHEN Division = 1 THEN M.J1041 
			WHEN Division = 2 THEN GM.K0703
			END AS [Supply Address line 6], 
		CASE
			WHEN Division = 1 THEN M.J1042 
			WHEN Division = 2 THEN GM.K0150
			END AS [Supply Address line 7], 
		CASE
			WHEN Division = 1 THEN M.J1043 
			WHEN Division = 2 THEN NULL 
			END AS [Supply Address line 8], 
		CASE
			WHEN Division = 1 THEN M.J1044 
			WHEN Division = 2 THEN NULL
			END AS [Supply Address line 9], 
		CASE
			WHEN Division = 1 THEN M.j0263 
			WHEN Division = 2 THEN COALESCE(GM.K0700+' ','')+COALESCE(GM.K0699,'')
			END AS [Supply Postcode], 
		C.Contract_Account AS [Supplier's Customer Number], 
		RTRIM(COALESCE([Name1]+' ','') +COALESCE([Name2]+' ','') +COALESCE([Title]+' ','') +COALESCE([First_Name]+' ','') +COALESCE([Last_Name],'')) AS [Customer Name], 
		RTrim(COALESCE([PO_BOX]+' ','') +COALESCE([Room_No],'')) AS [Customer Address line 1], 
		B.Floor AS [Customer Address line 2], 
		B.Street3 AS [Customer Address line 3], 
		RTrim(COALESCE([House_Number]+' ','') +COALESCE([Street],'')) AS [Customer Address line 4], 
		RTrim(COALESCE([Street2]+' ','') +COALESCE([Supplement],'')) AS [Customer Address line 5], 
		B.Street4 AS [Customer Address line 6], 
		B.Street5 AS [Customer Address line 7], 
		B.City AS [Customer Address line 8], 
		B.Description AS [Customer Address line 9], 
		B.PostCode AS [Customer Postcode], 
		C.Contract_Account AS [Supplier's Account Number], 
		RTRIM(COALESCE([Title]+' ','') +COALESCE([First_Name]+' ','') +COALESCE([Last_Name],'')) AS [Account holders name 1], 
		RTrim(COALESCE([Name1]+' ','') +COALESCE([Name2],'')) AS [Billing Company Name], 
		Null AS ["Trading As" Account Company Name], 
		RTrim(COALESCE([PO_BOX]+' ','') +COALESCE([Room_No],'')) AS [Billing Address line 1], 
		B.Floor AS [Billing Address line 2], 
		B.Street3 AS [Billing Address line 3], 
		RTrim(COALESCE([House_Number]+' ','') +COALESCE([Street],'')) AS [Billing Address line 4], 
		RTrim(COALESCE([Street2]+' ','') +COALESCE([Supplement],'')) AS [Billing Address line 5], 
		B.Street4 AS [Billing Address line 6], 
		B.Street5 AS [Billing Address line 7], 
		B.City AS [Billing Address line 8], 
		B.Description AS [Billing Address line 9], 
		B.PostCode AS [Billing Postcode],
		FORMAT(C.MI_Date,'yyyyMMdd') AS [Account start date], 
		FORMAT(MB.[MaxBilled],'yyyyMMdd') AS [Annual Consumption bill to date], 
		CASE
			WHEN E.[EAC] IS NOT NULL THEN E.EAC
			WHEN C.Division =1 AND J0049 < Dateadd(dd,-31,@ReportEnd) THEN -99998
			WHEN C.Division =2 AND K0116 < Dateadd(dd,-31,@ReportEnd) THEN -99998
			ELSE -99999
			END AS [Annual Consumption], 
		CASE
			WHEN J1684 = 'A' AND HH.HH_Cons IS NULL AND J0049 < Dateadd(dd,-31,@ReportEnd) THEN -99998
			WHEN J1684 = 'A' AND HH.HH_Cons IS NULL THEN -99999
			WHEN J1684 = 'A' THEN HH.HH_Cons
			ELSE NULL 
			END AS [HH - Monthly Consumption T], 
		Null AS [HH - Monthly Consumption T1], 
		Null AS [HH - Monthly Consumption T2], 
		Null AS [HH - Monthly Consumption T3], 
		Null AS [HH - Monthly Consumption T4], 
		Null AS [HH - Monthly Consumption T5], 
		Null AS [HH - Monthly Consumption T6], 
		Null AS [HH - Monthly Consumption T7], 
		Null AS [HH - Monthly Consumption T8],
		Null AS [HH - Monthly Consumption T9], 
		Null AS [HH - Monthly Consumption T10],
		Null AS [HH - Monthly Consumption T11], 
		FORMAT(AR.MaxReadDate,'yyyyMMdd') AS [Last Meter Read date], 
		FORMAT(CASE
			WHEN AR.MaxReadDate IS NOT NULL THEN AR.MaxReadDate
			WHEN Division =1 THEN ME.J0848 
			WHEN Division =2 THEN GME.k0518
			ELSE NULL
			END,'yyyyMMdd') AS [Last Meter inspection date], 
		FORMAT(CASE
			WHEN Division =1 THEN ME.J0848 
			WHEN Division =2 THEN GME.k0518 
			ELSE NULL
			END,'yyyyMMdd') AS [Meter Installation Date], 
		CASE
			WHEN SUBSTRING([MRU],2,1) = 'D' THEN 'M'
			WHEN SUBSTRING([MRU],2,1) = 'A' THEN 'M'
			WHEN SUBSTRING([MRU],2,1) = 'U' THEN 'M'
			WHEN SUBSTRING([MRU],2,1) = 'P' THEN 'S'
			ELSE SUBSTRING([MRU],2,1)
			END AS [Billing Frequency], 
		CASE
			WHEN C.Payment_Method = 'C' THEN 'T'
			WHEN C.Payment_Method = 'D' THEN 'F'
			WHEN C.Payment_Method = 'K' THEN 'P'
			WHEN C.Payment_Method = 'V' THEN 'V'
			ELSE 'C' 
			END AS [Normal Payment method], 
		FORMAT(C.MI_Date,'yyyyMMdd') AS MI_Date, FORMAT(C.MO_Date,'yyyyMMdd') AS MO_Date, 
		CASE
			WHEN substring([Rate_Category],2,1)='D' THEN 'Dom'
			WHEN substring([Rate_Category],2,1)='B' THEN 'Bus'
			ELSE 'Unknown' END AS Type, 
		CASE
			WHEN C.Division = 1 THEN M.j0080
			WHEN C.Division = 2 THEN GME.K0546
			ELSE NULL
			END AS Meter_Status,
		CASE
			WHEN C.Division = 1 AND J1684 = 'A' THEN 'Elec HH'
			WHEN C.Division = 1 AND ISNULL(J1684,'') <> 'A' THEN 'Elec NHH'
			WHEN C.Division = 2 THEN 'Gas'
			ELSE 'Unknown'
			END AS Fuel
INTO	#TRAS
FROM	DatsUp.dbo.CAMPAN_Data C
			INNER JOIN DatsUp.dbo.BillingAddress B ON C.Contract_Account = B.Contract_Number
			LEFT JOIN AFMSLocal.dbo.mpan M ON C.MPAN_MPRN = M.J0003
			LEFT JOIN AFMSLocal.dbo.meter ME ON M.UNIQ_ID = ME.MPAN_LNK
			LEFT JOIN AFMSLocal.Gas.continuous_mprn GM ON C.MPAN_MPRN = GM.K0533
			LEFT JOIN AFMSLocal.gas.combined_meter GME ON GM.MPRN_PK = GME.MPRN_FK
			LEFT JOIN DatSup.dbo.SAP_EAC E ON C.Installation = E.Installation
			LEFT JOIN #MaxBilled MB ON C.[Contract] = MB.[Contract]
			LEFT JOIN #AFMS_READS AR ON C.MPAN_MPRN = AR.POD
			LEFT JOIN #HH_Cons HH ON C.MPAN_MPRN = HH.MPAN
WHERE	C.MI_Date <= @ReportEnd AND C.MO_Date >= @ReportStart
		AND
		((C.Division = 1 AND M.J0049 <= @ReportEnd AND ISNULL(M.J0117,'31Dec9999') >=@ReportStart AND ME.J0848 <= @ReportEnd AND ISNULL(ME.J1269,'31Dec9999')>=@ReportStart)
		OR
		(C.Division = 2 AND GM.K0116 <= @ReportEnd AND ISNULL(GM.K0882,'31Dec9999') >=@ReportStart AND GME.K0518 <= @ReportEnd AND ISNULL(GME.K0543,'31Dec9999')>=@ReportStart));

--Res_Consumption File
WITH Res_Con_Header AS 
(SELECT Val = '000TRAS RES CONSUMPTIONXYZ        '+ FORMAT(getdate(),'yyyyMMdd') +'FMT001'+ replicate(' ',1967)),

Res_Con_Query AS
(
Select  Val =
Concat(
		left([Record Type]+replicate(' ',3-len([Record Type])),3)
		,coalesce(left(#TRAS.MPAN_MPRN+replicate(' ',20-len(#TRAS.MPAN_MPRN)),20),replicate(' ',20))
		,coalesce(left([Multiple MPAN]+replicate(' ',1-len([Multiple MPAN])),1),replicate(' ',1))
		,coalesce(left([Supplier Start date (SSD)]+replicate(' ',8-len([Supplier Start date (SSD)])),8),replicate(' ',8))
		,coalesce(left([Supply Address line 1]+replicate(' ',40-len([Supply Address line 1])),40),replicate(' ',40))
		,coalesce(left([Supply Address line 2]+replicate(' ',40-len([Supply Address line 2])),40),replicate(' ',40))
		,coalesce(left([Supply Address line 3]+replicate(' ',40-len([Supply Address line 3])),40),replicate(' ',40))
		,coalesce(left([Supply Address line 4]+replicate(' ',40-len([Supply Address line 4])),40),replicate(' ',40))
		,coalesce(left([Supply Address line 5]+replicate(' ',40-len([Supply Address line 5])),40),replicate(' ',40))
		,coalesce(left([Supply Address line 6]+replicate(' ',40-len([Supply Address line 6])),40),replicate(' ',40))
		,coalesce(left([Supply Address line 7]+replicate(' ',40-len([Supply Address line 7])),40),replicate(' ',40))
		,coalesce(left([Supply Address line 8]+replicate(' ',40-len([Supply Address line 8])),40),replicate(' ',40))
		,coalesce(left([Supply Address line 9]+replicate(' ',40-len([Supply Address line 9])),40),replicate(' ',40))
		,coalesce(left([Supply Postcode]+replicate(' ',8-len([Supply Postcode])),8),replicate(' ',8))
		,coalesce(left(CAST([Supplier's Customer Number] AS Varchar)+replicate(' ',40-len(CAST([Supplier's Customer Number] AS Varchar))),40),replicate(' ',40))
		,coalesce(left([Customer Name]+replicate(' ',39-len([Customer Name])),39),replicate(' ',39))
		,replicate(' ',488)
		,coalesce(left([Billing Address line 1]+replicate(' ',40-len([Billing Address line 1])),40),replicate(' ',40))
		,coalesce(left([Billing Address line 2]+replicate(' ',40-len([Billing Address line 2])),40),replicate(' ',40))
		,coalesce(left([Billing Address line 3]+replicate(' ',40-len([Billing Address line 3])),40),replicate(' ',40))
		,coalesce(left([Billing Address line 4]+replicate(' ',40-len([Billing Address line 4])),40),replicate(' ',40))
		,coalesce(left([Billing Address line 5]+replicate(' ',40-len([Billing Address line 5])),40),replicate(' ',40))
		,coalesce(left([Billing Address line 6]+replicate(' ',40-len([Billing Address line 6])),40),replicate(' ',40))
		,coalesce(left([Billing Address line 7]+replicate(' ',40-len([Billing Address line 7])),40),replicate(' ',40))
		,coalesce(left([Billing Address line 8]+replicate(' ',40-len([Billing Address line 8])),40),replicate(' ',40))
		,coalesce(left([Billing Address line 9]+replicate(' ',40-len([Billing Address line 9])),40),replicate(' ',40))
		,coalesce(left([Billing Postcode]+replicate(' ',8-len([Billing Postcode])),8),replicate(' ',8))
		,coalesce(left([Account start date]+replicate(' ',8-len([Account start date])),8),replicate(' ',8))
		,coalesce(left([Billing Frequency]+replicate(' ',1-len([Billing Frequency])),1),replicate(' ',1))
		,coalesce(left([Normal Payment method]+replicate(' ',1-len([Normal Payment method])),1),replicate(' ',1))
		,replicate(' ',64)
		,coalesce(left([Billing Company Name]+replicate(' ',80-len([Billing Company Name])),80),replicate(' ',80))
		,replicate(' ',452)
		,coalesce(left([Annual Consumption Bill to Date]+replicate(' ',8-len([Annual Consumption Bill to Date])),8),replicate(' ',8))
		,coalesce(left(CAST([Annual Consumption] as varchar(14))+replicate(' ',14-len(CAST([Annual Consumption] AS Varchar(14)))),14),replicate(' ',14))
		,coalesce(left([Last Meter Read date]+replicate(' ',8-len([Last Meter Read date])),8),replicate(' ',8))
		,replicate(' ',27)
		,coalesce(left([Last Meter inspection date]+replicate(' ',8-len([Last Meter inspection date])),8),replicate(' ',8))
		,coalesce(left([Meter installation date]+replicate(' ',8-len([Meter installation date])),8),replicate(' ',8))
		,replicate(' ',2)
		,coalesce(left(Meter_Status+replicate(' ',2-len(Meter_Status)),2),replicate(' ',2)))
		FROM	#TRAS
		WHERE	[TYPE] = 'Dom'),

Res_Con_Footer AS
(Select Val = '999' + replicate('0',8-len((select CAST(Count([MPAN_MPRN]) AS varchar) FROM	#TRAS WHERE	[TYPE] = 'Dom')))+(select CAST(Count([MPAN_MPRN]) AS varchar) FROM	#TRAS WHERE	[TYPE] = 'Dom') + replicate(' ',2006))

Select Val FROM Res_Con_Header
UNION ALL
SELECT Val FROM Res_Con_Query
UNION ALL
Select Val From Res_Con_Footer;




--Com_Consumption File
WITH Com_Con_Header AS 
(SELECT Val = '000TRAS COM CONSUMPTION111        '+ FORMAT(getdate(),'yyyyMMdd') +'FMT001'+ replicate(' ',2092)),

Com_Con_Query AS
(Select  Val = Concat(
		 coalesce(left([Record Type]+replicate(' ',3-len([Record Type])),3),replicate(' ',3))
		,coalesce(left([MPAN_MPRN]+replicate(' ',20-len([MPAN_MPRN])),20),replicate(' ',20))
		,coalesce(left([Multiple MPAN]+replicate(' ',1-len([Multiple MPAN])),1),replicate(' ',1))
		,coalesce(left([Supplier Start date (SSD)]+replicate(' ',8-len([Supplier Start date (SSD)])),8),replicate(' ',8))
		,coalesce(left([Supply Address line 1]+replicate(' ',40-len([Supply Address line 1])),40),replicate(' ',40))
		,coalesce(left([Supply Address line 2]+replicate(' ',40-len([Supply Address line 2])),40),replicate(' ',40))
		,coalesce(left([Supply Address line 3]+replicate(' ',40-len([Supply Address line 3])),40),replicate(' ',40))
		,coalesce(left([Supply Address line 4]+replicate(' ',40-len([Supply Address line 4])),40),replicate(' ',40))
		,coalesce(left([Supply Address line 5]+replicate(' ',40-len([Supply Address line 5])),40),replicate(' ',40))
		,coalesce(left([Supply Address line 6]+replicate(' ',40-len([Supply Address line 6])),40),replicate(' ',40))
		,coalesce(left([Supply Address line 7]+replicate(' ',40-len([Supply Address line 7])),40),replicate(' ',40))
		,coalesce(left([Supply Address line 8]+replicate(' ',40-len([Supply Address line 8])),40),replicate(' ',40))
		,coalesce(left([Supply Address line 9]+replicate(' ',40-len([Supply Address line 9])),40),replicate(' ',40))
		,coalesce(left([Supply Postcode]+replicate(' ',8-len([Supply Postcode])),8),replicate(' ',8))
		,coalesce(left(CAST([Supplier's Customer Number] AS Varchar)+replicate(' ',50-len(CAST([Supplier's Customer Number] AS Varchar))),50),replicate(' ',50))
		,coalesce(left([Customer Name]+replicate(' ',80-len([Customer Name])),80),replicate(' ',80))
		,replicate(' ',20)
		,coalesce(left(["Trading As" Account Company Name]+replicate(' ',80-len(["Trading As" Account Company Name])),80),replicate(' ',80))
		,coalesce(left([Customer Address line 1]+replicate(' ',40-len([Customer Address line 1])),40),replicate(' ',40))
		,coalesce(left([Customer Address line 2]+replicate(' ',40-len([Customer Address line 2])),40),replicate(' ',40))
		,coalesce(left([Customer Address line 3]+replicate(' ',40-len([Customer Address line 3])),40),replicate(' ',40))
		,coalesce(left([Customer Address line 4]+replicate(' ',40-len([Customer Address line 4])),40),replicate(' ',40))
		,coalesce(left([Customer Address line 5]+replicate(' ',40-len([Customer Address line 5])),40),replicate(' ',40))
		,coalesce(left([Customer Address line 6]+replicate(' ',40-len([Customer Address line 6])),40),replicate(' ',40))
		,coalesce(left([Customer Address line 7]+replicate(' ',40-len([Customer Address line 7])),40),replicate(' ',40))
		,coalesce(left([Customer Address line 8]+replicate(' ',40-len([Customer Address line 8])),40),replicate(' ',40))
		,coalesce(left([Customer Address line 9]+replicate(' ',40-len([Customer Address line 9])),40),replicate(' ',40))
		,coalesce(left([Customer Postcode]+replicate(' ',8-len([Customer Postcode])),8),replicate(' ',8))
		,replicate(' ',70)
		,coalesce(left(CAST([Supplier's Account Number] AS Varchar)+replicate(' ',20-len(CAST([Supplier's Account Number] AS Varchar))),20),replicate(' ',20))
		,replicate(' ',234)
		,coalesce(left([Billing Company Name]+replicate(' ',80-len([Billing Company Name])),80),replicate(' ',80))
		,replicate(' ',100)
		,coalesce(left([Billing Address line 1]+replicate(' ',40-len([Billing Address line 1])),40),replicate(' ',40))
		,coalesce(left([Billing Address line 2]+replicate(' ',40-len([Billing Address line 2])),40),replicate(' ',40))
		,coalesce(left([Billing Address line 3]+replicate(' ',40-len([Billing Address line 3])),40),replicate(' ',40))
		,coalesce(left([Billing Address line 4]+replicate(' ',40-len([Billing Address line 4])),40),replicate(' ',40))
		,coalesce(left([Billing Address line 5]+replicate(' ',40-len([Billing Address line 5])),40),replicate(' ',40))
		,coalesce(left([Billing Address line 6]+replicate(' ',40-len([Billing Address line 6])),40),replicate(' ',40))
		,coalesce(left([Billing Address line 7]+replicate(' ',40-len([Billing Address line 7])),40),replicate(' ',40))
		,coalesce(left([Billing Address line 8]+replicate(' ',40-len([Billing Address line 8])),40),replicate(' ',40))
		,coalesce(left([Billing Address line 9]+replicate(' ',40-len([Billing Address line 9])),40),replicate(' ',40))
		,coalesce(left([Billing Postcode]+replicate(' ',8-len([Billing Postcode])),8),replicate(' ',8))
		,coalesce(left([Account start date]+replicate(' ',8-len([Account start date])),8),replicate(' ',8))
		,coalesce(left([Billing Frequency]+replicate(' ',1-len([Billing Frequency])),1),replicate(' ',1))
		,coalesce(left([Normal payment method]+replicate(' ',1-len([Normal payment method])),1),replicate(' ',1))
		,replicate(' ',18)
		,coalesce(left([Annual Consumption bill to date]+replicate(' ',8-len([Annual Consumption bill to date])),8),replicate(' ',8))
		,coalesce(left(CAST([Annual Consumption] AS Varchar(14))+replicate(' ',14-len(CAST([Annual Consumption] AS Varchar(14)))),14),replicate(' ',14))
		,coalesce(left(CAST([HH - Monthly Consumption T] AS Varchar(14))+replicate(' ',14-len(CAST([HH - Monthly Consumption T] AS Varchar(14)))),14),replicate(' ',14))
		,coalesce(left(CAST([HH - Monthly Consumption T1] AS Varchar(14))+replicate(' ',14-len(CAST([HH - Monthly Consumption T1] AS Varchar(14)))),14),replicate(' ',14))
		,coalesce(left(CAST([HH - Monthly Consumption T2] AS Varchar(14))+replicate(' ',14-len(CAST([HH - Monthly Consumption T2] AS Varchar(14)))),14),replicate(' ',14))
		,coalesce(left(CAST([HH - Monthly Consumption T3] AS Varchar(14))+replicate(' ',14-len(CAST([HH - Monthly Consumption T3] AS Varchar(14)))),14),replicate(' ',14))
		,coalesce(left(CAST([HH - Monthly Consumption T4] AS Varchar(14))+replicate(' ',14-len(CAST([HH - Monthly Consumption T4] AS Varchar(14)))),14),replicate(' ',14))
		,coalesce(left(CAST([HH - Monthly Consumption T5] AS Varchar(14))+replicate(' ',14-len(CAST([HH - Monthly Consumption T5] AS Varchar(14)))),14),replicate(' ',14))
		,coalesce(left(CAST([HH - Monthly Consumption T6] AS Varchar(14))+replicate(' ',14-len(CAST([HH - Monthly Consumption T6] AS Varchar(14)))),14),replicate(' ',14))
		,coalesce(left(CAST([HH - Monthly Consumption T7] AS Varchar(14))+replicate(' ',14-len(CAST([HH - Monthly Consumption T7] AS Varchar(14)))),14),replicate(' ',14))
		,coalesce(left(CAST([HH - Monthly Consumption T8] AS Varchar(14))+replicate(' ',14-len(CAST([HH - Monthly Consumption T8] AS Varchar(14)))),14),replicate(' ',14))
		,coalesce(left(CAST([HH - Monthly Consumption T9] AS Varchar(14))+replicate(' ',14-len(CAST([HH - Monthly Consumption T9] AS Varchar(14)))),14),replicate(' ',14))
		,coalesce(left(CAST([HH - Monthly Consumption T10] AS Varchar(14))+replicate(' ',14-len(CAST([HH - Monthly Consumption T10] AS Varchar(14)))),14),replicate(' ',14))
		,coalesce(left(CAST([HH - Monthly Consumption T11] AS Varchar(14))+replicate(' ',14-len(CAST([HH - Monthly Consumption T11] AS Varchar(14)))),14),replicate(' ',14))
		,coalesce(left([Last Meter Read date]+replicate(' ',8-len([Last Meter Read date])),8),replicate(' ',8))
		,replicate(' ',27)
		,coalesce(left([Last Meter inspection date]+replicate(' ',8-len([Last Meter inspection date])),8),replicate(' ',8))
		,coalesce(left([Meter installation date]+replicate(' ',8-len([Meter installation date])),8),replicate(' ',8))
		,replicate(' ',2)
		,coalesce(left(Meter_Status+replicate(' ',2-len(Meter_Status)),2),replicate(' ',2)))

FROM	#TRAS
WHERE	[TYPE] = 'Bus'),

Com_Con_Footer AS
(Select Val1 = '999'+(replicate('0',8-len((select CAST(Count([MPAN_MPRN]) AS varchar) FROM	#TRAS WHERE	[TYPE] = 'Bus')))+(select CAST(Count([MPAN_MPRN]) AS varchar) FROM	#TRAS WHERE	[TYPE] = 'Bus')),
		Val2 = replicate(' ',2132))

Select Val FROM Com_Con_Header
UNION ALL
SELECT Val FROM Com_Con_Query
UNION ALL
Select Val1 + Val2 From Com_Con_Footer;


--Residential Outcome files

WITH Res_Out_Header AS 
(SELECT Val = '000TRAS RES OUTCOMEXYZ        '+ FORMAT(getdate(),'yyyyMMdd') +'FMT001'+ replicate(' ',1970)),

Res_Out_Query AS
(SELECT	Val = Concat(
		CASE
			WHEN ISNULL(C.MO_Date,'01Jan1900') <= @reportend THEN '002'
			ELSE '001' 
		END
		,coalesce(left(T.[MPAN_MPRN]+replicate(' ',20-len(T.[MPAN_MPRN])),20),replicate(' ',20))
		,coalesce(left([Supply_Address_line_1]+replicate(' ',40-len([Supply_Address_line_1])),40),replicate(' ',40))
		,coalesce(left([Supply_Address_line_2]+replicate(' ',40-len([Supply_Address_line_2])),40),replicate(' ',40))
		,coalesce(left([Supply_Address_line_3]+replicate(' ',40-len([Supply_Address_line_3])),40),replicate(' ',40))
		,coalesce(left([Supply_Address_line_4]+replicate(' ',40-len([Supply_Address_line_4])),40),replicate(' ',40))
		,coalesce(left([Supply_Address_line_5]+replicate(' ',40-len([Supply_Address_line_5])),40),replicate(' ',40))
		,coalesce(left([Supply_Address_line_6]+replicate(' ',40-len([Supply_Address_line_6])),40),replicate(' ',40))
		,coalesce(left([Supply_Address_line_7]+replicate(' ',40-len([Supply_Address_line_7])),40),replicate(' ',40))
		,coalesce(left([Supply_Address_line_8]+replicate(' ',40-len([Supply_Address_line_8])),40),replicate(' ',40))
		,coalesce(left([Supply_Address_line_9]+replicate(' ',40-len([Supply_Address_line_9])),40),replicate(' ',40))
		,coalesce(left([Supply_Postcode]+replicate(' ',8-len([Supply_Postcode])),8),replicate(' ',8))
		,coalesce(left(CAST([Supplier_Account_Number] AS Varchar)+replicate(' ',40-len(CAST([Supplier_Account_Number] AS Varchar))),40),replicate(' ',40))
		,coalesce(left([Account_holders_name_1]+replicate(' ',39-len([Account_holders_name_1])),39),replicate(' ',39))
		,coalesce(left([Account_holders_Date_of_birth_1]+replicate(' ',8-len([Account_holders_Date_of_birth_1])),8),replicate(' ',8))
		,coalesce(left([Billing_Address_line_1]+replicate(' ',40-len([Billing_Address_line_1])),40),replicate(' ',40))
		,coalesce(left([Billing_Address_line_2]+replicate(' ',40-len([Billing_Address_line_2])),40),replicate(' ',40))
		,coalesce(left([Billing_Address_line_3]+replicate(' ',40-len([Billing_Address_line_3])),40),replicate(' ',40))
		,coalesce(left([Billing_Address_line_4]+replicate(' ',40-len([Billing_Address_line_4])),40),replicate(' ',40))
		,coalesce(left([Billing_Address_line_5]+replicate(' ',40-len([Billing_Address_line_5])),40),replicate(' ',40))
		,coalesce(left([Billing_Address_line_6]+replicate(' ',40-len([Billing_Address_line_6])),40),replicate(' ',40))
		,coalesce(left([Billing_Address_line_7]+replicate(' ',40-len([Billing_Address_line_7])),40),replicate(' ',40))
		,coalesce(left([Billing_Address_line_8]+replicate(' ',40-len([Billing_Address_line_8])),40),replicate(' ',40))
		,coalesce(left([Billing_Address_line_9]+replicate(' ',40-len([Billing_Address_line_9])),40),replicate(' ',40))
		,coalesce(left([Billing_Postcode]+replicate(' ',8-len([Billing_Postcode])),8),replicate(' ',8))
		,coalesce(left([Meter_Serial_Number]+replicate(' ',20-len([Meter_Serial_Number])),20),replicate(' ',20))
		,CASE WHEN LEN(T.MPAN_MPRN)=13 THEN 'E_' ELSE 'G_' END + CAST([Supplier_Account_Number] AS Varchar) + replicate(' ',20-len(CASE WHEN LEN(T.MPAN_MPRN)=13 THEN 'E_' ELSE 'G_' END + CAST([Supplier_Account_Number] AS Varchar)))
		,coalesce(left([Theft_lead_source]+replicate(' ',20-len([Theft_lead_source])),20),replicate(' ',20))
		,coalesce(left([Date_Investigation_Closed]+replicate(' ',8-len([Date_Investigation_Closed])),8),replicate(' ',8))
		,coalesce(left([Current_investigation_code]+replicate(' ',2-len([Current_investigation_code])),2),replicate(' ',2))
		,coalesce(left([Type_of_theft]+replicate(' ',50-len([Type_of_theft])),50),replicate(' ',50))
		,coalesce(left([Crime_reference_no]+replicate(' ',20-len([Crime_reference_no])),20),replicate(' ',20))
		,coalesce(left([Assessed_start_date_for_theft]+replicate(' ',8-len([Assessed_start_date_for_theft])),8),replicate(' ',8))
		,coalesce(left([Assessed_end_date_for_theft]+replicate(' ',8-len([Assessed_end_date_for_theft])),8),replicate(' ',8))
		,coalesce(left([Assessed_losses]+replicate(' ',19-len([Assessed_losses])),19),replicate(' ',19))
		,coalesce(left([Tampering_Code]+replicate(' ',2-len([Tampering_Code])),2),replicate(' ',2))
		,coalesce(left([Tampering_Report_Date]+replicate(' ',8-len([Tampering_Report_Date])),8),replicate(' ',8))
		,coalesce(left([Tampering_Report_Source]+replicate(' ',2-len([Tampering_Report_Source])),2),replicate(' ',2))
		,coalesce(left([Security_devices_fitted]+replicate(' ',1-len([Security_devices_fitted])),1),replicate(' ',1)))

FROM	[DatSup].[dbo].[TRAS_Outcome] T
			LEFT JOIN [DatSup].[dbo].CAMPAN_Data C ON T.MPAN_MPRN = C.MPAN_MPRN AND T.Supplier_Account_Number = C.Contract_Account
WHERE	[Month] BETWEEN @reportstart AND @ReportEnd),

Res_Out_Footer AS
(Select Val1 = '999'+(replicate('0',8-len((select CAST(Count(MPAN_MPRN) AS varchar) FROM [DatSup].[dbo].[TRAS_Outcome] WHERE [Month] BETWEEN @reportstart AND @ReportEnd)))+(select CAST(Count(MPAN_MPRN) AS varchar) FROM [DatSup].[dbo].[TRAS_Outcome] WHERE [Month] BETWEEN @reportstart AND @ReportEnd)),
		Val2 = replicate(' ',2003))

Select Val FROM Res_Out_Header
UNION ALL
SELECT Val FROM Res_Out_Query
UNION ALL
Select Val1 + Val2 From Res_Out_Footer;