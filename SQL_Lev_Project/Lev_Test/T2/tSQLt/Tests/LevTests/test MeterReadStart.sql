﻿CREATE PROCEDURE LevTests.[test Meter Read Start View]
AS
Begin
--Assemble
EXEC tSQLt.FakeTable @TableName='dbo.READINGS_STAGING'
--EXECtSQLt.FakeTable@TableName='dbo.Meter_Read_Start_Expected'
--EXECtSQLt.FakeTable@TableName='dbo.Meter_Read_End_Expected'
--EXECtSQLt.FakeTable@TableName='dbo.Meter_Read_Start_Act'
--EXECtSQLt.FakeTable@TableName='dbo.Meter_Read_End_Act'

INSERT INTO dbo.READINGS_STAGING
	([READ_ID],[INSTALLATION_ID_link],[Meter_Read_Type],[Meter_Read_Date],[Meter_Read_Reason],[Generation_Read],[Export_Read],[IncludedInLevelisation],[Gen_Validation],[Modified_By],[Date_Modified],[Time_Modified],[Claimed],[Generation_Clocked],[Export_Clocked],[Notes],[MSN],[ESP_Billed],[Recieved_Method],[Photo],[Export_MSN],[Current_Price_Ext1],[Current_Price_Ext2],[Current_Price_Initial],[Current_Price_Export],[Current_Price_Export1],[Current_Price_Export2],[Current_Price_InitExp2],[Fit_Period])
VALUES
	(767207	,10677	,'Customer'	,'2018-06-21','F9Q1'	,11929,NULL	,1	,'VALID' ,'Kieran' ,'2018-06-21' ,'1899-12-30 15:11:58.000'	,1	,NULL	,NULL	,NULL	,9101680	,NULL	,'Email',0	,NULL	,NULL	,NULL	,52.75	,3.72,NULL,NULL,NULL,9),
	(767207	,10677	,'Customer'	,'2018-06-21','F9Q1'	,11929,NULL	,1	,'VALID' ,'Kieran' ,'2018-06-21' ,'1899-12-30 15:11:58.000'	,1	,NULL	,NULL	,NULL	,9101680	,NULL	,'Email',0	,NULL	,NULL	,NULL	,52.75	,3.72	,NULL	,NULL	,NULL	,9),
	(931010	,10677	,'Customer'	,'2018-10-09','F9Q2'	,12569,NULL	,0	,'INVALID' ,'KRIL-ECO' ,'2018-11-12' ,'1899-12-30 15:58:26.000'	,0	,NULL	,NULL	,NULL	,9101680	,NULL	,'Webform',0	,NULL	,NULL	,NULL	,52.75	,3.72	,NULL	,NULL	,NULL	,9),
	(764366	,10678	,'Customer'	,'2018-06-10','F9Q1'	,15304,NULL	,1	,'VALID' ,'MASS' ,'2018-06-12' ,'1899-12-30 19:10:17.000'	,1	,NULL	,NULL	,NULL	,9101349	,NULL	,'Webform',NULL	,NULL	,NULL	,NULL	,46	,3.72	,NULL	,NULL	,NULL	,9),
	(855287	,10678	,'Customer'	,'2018-09-15','F9Q2'	,16046,NULL	,0	,'VALID' ,'MASS' ,'2018-09-17' ,'1899-12-30 15:08:10.000'	,0	,NULL	,NULL	,NULL	,9101349	,NULL	,'Webform',NULL	,NULL	,NULL	,NULL	,46	,3.72	,NULL	,NULL	,NULL	,9)

CREATE TABLE LevTests.Expected
(
[READ_ID][INT]NULL,
[INSTALLATION_ID_link][INT]NULL,
[Meter_Read_Type][NVARCHAR](255)NULL,
Meter_Read_Date_START[DATETIME]NULL,
[Meter_Read_Reason][NVARCHAR](255)NULL,
Generation_Read_START[INT]NULL,
[Export_Read_START][INT]NULL,
[IncludedInLevelisation][BIT]NULL,
[Gen_Validation][NVARCHAR](255)NULL,
[Modified_By][NVARCHAR](255)NULL,
[Claimed][BIT]NULL,
[MSN][VARCHAR](255)NULL,
[Fit_Period][INT]NULL,
[Current_Price_Initial][NUMERIC](12,2)NULL,
[Current_Price_Export][NUMERIC](12,2)NULL,
[Current_Price_Ext1][NUMERIC](12,2)NULL,
[Current_Price_Ext2][NUMERIC](12,2)NULL,
[Current_Price_Export1][NUMERIC](12,2)NULL,
[Current_Price_Export2][NUMERIC](12,2)NULL,
[Current_Price_InitExp2][NUMERIC](12,2)NULL
)
INSERT INTO LevTests.Expected
	([READ_ID],[INSTALLATION_ID_link],[Meter_Read_Type],[Meter_Read_Date_START],[Meter_Read_Reason],[Generation_Read_START],[Export_Read_START],[IncludedInLevelisation],[Gen_Validation],[Modified_By],[Claimed],[MSN],[Fit_Period],[Current_Price_Initial],[Current_Price_Export],[Current_Price_Ext1],[Current_Price_Ext2],[Current_Price_Export1],[Current_Price_Export2],[Current_Price_InitExp2])
VALUES
	(764366	,10678	,'Customer'	,'2018-06-10','F9Q1'	,15304 ,NULL	,1	,'VALID'	,'MASS'	,1	,9101349	,9	,46	,3.72	,NULL	,NULL	,NULL	,NULL	,NULL),
	(767207	,10677	,'Customer'	,'2018-06-21','F9Q1'	,11929 ,NULL	,1	,'VALID'	,'Kieran'	,1	,9101680	,9	,52.75	,3.72	,NULL	,NULL	,NULL	,NULL	,NULL)

--INSERTINTOdbo.Meter_Read_END_Expected
--VALUES
--(855287	,10678	,'Customer'	,'2018-9-15'	,'F9Q2'	,16046	,NULL	,0	,'VALID'	,'MASS'	,'1900-1-0'	,'1899-12-3015:08:10.000'	,0	,9101349	,9	,46	,3.72	,NULL	,NULL	,NULL	,NULL	,NULL),
--(931010	,10677	,'Customer'	,'2018-10-9'	,'F9Q2'	,12569	,NULL	,0	,'INVALID'	,'KRIL-ECO'	,'1900-1-0'	,'1899-12-3015:58:26.000'	,0	,9101680	,9	,52.75	,3.72	,NULL	,NULL	,NULL	,NULL	,NULL)

--Actuals
Select * INTO LevTests.Actual from dbo.Meter_Read_START_vw

--Select*INTOMeter_Read_End_ActfromMeter_Read_END_vw


--Assert(CompareExpectedtoActual)
EXEC tSQLt.AssertEqualsTable @Expected = 'LevTests.Expected',@Actual='LevTests.Actual'

--EXECtSQLt.AssertEqualsTable@Expected='dbo.Meter_Read_END_Expected',@Actual='Meter_Read_END_vw'
End;


