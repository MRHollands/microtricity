﻿CREATE TABLE [dbo].[Numbers] (
    [Number] SMALLINT IDENTITY (1, 1) NOT NULL,
    CONSTRAINT [PK__Numbers__78A1A19CE5B48119] PRIMARY KEY CLUSTERED ([Number] ASC)
);

