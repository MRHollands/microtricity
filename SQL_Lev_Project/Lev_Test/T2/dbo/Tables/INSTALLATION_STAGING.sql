﻿CREATE TABLE [dbo].[INSTALLATION_STAGING] (
    [INSTALLATION_ID]             INT            NULL,
    [Scheme_Type]                 NVARCHAR (255) CONSTRAINT [DF__INSTALLAT__Schem__5441852A] DEFAULT ('FiT') NULL,
    [Generation_Type]             NVARCHAR (255) NULL,
    [Total_Installed_Capacity_kW] FLOAT (53)     NULL,
    [FiT_ID_Initial]              NVARCHAR (255) NULL,
    [Initial_System_Tariff_Rate]  NVARCHAR (255) NULL,
    [Initial_System_Export_Rate]  NVARCHAR (255) NULL,
    [Initial_System_Pct_Split]    FLOAT (53)     CONSTRAINT [DF__INSTALLAT__Initi__5535A963] DEFAULT ((1)) NULL,
    [FiT_ID_Ext1]                 NVARCHAR (255) NULL,
    [Extension_1_Tariff_Rate]     NVARCHAR (255) NULL,
    [Extension_1_Export_Rate]     NVARCHAR (255) NULL,
    [Extension_1_Pct_Split]       FLOAT (53)     CONSTRAINT [DF__INSTALLAT__Exten__5629CD9C] DEFAULT ((0)) NULL,
    [FiT_ID_Ext2]                 NVARCHAR (255) NULL,
    [Extension_2_Tariff_Rate]     NVARCHAR (255) NULL,
    [Extension_2_Export_Rate]     NVARCHAR (255) NULL,
    [Extension_2_Pct_Split]       FLOAT (53)     CONSTRAINT [DF__INSTALLAT__Exten__571DF1D5] DEFAULT ((0)) NULL,
    [TopUp_Tariff_Rate]           NVARCHAR (255) NULL,
    [ExpPct1]                     FLOAT (53)     NULL,
    [ExpPct2]                     FLOAT (53)     NULL,
    [InitSysExpRate2]             NVARCHAR (255) NULL,
    [Account_Status]              VARCHAR (50)   NULL
);

