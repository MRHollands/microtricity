﻿CREATE TABLE [dbo].[Exceptions] (
    [INSTALLATION_ID]         INT             NULL,
    [FiT_ID_Initial]          NVARCHAR (255)  NULL,
    [FiT_ID_Ext1]             NVARCHAR (255)  NULL,
    [FiT_ID_Ext2]             NVARCHAR (255)  NULL,
    [Meter_Read_Reason_Start] NVARCHAR (255)  NULL,
    [Meter_Read_Date_START]   DATETIME        NULL,
    [Meter_Read_Reason_END]   NVARCHAR (255)  NULL,
    [Meter_Read_Date_END]     DATETIME        NULL,
    [Units_Exported]          INT             NULL,
    [Units_Generated]         INT             NULL,
    [Generation_Payment]      FLOAT (53)      NULL,
    [Export_Payment]          NUMERIC (27, 6) NULL,
    [Total_Payment]           FLOAT (53)      NULL,
    [Number_of_Splits]        INT             NULL
);

