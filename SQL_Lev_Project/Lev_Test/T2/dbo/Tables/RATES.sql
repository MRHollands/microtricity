﻿CREATE TABLE [dbo].[RATES] (
    [RATE_ID]                  INT             NULL,
    [Generation_Type]          NVARCHAR (255)  NULL,
    [Tariff_Code]              NVARCHAR (255)  NULL,
    [Description]              NVARCHAR (255)  NULL,
    [FiT_Period]               INT             NULL,
    [Min_Capacity]             INT             NULL,
    [Max_Capacity]             INT             NULL,
    [Valid_From]               DATETIME        NULL,
    [Valid_To]                 DATETIME        NULL,
    [Price]                    NUMERIC (12, 2) NULL,
    [EligibilityDateFrom]      DATETIME        NULL,
    [EligibilityDateTo]        DATETIME        NULL,
    [ESP_Year]                 INT             NULL,
    [Gross_Electricity_Charge] NUMERIC (12, 2) NULL,
    CONSTRAINT [UQ_codes] UNIQUE NONCLUSTERED ([Tariff_Code] ASC, [FiT_Period] ASC, [Valid_From] ASC, [Valid_To] ASC)
);

