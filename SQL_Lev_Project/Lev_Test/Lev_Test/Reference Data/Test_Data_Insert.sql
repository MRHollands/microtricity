
INSERT INTO dbo.INSTALLATION_STAGING
(
 INSTALLATION_ID,
 Scheme_Type,
 Generation_Type,
 Total_Installed_Capacity_kW,
 FiT_ID_Initial,
 Initial_System_Tariff_Rate,
 Initial_System_Export_Rate,
 Initial_System_Pct_Split,
 FiT_ID_Ext1,
 Extension_1_Tariff_Rate,
 Extension_1_Export_Rate,
 Extension_1_Pct_Split,
 FiT_ID_Ext2,
 Extension_2_Tariff_Rate,
 Extension_2_Export_Rate,
 Extension_2_Pct_Split,
 TopUp_Tariff_Rate,
 ExpPct1,
 ExpPct2,
 InitSysExpRate2,
 Account_Status
)
VALUES
(10677, 'FIT', 'Solar PV', 1.48, 'FIT00000219-1', 'PV-R/0-4/01', 'EXP_1', 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'Registered Live')
,(10678, 'FIT', 'Solar PV', 2.16, 'FIT00000221-1', 'PV-N/0-4/01', 'EXP_1', 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'Registered Live');
GO

INSERT INTO dbo.READINGS_STAGING
(
 READ_ID,
 INSTALLATION_ID_link,
 Meter_Read_Type,
 Meter_Read_Date,
 Meter_Read_Reason,
 Generation_Read,
 Export_Read,
 IncludedInLevelisation,
 Gen_Validation,
 Modified_By,
 Date_Modified,
 Time_Modified,
 Claimed,
 Generation_Clocked,
 Export_Clocked,
 Notes,
 MSN,
 ESP_Billed,
 Recieved_Method,
 Photo,
 Export_MSN,
 Current_Price_Ext1,
 Current_Price_Ext2,
 Current_Price_Initial,
 Current_Price_Export,
 Current_Price_Export1,
 Current_Price_Export2,
 Current_Price_InitExp2,
 Fit_Period
)
VALUES
(767207 ,10677 ,'Customer' ,'2018-06-21' ,'F9Q1' ,11929 ,NULL ,1 ,'VALID' ,'Kieran' ,'2018-06-21' ,'1899-12-30 15:11:58.000' ,1 ,NULL ,NULL ,NULL ,9101680 ,NULL ,'Email' ,0 ,NULL ,NULL ,NULL ,52.75 ,3.72 ,NULL ,NULL ,NULL ,9),
(767207 ,10677 ,'Customer' ,'2018-06-21' ,'F9Q1' ,11929 ,NULL ,1 ,'VALID' ,'Kieran' ,'2018-06-21' ,'1899-12-30 15:11:58.000' ,1 ,NULL ,NULL ,NULL ,9101680 ,NULL ,'Email' ,0 ,NULL ,NULL ,NULL ,52.75 ,3.72 ,NULL ,NULL ,NULL ,9),
(931010 ,10677 ,'Customer' ,'2018-10-09' ,'F9Q2' ,12569 ,NULL ,0 ,'INVALID' ,'KRIL-ECO' ,'2018-11-12' ,'1899-12-30 15:58:26.000' ,0 ,NULL ,NULL ,NULL ,9101680 ,NULL ,'Webform' ,0 ,NULL ,NULL ,NULL ,52.75 ,3.72 ,NULL ,NULL ,NULL ,9),
(764366 ,10678 ,'Customer' ,'2018-06-10' ,'F9Q1' ,15304 ,NULL ,1 ,'VALID' ,'MASS' ,'2018-06-12' ,'1899-12-30 19:10:17.000' ,1 ,NULL ,NULL ,NULL ,9101349 ,NULL ,'Webform' ,NULL ,NULL ,NULL ,NULL ,46 ,3.72 ,NULL ,NULL ,NULL ,9),
(855287 ,10678 ,'Customer' ,'2018-09-15' ,'F9Q2' ,16046 ,NULL ,0 ,'VALID' ,'MASS' ,'2018-09-17' ,'1899-12-30 15:08:10.000' ,0 ,NULL ,NULL ,NULL ,9101349 ,NULL ,'Webform' ,NULL ,NULL ,NULL ,NULL ,46 ,3.72 ,NULL ,NULL ,NULL ,9)
GO

INSERT INTO dbo.RATES
(
 RATE_ID,
 Generation_Type,
 Tariff_Code,
 Description,
 FiT_Period,
 Min_Capacity,
 Max_Capacity,
 Valid_From,
 Valid_To,
 Price,
 EligibilityDateFrom,
 EligibilityDateTo,
 ESP_Year,
 Gross_Electricity_Charge
)
VALUES
(16702 ,'Photovoltaic' ,'PV-N/0-4/01' ,'PV (<=4kW (new build))-2010/11' ,9 ,NULL ,NULL ,'2018-04-01' ,'2019-03-31' ,46 ,'2010-04-01' ,'2011-03-31' ,NULL ,NULL),
(16686 ,'PhotoVoltaic' ,'PV-R/0-4/01' ,'PV (<=4kW (retrofit))-2010/11' ,9 ,NULL ,NULL ,'2018-04-01' ,'9999-12-31' ,52.75 ,'1900-01-01' ,'9999-12-31' ,NULL ,NULL)
GO

SELECT * FROM dbo.RATES