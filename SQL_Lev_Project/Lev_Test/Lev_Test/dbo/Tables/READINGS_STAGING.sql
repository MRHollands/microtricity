﻿CREATE TABLE [dbo].[READINGS_STAGING] (
    [READ_ID]                INT             NULL,
    [INSTALLATION_ID_link]   INT             NULL,
    [Meter_Read_Type]        NVARCHAR (255)  NULL,
    [Meter_Read_Date]        DATETIME        NULL,
    [Meter_Read_Reason]      NVARCHAR (255)  NULL,
    [Generation_Read]        INT             NULL,
    [Export_Read]            INT             NULL,
    [IncludedInLevelisation] BIT             CONSTRAINT [DF__READINGS__Includ__4BAC3F29] DEFAULT ((0)) NULL,
    [Gen_Validation]         NVARCHAR (255)  NULL,
    [Modified_By]            NVARCHAR (255)  NULL,
    [Date_Modified]          DATETIME        NULL,
    [Time_Modified]          DATETIME        NULL,
    [Claimed]                BIT             NULL,
    [Generation_Clocked]     INT             NULL,
    [Export_Clocked]         INT             NULL,
    [Notes]                  VARCHAR (255)   NULL,
    [MSN]                    VARCHAR (255)   NULL,
    [ESP_Billed]             BIT             NULL,
    [Recieved_Method]        NVARCHAR (255)  NULL,
    [Photo]                  BIT             NULL,
    [Export_MSN]             VARCHAR (255)   NULL,
    [Current_Price_Ext1]     NUMERIC (12, 2) NULL,
    [Current_Price_Ext2]     NUMERIC (12, 2) NULL,
    [Current_Price_Initial]  NUMERIC (12, 2) NULL,
    [Current_Price_Export]   NUMERIC (12, 2) NULL,
    [Current_Price_Export1]  NUMERIC (12, 2) NULL,
    [Current_Price_Export2]  NUMERIC (12, 2) NULL,
    [Current_Price_InitExp2] NUMERIC (12, 2) NULL,
    [Fit_Period]             INT             NULL
);

