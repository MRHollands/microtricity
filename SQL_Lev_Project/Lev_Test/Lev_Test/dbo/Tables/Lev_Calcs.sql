﻿CREATE TABLE [dbo].[Lev_Calcs] (
    [FiT_ID_Initial]        NVARCHAR (255)  NULL,
    [Meter_Read_Reason_Old] VARCHAR (50)    NULL,
    [Meter_Read_Date_Old]   DATE            NULL,
    [Meter_Read_Reason_New] VARCHAR (50)    NULL,
    [Meter_Read_Date_New]   DATE            NULL,
    [Generation]            INT             NULL,
    [Old_Price]             NUMERIC (12, 2) NULL,
    [New_Price]             NUMERIC (12, 2) NULL,
    [Installation_ID]       INT             NULL
);

